# Cookbook

Cookbook is a web application for personal recipe management. This project
serves as a playground for various technologies, exploring their compatibility
and best practices, hence the name *Cookbook*.

## Overview

Cookbook is designed to manage a private recipe collection of one household.
Recipes may be collected from various sources, stored in a database, searched,
and modified for one's personal taste or requirements. Consequently, Cookbook
is not designed for web publishing.

The project consists of a single-page application as the front end, and a
RESTful web service as the backend.
The service supports creating, viewing, editing and deleting recipes through
a REST API.
Additionally, powerful search capabilities are provided by
[Apache Solr](http://lucene.apache.org/solr/).

## Technologies

Originally, this project consisted of a conventional
[Spring](http://spring.io)-based web application, including plain old-fashioned
[JSP](https://en.wikipedia.org/wiki/JavaServer_Pages) views enhanced with a few
[jQuery](https://jquery.com/)-based client-side components.
The application could be run, for example, on a home server or on
a hosted virtual server.
From the very beginning, the UI was designed as responsive to also support
mobile devices, so that one may easily access the recipes from where they are
needed, i.e. the kitchen.

Afterwards, some newer technologies,
such as [Thymeleaf](http://www.thymeleaf.org/),
were introduced. Subsequently, the project has continued its
evolution, experimenting new technologies and their compatibility.

Currently, the following tools and technologies are being fit together.

The frontend includes:

* [AngularJS](https://angularjs.org/)
* [Bootstrap](http://getbootstrap.com/)
* [Summernote](http://summernote.org/)
* [Bootstrap Tags Input](https://github.com/bootstrap-tagsinput/bootstrap-tagsinput)
* [Sass](http://sass-lang.com/)

The backend is implemented with:

* [Apache Solr](http://lucene.apache.org/solr/)
* Several Spring projects
    * [Spring Boot](http://projects.spring.io/spring-boot/)
    * [Spring Data](http://projects.spring.io/spring-data/)
    * [Spring Framework](http://projects.spring.io/spring-framework/)
    * [Spring HATEOAS](http://projects.spring.io/spring-hateoas/)
    * [Spring REST Docs](http://projects.spring.io/spring-restdocs/)
    * [Spring Security](http://projects.spring.io/spring-security/)
* [Querydsl](http://www.querydsl.com/)
* [Hibernate ORM](http://hibernate.org/orm/)
* [Hibernate Validator](http://hibernate.org/validator/)
* [Guava](https://github.com/google/guava)
* [Apache Derby](https://db.apache.org/derby/)
* JDK 8

Furthermore, some very useful tools:

* [Docker](https://www.docker.com/)
* [Gradle](http://gradle.org/)

An online demo is available [here](http://demo.cookbook.versatackle.com).
