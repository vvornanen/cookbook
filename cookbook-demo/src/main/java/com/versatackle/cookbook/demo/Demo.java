package com.versatackle.cookbook.demo;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.springframework.context.annotation.Profile;

/**
 * Annotates a service to be enabled only when the demo profile is active.
 *
 * @author Ville Vornanen
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Profile("demo")
public @interface Demo {
}
