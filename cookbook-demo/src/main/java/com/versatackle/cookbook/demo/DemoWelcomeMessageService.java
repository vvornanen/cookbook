package com.versatackle.cookbook.demo;

import com.versatackle.cookbook.spi.login.WelcomeMessage;
import com.versatackle.cookbook.spi.login.WelcomeMessageService;
import java.util.Locale;
import java.util.ResourceBundle;
import org.springframework.stereotype.Service;

/**
 * Provides a welcome message for the demonstration including demo user credentials
 * and information.
 *
 * This service is enabled only when the demo spring profile is active.
 * 
 * @author Ville Vornanen
 */
@Demo
@Service
public class DemoWelcomeMessageService implements WelcomeMessageService {

    @Override
    public WelcomeMessage getMessage(final Locale locale) {
        return new WelcomeMessage() {
            /**
             * Bundle is reloaded for each call with the current locale.
             */
            private final ResourceBundle bundle
                    = ResourceBundle.getBundle("messages", locale);

            @Override
            public String getTitle() {
                return bundle.getString("welcome.title");
            }

            @Override
            public String getContent() {
                return bundle.getString("welcome.message");
            }
        };
    }

}
