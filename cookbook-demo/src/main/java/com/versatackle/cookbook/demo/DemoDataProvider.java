package com.versatackle.cookbook.demo;

import com.versatackle.cookbook.recipe.RecipeDTO;
import com.versatackle.cookbook.recipe.RecipeService;
import com.versatackle.cookbook.user.UserDTO;
import com.versatackle.cookbook.user.UserService;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.xml.transform.stream.StreamSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * Provides a user and some recipes for demonstration.
 *
 * To prevent accidental loss of existing recipes, demonstration features
 * are enabled only then the demo profile is explicitly activated.
 *
 * @author Ville Vornanen
 */
@Demo
@Service
public class DemoDataProvider {

    private static final Logger LOGGER
            = LoggerFactory.getLogger(DemoDataProvider.class);
    private static final String DEMO_RECIPES_XML = "demo-recipes.xml";

    @Resource
    private UserService userService;

    @Resource
    private RecipeService recipeService;

    @Resource
    private Jaxb2Marshaller marshaller;

    @PostConstruct
    public void initialize() {
        LOGGER.info("Initializing demo data provider");

        UserDTO user = new UserDTO();
        user.setUsername("user");
        user.setName("Demo user");
        user.setId(UUID.fromString("565577af-4640-42f6-bc05-1ef67b7f7b69"));
        // Password: user
        user.setPassword(
                "$2a$10$OoMmwc9imaw6vKeDGbYf7uexMjOKz1cPSgnrqJS1FJp/2CK2bL.Aa");

        if (!userService.exists(user.getId())) {
            userService.add(user);
            LOGGER.info("Added demo user: {}", user);
        } else {
            LOGGER.debug("Demo user already exists: {}", user);
        }

        deleteAllAndInsertDemoRecipes();
    }

    @Scheduled(cron = "0 0 * * * *")
    public void deleteAllAndInsertDemoRecipes() {
        LOGGER.info("Deleting all existing recipes");
        recipeService.deleteAll();
        insertDemoRecipes();
    }

    private void insertDemoRecipes() {
        try (InputStream is = getResource(DEMO_RECIPES_XML)) {
            StreamSource source = new StreamSource(is);
            RecipeDTO.RecipeList recipes
                    = (RecipeDTO.RecipeList) marshaller.unmarshal(source);
            recipeService.addAll(recipes.getRecipes());
            LOGGER.info("Created {} demo recipe(s)", recipes.getRecipes().size());
        } catch (IOException ex) {
            LOGGER.error("Loading demo recipes failed", ex);
        }
    }

    private InputStream getResource(String path) throws IOException {
        return new ClassPathResource(path).getInputStream();
    }
}
