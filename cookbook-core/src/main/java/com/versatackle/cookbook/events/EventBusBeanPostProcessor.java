package com.versatackle.cookbook.events;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Set;
import javax.annotation.Resource;
import javax.annotation.concurrent.ThreadSafe;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.DestructionAwareBeanPostProcessor;

/**
 * Registers automatically all beans with {@link Subscribe} annotations to
 * the event bus.
 *
 * To prevent memory leaks, beans are also automatically unregistered before
 * destruction. Most listener beans will probably anyway have singleton as their
 * scope, but automatic unregistering makes it possible to have subscribers with any
 * scope.
 *
 * @author Ville Vornanen
 */
@ThreadSafe
public class EventBusBeanPostProcessor
        implements DestructionAwareBeanPostProcessor {

    private static final Logger LOGGER
            = LoggerFactory.getLogger(EventBusBeanPostProcessor.class);

    /**
     * EventBus used for registering and unregistering subscriber beans.
     *
     * Though undocumented, {@code EventBus.register()} and
     * {@code EventBus.unregister()} seem to be thread-safe.
     */
    @Resource
    private EventBus eventBus;

    /**
     * Keeps track of all registered objects so that they can be unregistered
     * before destruction.
     */
    private final Set<Object> listeners = new HashSet<>();

    @Override
    public void postProcessBeforeDestruction(Object bean, String beanName)
            throws BeansException {
        unregister(bean);
    }

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName)
            throws BeansException {
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName)
            throws BeansException {
        for (Method method: bean.getClass().getMethods()) {
            if (isSubscriber(method)) {
                register(bean);
                break;
            }
        }

        return bean;
    }

    /**
     * Returns true if the given method is annotated with {@link Subscribe}.
     *
     * @param method method to check for annotation
     * @return returns true if the method is annotated
     */
    private boolean isSubscriber(Method method) {
        return method.isAnnotationPresent(Subscribe.class);
    }

    /**
     * Registers the given bean to the event bus if not already registered.
     *
     * @param bean bean containing {@link Subscriber} annotations
     */
    private synchronized void register(Object bean) {
        if (listeners.add(bean)) {
            LOGGER.debug("Registering bean {} to the event bus", bean);
            eventBus.register(bean);
        }
    }

    /**
     * Unregisters the given bean from the event bus.
     *
     * Does nothing if the bean has not been previously registered.
     *
     * @param bean previously registered bean to unregister
     */
    private synchronized void unregister(Object bean) {
        if (listeners.remove(bean)) {
            LOGGER.debug("Unregistering bean {} from the event bus", bean);
            eventBus.unregister(bean);
        }
    }
}
