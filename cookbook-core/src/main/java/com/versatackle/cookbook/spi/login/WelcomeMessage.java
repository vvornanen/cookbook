package com.versatackle.cookbook.spi.login;

/**
 * Describes welcome content to be displayed on the login view.
 *
 * @author Ville Vornanen
 */
public interface WelcomeMessage {

    /**
     * Provides a title for the login view welcome message.
     *
     * @return plain text string to display as the title for the login message
     */
    public String getTitle();

    /**
     * Provides a safe HTML message for the login view.
     *
     * The message is not filtered so it must come from a trusted source.
     *
     * @return HTML string to display on the login view
     */
    public String getContent();

}
