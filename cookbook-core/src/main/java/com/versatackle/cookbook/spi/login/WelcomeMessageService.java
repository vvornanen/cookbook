package com.versatackle.cookbook.spi.login;

import java.util.Locale;

/**
 * Provides a welcome message for the login view.
 *
 * @author Ville Vornanen
 */
public interface WelcomeMessageService {

    /**
     * Returns a welcome message to be displayed on the login view.
     *
     * @param locale the locale used for the message
     * @return message to be displayed on the login view
     */
    WelcomeMessage getMessage(Locale locale);

}
