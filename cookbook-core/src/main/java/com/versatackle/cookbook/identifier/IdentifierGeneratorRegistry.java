package com.versatackle.cookbook.identifier;

import static com.google.common.base.Preconditions.checkArgument;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import javax.annotation.concurrent.ThreadSafe;

/**
 * Keeps a registry about different types of identifier generators.
 *
 * @author Ville Vornanen
 */
@ThreadSafe
public class IdentifierGeneratorRegistry {

    private static final IdentifierGeneratorRegistry INSTANCE
            = new IdentifierGeneratorRegistry();

    private final Map<Class<?>, IdentifierGenerator<?>> generators
            = new ConcurrentHashMap<>();

    private IdentifierGeneratorRegistry() {
    }

    public static IdentifierGeneratorRegistry getInstance() {
        return INSTANCE;
    }

    public <T> void registerGenerator(
            Class<T> type, IdentifierGenerator<T> generator) {
        generators.put(type, generator);
    }

    public void setUUIDGenerator(IdentifierGenerator<UUID> generator) {
        registerGenerator(UUID.class, generator);
    }

    public void clearRegistry() {
        generators.clear();
    }

    public <T> IdentifierGenerator<T> getIdGenerator(Class<T> type) {
        checkArgument(generators.containsKey(type),
                "Could not find registered generator for type [%s]", type);
        return (IdentifierGenerator<T>) generators.get(type);
    }

}
