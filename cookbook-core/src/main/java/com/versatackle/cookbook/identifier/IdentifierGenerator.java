package com.versatackle.cookbook.identifier;

/**
 * This interface is used to generate unique identifiers.
 *
 * @author Ville Vornanen
 * @param <T> identifier type
 */
public interface IdentifierGenerator<T> {

    /**
     * Returns a unique identifier.
     *
     * @return identifier guaranteed to be unique
     */
    T newId();

}
