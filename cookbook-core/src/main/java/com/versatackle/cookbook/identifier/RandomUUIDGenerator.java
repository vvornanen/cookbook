package com.versatackle.cookbook.identifier;

import java.util.UUID;
import javax.annotation.concurrent.ThreadSafe;

@ThreadSafe
public class RandomUUIDGenerator implements IdentifierGenerator<UUID> {

    @Override
    public UUID newId() {
        return UUID.randomUUID();
    }

}
