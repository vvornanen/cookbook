package com.versatackle.cookbook.recipe.index;

import java.util.UUID;
import org.springframework.data.solr.repository.SolrCrudRepository;

/**
 * Apache Solr repository containing {@link RecipeDocument} objects.
 *
 * @author Ville Vornanen
 */
interface RecipeIndexRepository extends SearchableRecipeIndexRepository,
        SolrCrudRepository<RecipeDocument, UUID> {
}
