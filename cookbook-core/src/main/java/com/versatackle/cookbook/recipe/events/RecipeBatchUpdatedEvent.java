package com.versatackle.cookbook.recipe.events;

import com.versatackle.cookbook.recipe.Recipe;

/**
 * Event which is fired after a batch of recipes have been updated.
 *
 * @author Ville Vornanen
 */
public final class RecipeBatchUpdatedEvent extends BatchRecipeEvent {

    public RecipeBatchUpdatedEvent(Iterable<Recipe> recipes) {
        super(recipes);
    }

}
