package com.versatackle.cookbook.recipe.events;

/**
 * A common supertype for all recipe events.
 *
 * This allows a single listener method to subscribe to all recipe events.
 *
 * @author Ville Vornanen
 */
public abstract class RecipeEvent {
}
