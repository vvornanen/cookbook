package com.versatackle.cookbook.recipe.events;

import com.versatackle.cookbook.recipe.Recipe;

/**
 * Event which is fired after a batch of new recipes have been added to a repository.
 *
 * @author Ville Vornanen
 */
public final class RecipeBatchAddedEvent extends BatchRecipeEvent {

    public RecipeBatchAddedEvent(Iterable<Recipe> recipes) {
        super(recipes);
    }

}
