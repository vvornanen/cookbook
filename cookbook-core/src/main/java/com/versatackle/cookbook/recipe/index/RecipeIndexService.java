package com.versatackle.cookbook.recipe.index;

import com.versatackle.cookbook.core.NotFoundException;
import com.versatackle.cookbook.recipe.Recipe;
import java.util.UUID;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.solr.core.query.result.FacetFieldEntry;
import org.springframework.data.solr.core.query.result.HighlightPage;

/**
 * A service that indexes {@link Recipe} objects for searching.
 *
 * @author Ville Vornanen
 */
public interface RecipeIndexService {

    /**
     * Finds all recipes matching the given search query.
     *
     * @param queryString search query
     * @param pageable pagination information
     * @param sort sorting specification
     * @return page for all matching recipes including highlighting information
     */
    HighlightPage<RecipeDocument> search(
            String queryString, Pageable pageable, Sort sort);

    /**
     * Finds all indexed labels beginning with the given string.
     *
     * @param queryString beginning of a label name
     * @param pageable pagination information
     * @return page for all matching labels
     */
    Page<FacetFieldEntry> findLabels(String queryString, Pageable pageable);

    /**
     * Finds facet information for the given label.
     *
     * @param label exactly matching label name
     * @return facet information for the requested label
     * @throws NotFoundException if the label was not found
     */
    FacetFieldEntry findLabel(String label) throws NotFoundException;

    /**
     * Adds the given {@link Recipe} object to the index.
     *
     * @param recipe recipe to be indexed
     */
    void add(Recipe recipe);

    /**
     * Adds all given {@link Recipe} objects to the index.
     *
     * @param recipes recipes to be indexed
     */
    void addAll(Iterable<Recipe> recipes);

    /**
     * Deletes a {@link Recipe} matching the given identifier from the index.
     *
     * @param id identifier of the recipe to be deleted from the index
     */
    void deleteById(UUID id);

    /**
     * Deletes all given {@link Recipe} objects from the index.
     *
     * @param recipes recipes to be deleted from the index
     */
    void delete(Iterable<Recipe> recipes);

    /**
     * Deletes all existing {@link Recipe} objects from the index.
     */
    void deleteAll();

}
