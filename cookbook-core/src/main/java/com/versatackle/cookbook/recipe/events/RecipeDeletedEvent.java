package com.versatackle.cookbook.recipe.events;

import com.versatackle.cookbook.recipe.Recipe;

/**
 * Event which is fired after a recipe has been deleted from a repository.
 *
 * @author Ville Vornanen
 */
public final class RecipeDeletedEvent extends SingleRecipeEvent {

    public RecipeDeletedEvent(Recipe recipe) {
        super(recipe);
    }

}
