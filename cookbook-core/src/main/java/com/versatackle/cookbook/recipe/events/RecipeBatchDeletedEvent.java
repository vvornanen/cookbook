package com.versatackle.cookbook.recipe.events;

import com.versatackle.cookbook.recipe.Recipe;

/**
 * Event which is fired after a batch of recipes have been deleted from a repository.
 *
 * @author Ville Vornanen
 */
public final class RecipeBatchDeletedEvent extends BatchRecipeEvent {

    public RecipeBatchDeletedEvent(Iterable<Recipe> recipes) {
        super(recipes);
    }

}
