package com.versatackle.cookbook.recipe;

import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Defines a JPA repository interface for Recipes.
 *
 * Spring Data handles the actual repository implementation.
 *
 * @author Ville Vornanen
 */
interface RecipeRepository extends JpaRepository<Recipe, UUID> {
}
