package com.versatackle.cookbook.recipe;

import com.versatackle.cookbook.core.NotFoundException;
import java.util.List;
import java.util.UUID;

/**
 * RecipeService represents the application layer for Recipes.
 *
 * All interaction with Recipes in the upper layers should be done via this
 * interface.
 *
 * @author Ville Vornanen
 */
public interface RecipeService {

    /**
     * Adds the given recipe data to a repository.
     *
     * @param dto recipe data
     * @return the created Recipe instance
     * @throws NullPointerException if {@code dto} is null
     */
    Recipe add(RecipeDTO dto);

    /**
     * Adds all given recipes to a repository.
     *
     * @param dtos recipe data
     * @return all created {@link Recipe} instances
     * @throws NullPointerException if {@code dtos} is null
     */
    List<Recipe> addAll(Iterable<RecipeDTO> dtos);

    /**
     * Removes a recipe from a repository matching the given identifier.
     *
     * @param id identifier of a recipe to be removed
     * @return the removed Recipe instance
     * @throws NullPointerException if {@code id} is null
     * @throws NotFoundException if {@code id} did not match any recipes
     */
    Recipe deleteById(UUID id) throws NotFoundException;

    /**
     * Removes all given recipes from a repository.
     *
     * @param recipes recipes to be removed
     * @throws NullPointerException if {@code recipes} is null
     */
    void delete(Iterable<Recipe> recipes);

    /**
     * Removes all existing recipes from a repository.
     */
    void deleteAll();

    /**
     * Finds all recipes found in a repository.
     *
     * @return all existing recipes
     */
    List<Recipe> findAll();

    /**
     * Finds a recipe matching the given identifier.
     *
     * @param id identifier of a recipe
     * @return the found Recipe instance
     * @throws NotFoundException if {@code id} did not match any recipes
     * @throws NullPointerException if {@code id} is null
     */
    Recipe findById(UUID id) throws NotFoundException;

    /**
     * Updates a recipe with the given recipe data.
     *
     * @param dto recipe data to update
     * @return the updated recipe instance
     * @throws NotFoundException if {@code dto} did not match any recipes
     * @throws NullPointerException if {@code dto} is null
     */
    Recipe update(RecipeDTO dto) throws NotFoundException;

}
