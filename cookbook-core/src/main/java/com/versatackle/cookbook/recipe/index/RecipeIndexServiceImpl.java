package com.versatackle.cookbook.recipe.index;

import com.versatackle.cookbook.core.NotFoundException;
import com.versatackle.cookbook.recipe.Recipe;
import java.util.List;
import static java.util.Objects.requireNonNull;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import javax.annotation.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.solr.core.query.result.FacetFieldEntry;
import org.springframework.data.solr.core.query.result.HighlightPage;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Provides a default RecipeIndexService implementation using Apache Solr repository.
 *
 * @author Ville Vornanen
 */
@Service
class RecipeIndexServiceImpl implements RecipeIndexService {

    private static final Logger LOGGER
            = LoggerFactory.getLogger(RecipeIndexServiceImpl.class);

    @Resource
    private RecipeIndexRepository repository;

    @Transactional(readOnly = true)
    @Override
    public HighlightPage<RecipeDocument> search(
            String queryString, Pageable pageable, Sort sort) {
        return repository.search(queryString, pageable, sort);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<FacetFieldEntry> findLabels(String queryString, Pageable pageable) {
        return repository.findLabelsBeginningWith(queryString, pageable);
    }

    @Override
    public FacetFieldEntry findLabel(String label) throws NotFoundException {
        return repository.findLabel(label);
    }

    @Transactional
    @Override
    public void add(Recipe recipe) {
        requireNonNull(recipe, "Recipe must not be null");
        LOGGER.debug("Indexing recipe {}", recipe);
        repository.save(RecipeDocument.of(recipe));
    }

    @Transactional
    @Override
    public void addAll(Iterable<Recipe> recipes) {
        requireNonNull(recipes, "Recipes must not be null");
        LOGGER.debug("Indexing recipes {}", recipes);
        repository.save(toDocuments(recipes));
    }

    @Transactional
    @Override
    public void deleteById(UUID id) {
        requireNonNull(id);
        LOGGER.debug("Deleting recipe {} from the index", id);
        repository.delete(id);
    }

    @Transactional
    @Override
    public void delete(Iterable<Recipe> recipes) {
        requireNonNull(recipes, "Recipes must not be null");
        LOGGER.debug("Deleting recipes {} from the index", recipes);
        repository.delete(toDocuments(recipes));
    }

    @Transactional
    @Override
    public void deleteAll() {
        LOGGER.debug("Deleting all recipes from the index");
        repository.deleteAll();
    }

    protected static List<RecipeDocument> toDocuments(
            Iterable<Recipe> recipes) {
        return StreamSupport.stream(recipes.spliterator(), false)
                .map(r -> RecipeDocument.of(r))
                .collect(Collectors.toList());
    }
}
