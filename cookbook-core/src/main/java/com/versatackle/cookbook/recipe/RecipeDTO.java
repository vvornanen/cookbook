package com.versatackle.cookbook.recipe;

import com.google.common.base.MoreObjects;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import static java.util.Objects.requireNonNull;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.annotation.CheckForNull;
import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Data transfer object for Recipe.
 *
 * This class contains validation constraints, so it may be used to represent recipes
 * as form objects.
 *
 * RecipeDTO is a value object, i.e. two instances are equal when all their fields
 * are equal.
 *
 * @author Ville Vornanen
 */
@XmlRootElement(name = "recipe")
@XmlAccessorType(XmlAccessType.FIELD)
public final class RecipeDTO {

    public static final String LABEL_SEPARATOR = ",";
    public static final String AUTHOR_SEPARATOR = "\n";

    @XmlAttribute
    private UUID id;

    @NotEmpty
    private String name = "";

    @Length(max = 255)
    private String summary;

    @Length(max = 255)
    private String yield;

    @XmlElement(name = "label")
    private final Set<String> labels = new TreeSet<>();

    @XmlElement(name = "author")
    private List<String> authors = new ArrayList<>();

    @Length(max = 65535)
    private String instructions;

    /**
     * This class is used as a wrapper for JAXB to marshal a list of
     * {@link RecipeDTO} objects.
     */
    @XmlRootElement(name = "recipes")
    public static final class RecipeList {
        @XmlElement(name = "recipe")
        private final List<RecipeDTO> recipes;

        public RecipeList() {
            this.recipes = new ArrayList<>();
        }

        public RecipeList(List<RecipeDTO> recipes) {
            this.recipes = recipes;
        }

        public static RecipeList of(RecipeDTO... recipes) {
            return new RecipeList(Arrays.asList(recipes));
        }

        public List<RecipeDTO> getRecipes() {
            return recipes;
        }
    }

    public static RecipeDTO fromRecipe(Recipe recipe) {
        RecipeDTO dto = new RecipeDTO();
        dto.setId(recipe.getId());
        dto.setName(recipe.getName());
        dto.setSummary(recipe.getSummary());
        dto.setYield(recipe.getYield());
        dto.setLabels(recipe.getLabels().stream()
                .map(Label::toString)
                .collect(Collectors.toSet()));
        dto.setAuthors(recipe.getAuthors());
        dto.setInstructions(recipe.getInstructions());

        return dto;
    }

    /**
     *
     * @return {@code null} if identifier not defined
     */
    public UUID getId() {
        return id;
    }

    public void setId(@Nullable UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    /**
     * Sets the name of the recipe.
     *
     * @param name name of the recipe
     * @throws NullPointerException if {@code name} is null
     */
    public void setName(String name) {
        requireNonNull(name);
        this.name = name;
    }

    public void setSummary(@Nullable String summary) {
        this.summary = summary;
    }

    @CheckForNull
    public String getSummary() {
        return summary;
    }

    public void setYield(@Nullable String yield) {
        this.yield = yield;
    }

    @CheckForNull
    public String getYield() {
        return yield;
    }

    public String getLabels() {
        return labels.stream().collect(Collectors.joining(LABEL_SEPARATOR));
    }

    public Set<String> getLabelSet() {
        return Collections.unmodifiableSet(labels);
    }

    public void setLabels(@Nullable String labels) {
        Iterable<String> result = Splitter.on(LABEL_SEPARATOR)
                .trimResults()
                .omitEmptyStrings()
                .splitToList(Strings.nullToEmpty(labels));
        setLabels(result);
    }

    public void setLabels(Iterable<String> labels) {
        this.labels.clear();
        labels.forEach(this.labels::add);
    }

    public void setAuthors(@Nullable String authors) {
        this.authors = Splitter.on(AUTHOR_SEPARATOR)
                .trimResults()
                .omitEmptyStrings()
                .splitToList(Strings.nullToEmpty(authors));
    }

    public void setAuthors(Iterable<String> authors) {
        this.authors = ImmutableList.copyOf(authors);
    }

    public String getAuthors() {
        return authors.stream().collect(Collectors.joining(AUTHOR_SEPARATOR));
    }

    public List<String> getAuthorsAsList() {
        return authors;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    public String getInstructions() {
        return instructions;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("name", name)
                .toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, summary, yield, labels, authors, instructions);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RecipeDTO other = (RecipeDTO) obj;

        return Objects.equals(this.id, other.id)
                && Objects.equals(this.name, other.name)
                && Objects.equals(this.summary, other.summary)
                && Objects.equals(this.yield, other.yield)
                && Objects.equals(this.labels, other.labels)
                && Objects.equals(this.authors, other.authors)
                && Objects.equals(this.instructions, other.instructions);
    }

}
