package com.versatackle.cookbook.recipe.index;

import com.google.common.base.MoreObjects;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSortedSet;
import com.versatackle.cookbook.recipe.Label;
import com.versatackle.cookbook.recipe.Recipe;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import static java.util.Objects.requireNonNull;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import javax.annotation.CheckForNull;
import javax.annotation.Nullable;
import org.apache.solr.client.solrj.beans.Field;
import org.springframework.data.solr.core.mapping.SolrDocument;

/**
 * Solr document for recipes.
 *
 * @author Ville Vornanen
 */
@SolrDocument(solrCoreName = "recipes")
public class RecipeDocument {
    @Field
    private String id;

    @Field
    private String name;

    @Field
    private String summary;

    @Field
    private String yield;

    @Field("label")
    private List<String> labels = new ArrayList<>();

    @Field("author")
    private List<String> authors = new ArrayList<>();

    @Field
    private String instructions;

    @Field
    private Instant created;

    /**
     * UUID of the user who created the recipe.
     */
    @Field
    private String creator;

    @Field
    private Instant modified;

    /**
     * UUID of the user who last edited the recipe.
     */
    @Field
    private String modifier;

    /**
     * Converts a {@link Recipe} to a new {@link RecipeDocument}.
     *
     * @param recipe recipe to convert
     * @return document representing the given recipe
     */
    public static RecipeDocument of(Recipe recipe) {
        RecipeDocument document = new RecipeDocument();
        document.setId(recipe.getId().toString());
        document.setName(recipe.getName());
        document.setSummary(recipe.getSummary());
        document.setYield(recipe.getYield());
        document.setLabels(recipe.getLabels().stream()
                .map(Label::toString)
                .collect(Collectors.toSet()));
        document.setAuthors(recipe.getAuthors());
        document.setInstructions(recipe.getInstructions());
        document.setCreated(recipe.getCreated());

        if (recipe.getCreator() != null) {
            document.setCreator(recipe.getCreator().getId().toString());
        }

        if (recipe.getModified() != null) {
            document.setModified(recipe.getModified());
        }

        return document;
    }

    public void setId(String id) {
        requireNonNull(id);
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        requireNonNull(name);
        this.name = name;
    }

    @CheckForNull
    public String getSummary() {
        return summary;
    }

    public void setSummary(@Nullable String summary) {
        this.summary = summary;
    }

    @CheckForNull
    public String getYield() {
        return yield;
    }

    public void setYield(@Nullable String yield) {
        this.yield = yield;
    }

    public Set<String> getLabels() {
        return ImmutableSortedSet.copyOf(labels);
    }

    public void setLabels(Iterable<String> labels) {
        requireNonNull(labels);

        this.labels = ImmutableList.copyOf(labels);
    }

    public List<String> getAuthors() {
        return authors;
    }

    public void setAuthors(Iterable<String> authors) {
        requireNonNull(authors);

        ImmutableList.Builder<String> builder = ImmutableList.builder();
        this.authors = builder.addAll(authors).build();
    }

    @CheckForNull
    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(@Nullable String instructions) {
        this.instructions = instructions;
    }

    public Instant getCreated() {
        return created;
    }

    public void setCreated(Instant created) {
        requireNonNull(created);
        this.created = created;
    }

    @CheckForNull
    public String getCreator() {
        return creator;
    }

    public void setCreator(@Nullable String creator) {
        this.creator = creator;
    }

    @CheckForNull
    public Instant getModified() {
        return modified;
    }

    public void setModified(@Nullable Instant modified) {
        this.modified = modified;
    }

    @CheckForNull
    public String getModifier() {
        return modifier;
    }

    public void setModifier(@Nullable String modifier) {
        this.modifier = modifier;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("name", name)
                .toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, summary, yield, labels, authors, instructions,
                created, creator, modified, modifier);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RecipeDocument other = (RecipeDocument) obj;
        if (!java.util.Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!java.util.Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!java.util.Objects.equals(this.summary, other.summary)) {
            return false;
        }
        if (!java.util.Objects.equals(this.yield, other.yield)) {
            return false;
        }
        if (!java.util.Objects.equals(this.labels, other.labels)) {
            return false;
        }
        if (!java.util.Objects.equals(this.authors, other.authors)) {
            return false;
        }
        if (!java.util.Objects.equals(this.instructions, other.instructions)) {
            return false;
        }
        if (!java.util.Objects.equals(this.created, other.created)) {
            return false;
        }
        if (!java.util.Objects.equals(this.creator, other.creator)) {
            return false;
        }
        if (!java.util.Objects.equals(this.modified, other.modified)) {
            return false;
        }
        if (!java.util.Objects.equals(this.modifier, other.modifier)) {
            return false;
        }
        return true;
    }

}
