package com.versatackle.cookbook.recipe;

import com.google.common.collect.Iterables;
import com.google.common.eventbus.EventBus;
import com.versatackle.cookbook.core.NotFoundException;
import com.versatackle.cookbook.recipe.events.AllRecipesDeletedEvent;
import com.versatackle.cookbook.recipe.events.RecipeAddedEvent;
import com.versatackle.cookbook.recipe.events.RecipeBatchAddedEvent;
import com.versatackle.cookbook.recipe.events.RecipeBatchDeletedEvent;
import com.versatackle.cookbook.recipe.events.RecipeDeletedEvent;
import com.versatackle.cookbook.recipe.events.RecipeUpdatedEvent;
import com.versatackle.cookbook.user.User;
import com.versatackle.cookbook.user.UserService;
import java.time.Instant;
import java.util.List;
import static java.util.Objects.requireNonNull;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import javax.annotation.Resource;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Provides a default JPA repository implementation for RecipeService.
 *
 * @author Ville Vornanen
 */
@Service
final class RecipeServiceImpl implements RecipeService {

    @Resource
    private RecipeRepository repository;

    @Resource
    private UserService userService;

    @Resource
    private EventBus eventBus;

    @Transactional
    @Override
    public Recipe add(RecipeDTO dto) {
        requireNonNull(dto);

        Recipe recipe = Recipe.fromDTO(dto);
        recipe.setCreator(getAuthenticatedUser());

        Recipe result = repository.save(recipe);
        eventBus.post(new RecipeAddedEvent(result));

        return result;
    }

    @Transactional
    @Override
    public List<Recipe> addAll(Iterable<RecipeDTO> dtos) {
        requireNonNull(dtos);

        User creator = getAuthenticatedUser();
        List<Recipe> recipes = StreamSupport.stream(dtos.spliterator(), false)
                .map(Recipe::fromDTO)
                .collect(Collectors.toList());
        recipes.forEach(r -> r.setCreator(creator));

        List<Recipe> result = repository.save(recipes);
        eventBus.post(new RecipeBatchAddedEvent(result));

        return result;
    }

    @Transactional(rollbackFor = NotFoundException.class)
    @Override
    public Recipe deleteById(UUID id) throws NotFoundException {
        requireNonNull(id);

        Recipe recipe = findById(id);
        repository.delete(recipe);
        eventBus.post(new RecipeDeletedEvent(recipe));

        return recipe;
    }

    @Transactional
    @Override
    public void delete(Iterable<Recipe> recipes) {
        requireNonNull(recipes);
        repository.delete(recipes);
        eventBus.post(new RecipeBatchDeletedEvent(recipes));
    }

    @Transactional
    @Override
    public void deleteAll() {
        repository.deleteAll();
        eventBus.post(new AllRecipesDeletedEvent());
    }

    @Transactional(readOnly = true)
    @Override
    public List<Recipe> findAll() {
        return repository.findAll();
    }

    @Transactional(readOnly = true)
    @Override
    public Recipe findById(UUID id) throws NotFoundException {
        requireNonNull(id);

        Recipe recipe = repository.findOne(id);

        if (recipe == null) {
            throw newNotFoundException(id);
        }

        return recipe;
    }

    @Transactional(rollbackFor = NotFoundException.class)
    @Override
    public Recipe update(RecipeDTO dto) throws NotFoundException {
        requireNonNull(dto);

        Recipe recipe = findById(dto.getId());
        recipe.setName(dto.getName());
        recipe.setSummary(dto.getSummary());
        recipe.setYield(dto.getYield());

        recipe.clearLabels();
        dto.getLabelSet().forEach(s -> recipe.addLabel(Label.valueOf(s)));
        recipe.clearAuthors();
        recipe.addAuthors(dto.getAuthorsAsList());
        recipe.setInstructions(dto.getInstructions());
        recipe.setModified(Instant.now());
        recipe.setModifier(getAuthenticatedUser());

        Recipe result = repository.save(recipe);
        eventBus.post(new RecipeUpdatedEvent(recipe));

        return result;
    }

    private NotFoundException newNotFoundException(UUID id) {
        return new NotFoundException(String.format(
                "No recipe found with id [%s]", id));
    }

    private User getAuthenticatedUser() {
        Authentication authentication = SecurityContextHolder.getContext()
                .getAuthentication();

        if (authentication == null) {
            return null;
        }

        try {
            return userService.getAuthenticatedUser(authentication);
        } catch (NotFoundException ex) {
            return null;
        }
    }
}
