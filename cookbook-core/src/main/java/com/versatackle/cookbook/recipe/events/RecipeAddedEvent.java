package com.versatackle.cookbook.recipe.events;

import com.versatackle.cookbook.recipe.Recipe;

/**
 * Event which is fired after a new recipe has been added to a repository.
 *
 * @author Ville Vornanen
 */
public final class RecipeAddedEvent extends SingleRecipeEvent {

    public RecipeAddedEvent(Recipe recipe) {
        super(recipe);
    }

}
