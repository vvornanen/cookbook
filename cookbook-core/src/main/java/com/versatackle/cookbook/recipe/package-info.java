@XmlSchema(
        elementFormDefault = XmlNsForm.QUALIFIED,
        namespace = "http://cookbook.versatackle.com/ns/2014/recipe"
)
package com.versatackle.cookbook.recipe;

import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;
