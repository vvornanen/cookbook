package com.versatackle.cookbook.recipe.events;

import com.google.common.base.Objects;
import static com.google.common.base.Preconditions.checkNotNull;
import com.versatackle.cookbook.recipe.Recipe;

/**
 * A supertype for events which are fired after a batch of recipes have been
 * modified.
 *
 * This allows a single listener method to subscribe to all batch recipe events.
 *
 * @author Ville Vornanen
 */
public abstract class BatchRecipeEvent extends RecipeEvent {

    private final Iterable<Recipe> recipes;

    protected BatchRecipeEvent(Iterable<Recipe> recipes) {
        checkNotNull(recipes);
        this.recipes = recipes;
    }

    public final Iterable<Recipe> getRecipes() {
        return this.recipes;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this.getClass())
                .add("recipes", recipes)
                .toString();
    }
}
