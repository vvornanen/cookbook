package com.versatackle.cookbook.recipe.index;

import com.versatackle.cookbook.core.NotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.solr.core.query.result.FacetFieldEntry;
import org.springframework.data.solr.core.query.result.HighlightPage;

/**
 * This interface defines custom search methods for a recipe index repository.
 *
 * @author Ville Vornanen
 */
public interface SearchableRecipeIndexRepository {

    /**
     * Finds all recipes matching the given search query.
     *
     * @param queryString user-entered (edismax) search query
     * @param pageable pagination information
     * @param sort sorting specification
     * @return page for all matching recipes
     */
    HighlightPage<RecipeDocument> search(
            String queryString, Pageable pageable, Sort sort);

    /**
     * Finds all indexed labels beginning with the given string.
     *
     * @param queryString beginning of a label name
     * @param pageable pagination information
     * @return page for all matching labels
     */
    Page<FacetFieldEntry> findLabelsBeginningWith(
            String queryString, Pageable pageable);

    /**
     * Finds facet information for the given label.
     *
     * @param label exactly matching label name
     * @return facet information for the requested label
     * @throws NotFoundException if the label was not found
     */
    FacetFieldEntry findLabel(String label) throws NotFoundException;

}
