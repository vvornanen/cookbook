package com.versatackle.cookbook.recipe.index;

import com.versatackle.cookbook.core.NotFoundException;
import javax.annotation.Resource;
import org.apache.solr.client.solrj.SolrServer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.solr.core.SolrTemplate;
import org.springframework.data.solr.core.convert.SolrJConverter;
import org.springframework.data.solr.core.query.Criteria;
import org.springframework.data.solr.core.query.FacetOptions;
import org.springframework.data.solr.core.query.FacetQuery;
import org.springframework.data.solr.core.query.HighlightOptions;
import org.springframework.data.solr.core.query.SimpleFacetQuery;
import org.springframework.data.solr.core.query.SimpleField;
import org.springframework.data.solr.core.query.SimpleHighlightQuery;
import org.springframework.data.solr.core.query.SimpleQuery;
import org.springframework.data.solr.core.query.SimpleStringCriteria;
import org.springframework.data.solr.core.query.result.FacetFieldEntry;
import org.springframework.data.solr.core.query.result.HighlightPage;
import org.springframework.data.solr.core.query.result.ScoredPage;
import org.springframework.data.solr.core.query.result.SimpleFacetFieldEntry;
import org.springframework.data.solr.server.SolrServerFactory;
import org.springframework.data.solr.server.support.MulticoreSolrServerFactory;
import org.springframework.stereotype.Repository;

/**
 * Implements the custom query methods of {@link SearchableRecipeIndexRepository}.
 *
 * Spring uses this class to create an implementation for
 * {@link RecipeIndexRepository}.
 *
 * @author Ville Vornanen
 */
@Repository
class RecipeIndexRepositoryImpl implements SearchableRecipeIndexRepository {

    protected static final String RECIPES_CORE = "recipes";
    protected static final String DEF_TYPE_EDISMAX = "edismax";
    protected static final String FIELD_ID = "id";
    protected static final String FIELD_NAME = "name";
    protected static final String FIELD_SUMMARY = "summary";
    protected static final String FIELD_LABEL = "label";

    private SolrTemplate solrTemplate;

    /**
     * Accepts a multi-core Solr server and creates a template for the recipes core.
     *
     * The recipes core template cannot be defined as a bean because as of Spring
     * Data Solr 1.1.0 the multi-core support works only when there is only a single
     * template bean defined for the server root.
     *
     * @param solrServer Solr Server instance pointing to the server root
     */
    @Resource
    public void setSolrServer(SolrServer solrServer) {
        SolrServerFactory factory = new MulticoreSolrServerFactory(solrServer);
        solrTemplate = new SolrTemplate(factory);
        solrTemplate.setSolrConverter(new SolrJConverter());
        solrTemplate.setSolrCore(RECIPES_CORE);
        solrTemplate.afterPropertiesSet();
    }

    /**
     * Returns a {@link SolrTemplate} instance for the current core.
     *
     * @return {@link SolrTemplate} instance for the current core
     */
    SolrTemplate getSolrTemplate() {
        return solrTemplate;
    }

    @Override
    public HighlightPage<RecipeDocument> search(
            String queryString, Pageable pageable, Sort sort) {
        HighlightOptions highlightOptions = new HighlightOptions()
                .setSimplePrefix("<strong>")
                .setSimplePostfix("</strong>");

        SimpleHighlightQuery query = new SimpleHighlightQuery(
                new SimpleStringCriteria(queryString), pageable);
        query.addProjectionOnFields(
                FIELD_ID, FIELD_NAME, FIELD_SUMMARY, FIELD_LABEL);
        query.setDefType(DEF_TYPE_EDISMAX);
        query.setHighlightOptions(highlightOptions);
        query.addSort(sort);

        return solrTemplate.queryForHighlightPage(query, RecipeDocument.class);
    }

    @Override
    public Page<FacetFieldEntry> findLabelsBeginningWith(
            String queryString, Pageable pageable) {
        FacetOptions facetOptions = new FacetOptions(FIELD_LABEL)
                .setFacetPrefix(queryString)
                .setPageable(pageable);

        FacetQuery query = new SimpleFacetQuery(allDocuments());
        query.setDefType(DEF_TYPE_EDISMAX);
        query.setFacetOptions(facetOptions);
        query.setRows(0);

        return solrTemplate.queryForFacetPage(query, RecipeDocument.class)
                .getFacetResultPage(FIELD_LABEL);
    }

    @Override
    public FacetFieldEntry findLabel(String label) throws NotFoundException {
        SimpleQuery query = new SimpleQuery(hasLabel(label));
        query.setRows(0);

        ScoredPage<RecipeDocument> result = solrTemplate.queryForPage(
                query, RecipeDocument.class);

        if (result.getTotalElements() == 0L) {
            throw new NotFoundException("Label [%s] was not found", label);
        }

        return new SimpleFacetFieldEntry(
                new SimpleField(FIELD_LABEL),
                label,
                result.getTotalElements());
    }

    private static Criteria hasLabel(String label) {
        return Criteria.where(FIELD_LABEL).is(label);
    }

    private static Criteria allDocuments() {
        return Criteria.where(Criteria.WILDCARD).expression(Criteria.WILDCARD);
    }
}
