package com.versatackle.cookbook.recipe.index;

import com.google.common.eventbus.Subscribe;
import com.versatackle.cookbook.recipe.events.AllRecipesDeletedEvent;
import com.versatackle.cookbook.recipe.events.RecipeAddedEvent;
import com.versatackle.cookbook.recipe.events.RecipeBatchAddedEvent;
import com.versatackle.cookbook.recipe.events.RecipeBatchDeletedEvent;
import com.versatackle.cookbook.recipe.events.RecipeBatchUpdatedEvent;
import com.versatackle.cookbook.recipe.events.RecipeDeletedEvent;
import com.versatackle.cookbook.recipe.events.RecipeUpdatedEvent;
import static java.util.Objects.requireNonNull;
import javax.annotation.Resource;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Service;

/**
 * Listens to recipe modifications and keeps the index up to date.
 *
 * <p>Whenever a {@link RecipeService} posts an event, this handler calls
 * {@link RecipeIndexService} for any required actions so that the index content
 * always matches the actual recipe repository content.</p>
 *
 * <p>Both services are completely decoupled from each other.</p>
 *
 * <p>All indexing actions are run asynchronously by a {@link TaskExecutor} so that
 * the indexing doesn't affect {@link RecipeService}'s responsiveness.</p>
 *
 * @author Ville Vornanen
 */
@Service
class RecipeIndexEventHandler {

    @Resource
    private TaskExecutor executor;

    @Resource
    private RecipeIndexService indexService;

    @Subscribe
    public void onRecipeAdded(final RecipeAddedEvent e) {
        requireNonNull(e);
        executor.execute(() -> indexService.add(e.getRecipe()));
    }

    @Subscribe
    public void onRecipeBatchAdded(final RecipeBatchAddedEvent e) {
        requireNonNull(e);
        executor.execute(() -> indexService.addAll(e.getRecipes()));
    }

    @Subscribe
    public void onRecipeDeleted(final RecipeDeletedEvent e) {
        requireNonNull(e);
        executor.execute(() -> indexService.deleteById(e.getRecipe().getId()));
    }

    @Subscribe
    public void onRecipeBatchDeleted(final RecipeBatchDeletedEvent e) {
        requireNonNull(e);
        executor.execute(() -> indexService.delete(e.getRecipes()));
    }

    @Subscribe
    public void onAllRecipesDeleted(final AllRecipesDeletedEvent e) {
        requireNonNull(e);
        executor.execute(indexService::deleteAll);
    }

    @Subscribe
    public void onRecipeUpdated(final RecipeUpdatedEvent e) {
        requireNonNull(e);
        executor.execute(() -> indexService.add(e.getRecipe()));
    }

    @Subscribe
    public void onRecipeBatchUpdated(final RecipeBatchUpdatedEvent e) {
        requireNonNull(e);
        executor.execute(() -> indexService.addAll(e.getRecipes()));
    }
}
