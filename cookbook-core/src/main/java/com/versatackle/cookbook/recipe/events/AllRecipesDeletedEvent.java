package com.versatackle.cookbook.recipe.events;

/**
 * Event which is fired after all recipes in a repository have been deleted.
 *
 * @author Ville Vornanen
 */
public final class AllRecipesDeletedEvent extends RecipeEvent {
}
