package com.versatackle.cookbook.recipe.events;

import com.versatackle.cookbook.recipe.Recipe;

/**
 * Event which is fired after a recipe has been updated.
 *
 * @author Ville Vornanen
 */
public final class RecipeUpdatedEvent extends SingleRecipeEvent {

    public RecipeUpdatedEvent(Recipe recipe) {
        super(recipe);
    }

}
