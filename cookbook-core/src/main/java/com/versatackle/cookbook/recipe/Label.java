package com.versatackle.cookbook.recipe;

import static com.google.common.base.Preconditions.checkNotNull;
import java.util.Objects;
import javax.annotation.concurrent.Immutable;

/**
 * Label is a non-hierarchical keyword assigned to an entity.
 *
 * @author Ville Vornanen
 */
@Immutable
public final class Label implements Comparable<Label> {
    private final String label;

    private Label(String label) {
        checkNotNull(label, "Label must not be null");
        this.label = label;
    }

    public static Label valueOf(String label) {
        return new Label(label);
    }

    @Override
    public String toString() {
        return label;
    }

    @Override
    public int compareTo(Label o) {
        return this.label.compareTo(o.label);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 47 * hash + Objects.hashCode(this.label);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Label other = (Label) obj;
        if (!Objects.equals(this.label, other.label)) {
            return false;
        }
        return true;
    }
}
