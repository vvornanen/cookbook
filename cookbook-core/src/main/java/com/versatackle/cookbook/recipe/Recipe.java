package com.versatackle.cookbook.recipe;

import com.google.common.base.MoreObjects;
import com.google.common.base.Strings;
import com.versatackle.cookbook.core.AbstractEntity;
import com.versatackle.cookbook.identifier.IdentifierGeneratorRegistry;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.UUID;
import javax.annotation.CheckForNull;
import javax.annotation.Nullable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OrderBy;
import org.owasp.html.CssSchema;
import org.owasp.html.HtmlPolicyBuilder;
import org.owasp.html.PolicyFactory;
import org.owasp.html.Sanitizers;

/**
 * Recipe represents instructions for the preparation of food, meals and drinks.
 *
 * @author Ville Vornanen
 */
@Entity
public class Recipe extends AbstractEntity<UUID> implements Serializable {

    @Id
    private UUID id;

    private String name;

    /**
     * A short plain text overview.
     */
    private String summary;

    private String yield;

    @ElementCollection
    @OrderBy
    private final SortedSet<Label> labels = new TreeSet<>();

    @ElementCollection
    private final List<String> authors = new ArrayList<>();

    @Lob
    @Column(length = 65535)
    private String instructions;

    public Recipe(UUID id, String name) {
        Objects.requireNonNull(name, "Recipe name must not be null");

        if (id == null) {
            this.id = IdentifierGeneratorRegistry.getInstance()
                    .getIdGenerator(UUID.class)
                    .newId();
        } else {
            this.id = id;
        }

        this.name = name;
    }

    protected Recipe() {
    }

    public static Recipe fromDTO(RecipeDTO dto) {
        Recipe recipe = new Recipe(dto.getId(), dto.getName());
        recipe.setSummary(dto.getSummary());
        recipe.setYield(dto.getYield());
        dto.getLabelSet().forEach(label -> recipe.addLabel(Label.valueOf(label)));
        recipe.addAuthors(dto.getAuthorsAsList());
        recipe.setInstructions(dto.getInstructions());

        return recipe;
    }

    private static PolicyFactory getInstructionsPolicyFactory() {
        return new HtmlPolicyBuilder()
                .allowCommonBlockElements()
                .allowCommonInlineFormattingElements()
                .allowStandardUrlProtocols()
                .allowElements("em", "img")
                .allowAttributes("style", "src").onElements("img")
                .allowStyling(CssSchema.union(
                        CssSchema.DEFAULT,
                        CssSchema.withProperties(Arrays.asList("float"))))
                .requireRelNofollowOnLinks()
                .toFactory()
                .and(Sanitizers.LINKS);
    }

    @Override
    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        Objects.requireNonNull(name, "Recipe name must not be null");
        this.name = name;
    }

    public void setSummary(@Nullable String summary) {
        this.summary = Strings.emptyToNull(summary);
    }

    @CheckForNull
    public String getSummary() {
        return summary;
    }

    public void setYield(@Nullable String yield) {
        this.yield = Strings.emptyToNull(yield);
    }

    @CheckForNull
    public String getYield() {
        return yield;
    }

    /**
     * Adds the given label to this recipe.
     *
     * @param label label to be added to this recipe
     * @return {@code true} if this recipe did not already contain the given label
     */
    public boolean addLabel(Label label) {
        Objects.requireNonNull(label, "Label must not be null");
        return labels.add(label);
    }

    /**
     * Removes the given label from this recipe.
     *
     * @param label label to be removed from this recipe
     * @return {@code true} if this recipe contained the given label
     */
    public boolean removeLabel(Label label) {
        Objects.requireNonNull(label, "Label must not be null");
        return labels.remove(label);
    }

    /**
     * Removes all labels from this recipe.
     */
    public void clearLabels() {
        labels.clear();
    }

    /**
     * Adds all of the given labels to this recipe.
     *
     * @param labels collection containing label to be added to this recipe
     * @return {@code true} if this recipe changed as a result of the call
     */
    public boolean addLabels(Collection<Label> labels) {
        return this.labels.addAll(labels);
    }

    /**
     * Returns all labels contained in this recipe.
     *
     * @return an unmodifiable view of this recipe's labels
     */
    public Set<Label> getLabels() {
        return Collections.unmodifiableSet(labels);
    }

    /**
     * Adds the given author to this recipe.
     *
     * @param name author's name
     */
    public void addAuthor(String name) {
        authors.add(name);
    }

    /**
     * Removes the first occurrence of the given author from this recipe.
     *
     * @param name author's name
     * @return {@code true} if this recipe contained the given author
     */
    public boolean removeAuthor(String name) {
        return authors.remove(name);
    }

    /**
     * Appends all given authors to this recipe.
     *
     * @param names list of authors' names
     */
    public void addAuthors(Collection<String> names) {
        authors.addAll(names);
    }

    /**
     * Removes all authors from this recipe.
     */
    public void clearAuthors() {
        authors.clear();
    }

    /**
     * Returns all authors of this recipe.
     *
     * @return an unmodifiable list of authors or an empty list if authors not
     *         defined
     */
    public List<String> getAuthors() {
        return Collections.unmodifiableList(authors);
    }

    public void setInstructions(@Nullable String instructions) {
        this.instructions = Optional.ofNullable(instructions)
                .map(s -> Strings.emptyToNull(sanitize(s)))
                .orElse(null);
    }

    /**
     * Returns HTML for recipe's instructions.
     *
     * @return HTML sanitized using a whitelist of allowed elements and attributes
     */
    @CheckForNull
    public String getInstructions() {
        return this.instructions;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("name", name)
                .toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (getClass() != obj.getClass()) {
            return false;
        }

        final Recipe other = (Recipe) obj;

        return Objects.equals(id, other.id);
    }

    @CheckForNull
    private String sanitize(String html) {
        assert html != null: "HTML string must not be null";
        return getInstructionsPolicyFactory().sanitize(html);
    }

}
