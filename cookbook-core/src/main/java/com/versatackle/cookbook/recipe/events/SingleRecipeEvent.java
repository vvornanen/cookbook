package com.versatackle.cookbook.recipe.events;

import com.google.common.base.Objects;
import static com.google.common.base.Preconditions.checkNotNull;
import com.versatackle.cookbook.recipe.Recipe;

/**
 * A supertype for events which are fired after a single recipe has been modified.
 *
 * This allows a single listener method to subscribe to all single recipe events.
 *
 * @author Ville Vornanen
 */
public abstract class SingleRecipeEvent extends RecipeEvent {

    private final Recipe recipe;

    protected SingleRecipeEvent(Recipe recipe) {
        checkNotNull(recipe);
        this.recipe = recipe;
    }

    public final Recipe getRecipe() {
        return recipe;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this.getClass())
                .add("recipe", recipe)
                .toString();
    }
}
