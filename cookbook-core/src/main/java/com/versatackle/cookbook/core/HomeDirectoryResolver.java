package com.versatackle.cookbook.core;

import com.google.common.base.Optional;
import java.io.File;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Resolves Cookbook home directory from a system property or an environment
 * variable.
 *
 * @author Ville Vornanen
 */
public final class HomeDirectoryResolver {
    private static final Logger LOGGER
            = LoggerFactory.getLogger(HomeDirectoryResolver.class);
    private static final String HOME_DIR_SYSTEM_PROPERTY = "cookbook.home";
    private static final String HOME_DIR_ENV_VARIABLE = "COOKBOOK_HOME";

    public File resolve() {
        File result = new File(doResolve()).getAbsoluteFile();
        LOGGER.info("Cookbook home directory resolved to {}", result);

        return result;
    }

    private String doResolve() {
        try {
            String home = getSystemProperty()
                    .or(getEnvironmentVariable())
                    .get();
            setSystemPropertyIfAbsent(home);

            return home;
        } catch (IllegalStateException ex) {
            throw new IllegalArgumentException(String.format(
                    "Home directory [%s] not defined", HOME_DIR_SYSTEM_PROPERTY));
        }
    }

    private Optional<String> getSystemProperty() {
        return Optional.fromNullable(System.getProperty(HOME_DIR_SYSTEM_PROPERTY));
    }

    private Optional<String> getEnvironmentVariable() {
        return Optional.fromNullable(System.getenv(HOME_DIR_ENV_VARIABLE));
    }

    private void setSystemPropertyIfAbsent(String home) {
        if (!System.getProperties().containsKey(HOME_DIR_SYSTEM_PROPERTY)) {
            System.setProperty(HOME_DIR_SYSTEM_PROPERTY, home);
        }
    }
}
