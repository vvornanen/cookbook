package com.versatackle.cookbook.core;

import com.versatackle.cookbook.user.User;
import java.time.Instant;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * A common interface for all entities.
 *
 * @author Ville Vornanen
 * @param <ID> entity identifier type
 */
public interface Entity<ID> {

    /**
     * Returns the entity's identifier.
     *
     * @return entity's identifier, i.e. the primary key
     */
    @Nonnull
    public ID getId();

    /**
     * Returns timestamp when the entity was created.
     *
     * @return timestamp guaranteed to be not null
     */
    @Nonnull
    public Instant getCreated();

    /**
     * Returns a user who created the entity.
     *
     * @return user or null if not defined
     */
    @Nullable
    public User getCreator();

    /**
     * Returns timestamp when the entity was last modified.
     *
     * @return timestamp or null if not modified since its creation
     */
    @Nullable
    public Instant getModified();

    /**
     * Returns a user who last modified the entity.
     *
     * @return user or null if not defined
     */
    @Nullable
    public User getModifier();

    /**
     * Returns a version number used for optimistic locks
     *
     * To update an entity in a repository, the entity's version must be equal to the
     * version in the repository.
     *
     * @return entity's version or null if entity does not exist in a repository.
     */
    @Nullable
    public Long getVersion();
}
