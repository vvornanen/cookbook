package com.versatackle.cookbook.core;

import static com.google.common.base.Preconditions.checkNotNull;
import com.versatackle.cookbook.user.User;
import java.time.Instant;
import javax.annotation.CheckForNull;
import javax.annotation.Nullable;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

/**
 * An abstract superclass for all entities, implementing the common methods.
 *
 * @author Ville Vornanen
 * @param <ID> entity identifier type
 */
@MappedSuperclass
public abstract class AbstractEntity<ID> implements Entity<ID> {

    /**
     * The version field of an entity serves as its optimistic lock value.
     */
    @Version
    private Long version = 0L;

    @Column(nullable = false)
    private Instant created;

    @ManyToOne(fetch = FetchType.LAZY)
    private User creator;

    private Instant modified;

    @ManyToOne(fetch = FetchType.LAZY)
    private User modifier;

    protected AbstractEntity() {
        created = Instant.now();
    }

    protected AbstractEntity(Instant created) {
        this.created = checkNotNull(created, "Created timestamp is not nullable");
    }

    @Override
    public Instant getCreated() {
        return created;
    }

    public void setCreated(Instant created) {
        this.created = checkNotNull(created, "Created timestamp is not nullable");
    }

    @Override
    @CheckForNull
    public User getCreator() {
        return this.creator;
    }

    public void setCreator(@Nullable User creator) {
        this.creator = creator;
    }

    @Override
    @CheckForNull
    public Instant getModified() {
        return modified;
    }

    public void setModified(@Nullable Instant modified) {
        this.modified = modified;
    }

    @Override
    @CheckForNull
    public User getModifier() {
        return this.modifier;
    }

    public void setModifier(@Nullable User modifier) {
        this.modifier = modifier;
    }

    @Override
    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = checkNotNull(version, "Version is not nullable");
    }

}
