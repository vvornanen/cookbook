package com.versatackle.cookbook.core;

/**
 * This exception is thrown by a service if requested entity is not found.
 *
 * @author Ville Vornanen
 */
public final class NotFoundException extends Exception {

    private static final long serialVersionUID = 9875639481L;

    /**
     * Creates a new instance of <code>NotFoundException</code> without detail message.
     */
    public NotFoundException() {
    }

    /**
     * Constructs an instance of <code>NotFoundException</code> with the specified detail message.
     * @param msg the detail message.
     */
    public NotFoundException(String msg) {
        super(msg);
    }

    /**
     * A convenience constructor for formatting the message with arguments.
     *
     * <p>Calls {@link String#format(java.lang.String, java.lang.Object...)} to
     * format the message</p>
     *
     * @param message
     * @param args
     */
    public NotFoundException(String message, Object... args) {
        super(String.format(message, args));
    }

}
