package com.versatackle.cookbook.user;

import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

/**
 * Defines a JPA repository interface for users.
 *
 * Spring Data handles the actual repository implementation.
 *
 * @author Ville Vornanen
 */
interface UserRepository
        extends JpaRepository<User, UUID>, QueryDslPredicateExecutor<User> {}
