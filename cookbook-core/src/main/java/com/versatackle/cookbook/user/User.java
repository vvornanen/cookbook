package com.versatackle.cookbook.user;

import com.google.common.base.MoreObjects;
import com.versatackle.cookbook.core.AbstractEntity;
import com.versatackle.cookbook.identifier.IdentifierGeneratorRegistry;
import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;
import static java.util.Objects.requireNonNull;
import java.util.Optional;
import java.util.UUID;
import javax.annotation.Nullable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

@Entity
@Table(name = "users")
public class User extends AbstractEntity<UUID> implements UserDetails {

    @Id
    private UUID id;

    @Column(unique = true)
    private String username;

    private String password;

    private String name;

    public User(@Nullable UUID id, String username, String name) {
        requireNonNull(username, "Username must not be null");
        requireNonNull(name, "User's name must not be null");

        if (id == null) {
            this.id = IdentifierGeneratorRegistry.getInstance()
                .getIdGenerator(UUID.class)
                .newId();
        } else {
            this.id = id;
        }

        this.username = username;
        this.name = name;
    }

    public User(String username, String name) {
        requireNonNull(username);
        requireNonNull(name);

        id = IdentifierGeneratorRegistry.getInstance()
                .getIdGenerator(UUID.class)
                .newId();
        this.username = username;
        this.name = name;
    }

    protected User() {
    }

    public static User fromDTO(UserDTO dto) {
        requireNonNull(dto);

        User user = new User(dto.getId(), dto.getUsername(), dto.getName());
        user.setPassword(dto.getPassword());

        return user;
    }

    @Override
    public UUID getId() {
        return this.id;
    }

    public void setUsername(String username) {
        requireNonNull(username, "Username must not be null");
        this.username = username;
    }

    @Override
    public String getUsername() {
        return username;
    }

    public void setPassword(@Nullable String password) {
        this.password = password;
    }

    @Nullable
    @Override
    public String getPassword() {
        return password;
    }

    public void setName(String name) {
        requireNonNull(name, "User's name must not be null");
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Arrays.asList(new SimpleGrantedAuthority("user"));
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("username", username)
                .toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final User other = (User) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }
}
