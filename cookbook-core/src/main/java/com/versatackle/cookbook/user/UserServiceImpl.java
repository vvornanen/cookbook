package com.versatackle.cookbook.user;

import com.versatackle.cookbook.core.NotFoundException;
import java.time.Instant;
import java.util.List;
import static java.util.Objects.requireNonNull;
import java.util.UUID;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Provides a default JPA repository implementation for {@link UserService}.
 *
 * @author Ville Vornanen
 */
@Service
class UserServiceImpl implements UserService {

    private static final Logger LOGGER
            = LoggerFactory.getLogger(UserServiceImpl.class);

    @Inject
    private UserRepository repository;

    @Transactional
    @Override
    public User add(UserDTO dto) {
        requireNonNull(dto);

        User user = User.fromDTO(dto);
        user.setCreator(getAuthenticatedUser());

        return repository.save(user);
    }

    @Transactional(rollbackFor = NotFoundException.class)
    @Override
    public User deleteById(UUID id) throws NotFoundException {
        requireNonNull(id);

        User user = findById(id);
        repository.delete(user);

        return user;
    }

    @Transactional(readOnly = true)
    @Override
    public List<User> findAll() {
        return repository.findAll();
    }

    @Transactional(readOnly = true)
    @Override
    public User findById(UUID id) throws NotFoundException {
        requireNonNull(id);

        User user = repository.findOne(id);

        if (user == null) {
            handleUserNotFound(id);
        }

        return user;
    }

    @Transactional(readOnly = true)
    @Override
    public boolean exists(UUID id) {
        return repository.exists(id);
    }

    @Transactional(rollbackFor = NotFoundException.class)
    @Override
    public User update(UserDTO dto) throws NotFoundException {
        requireNonNull(dto);

        User user = findById(dto.getId());

        if (dto.getUsername() != null) {
            user.setUsername(dto.getUsername());
        }

        if (dto.getPassword() != null) {
            user.setPassword(dto.getPassword());
        }

        if (dto.getName() != null) {
            user.setName(dto.getName());
        }

        user.setModified(Instant.now());
        user.setModifier(getAuthenticatedUser());

        return user;
    }

    @Transactional(readOnly = true)
    @Override
    public UserDetails loadUserByUsername(String username)
            throws UsernameNotFoundException {
        User user = repository.findOne(UserPredicates.usernameEquals(username));

        if (user == null) {
            handleUserNotFound(username);
        }

        return user;
    }

    private void handleUserNotFound(UUID id) throws NotFoundException {
        throw new NotFoundException(
                String.format("No user found with id [%s]", id));
    }

    private void handleUserNotFound(String username)
            throws UsernameNotFoundException {
        throw new UsernameNotFoundException(
                String.format("Username [%s] not found", username));
    }

    private User getAuthenticatedUser() {
        Authentication authentication = SecurityContextHolder.getContext()
                .getAuthentication();

        if (authentication == null) {
            return null;
        }

        try {
            return getAuthenticatedUser(authentication);
        } catch (NotFoundException ex) {
            return null;
        }
    }

    @Transactional(readOnly = true)
    @Override
    public User getAuthenticatedUser(Authentication authentication)
            throws NotFoundException {
        requireNonNull(authentication, "Authentication token must not be null");

        if (!(authentication.getPrincipal() instanceof User)) {
            throw new NotFoundException(String.format(
                    "Unknown principal [%s]",
                    authentication.getPrincipal().toString()));
        }

        User user = (User) authentication.getPrincipal();
        LOGGER.debug("Authenticated user is: {}", user);

        try {
            return findById(user.getId());
        } catch (NotFoundException ex) {
            LOGGER.warn("User was authenticated but not found", ex);
            throw ex;
        }
    }
}
