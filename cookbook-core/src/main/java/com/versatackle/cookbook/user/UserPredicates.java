package com.versatackle.cookbook.user;

import com.mysema.query.types.Predicate;

/**
 * Contains predicate factory methods for {@link User}.
 *
 * Class QUser is generated automatically by {@code apt-maven-plugin}.
 *
 * @author Ville Vornanen
 */
class UserPredicates {

    /**
     * Case-insensitive search by username.
     *
     * @param username
     * @return the created username predicate
     */
    public static Predicate usernameEquals(String username) {
        return QUser.user.username.equalsIgnoreCase(username);
    }
}
