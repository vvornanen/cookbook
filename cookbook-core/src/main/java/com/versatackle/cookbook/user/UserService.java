package com.versatackle.cookbook.user;

import com.versatackle.cookbook.core.NotFoundException;
import java.util.List;
import java.util.UUID;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * RecipeService represents the application layer for users.
 *
 * All interaction with users in the upper layers should be done via this
 * interface.
 *
 * @author Ville Vornanen
 */
public interface UserService extends UserDetailsService {

    /**
     * Adds the given user data to a repository.
     *
     * @param dto user data
     * @return the created User instance
     * @throws NullPointerException if {@code dto} is null
     */
    User add(UserDTO dto);

    /**
     * Removes a user from a repository matching the given identifier.
     *
     * @param id identifier of a user to be removed
     * @return the removed User instance
     * @throws NullPointerException if {@code id} is null
     * @throws NotFoundException if {@code id} did not match any users
     */
    User deleteById(UUID id) throws NotFoundException;

    /**
     * Finds all users found in a repository.
     *
     * @return all existing users
     */
    List<User> findAll();

    /**
     * Finds a user matching the given identifier.
     *
     * @param id identifier of a user
     * @return the found User instance
     * @throws NotFoundException if {@code id} did not match any users
     * @throws NullPointerException if {@code id} is null
     */
    User findById(UUID id) throws NotFoundException;

    /**
     * Returns whether a user with the given id exists.
     *
     * @param id user identifier
     * @return true if a user with the given id exists, otherwise false
     */
    boolean exists(UUID id);

    /**
     * Updates a user with the given user data.
     *
     * @param dto user data to update
     * @return the updated user instance
     * @throws NotFoundException if {@code dto} did not match any users
     * @throws NullPointerException if {@code dto} is null
     */
    User update(UserDTO dto) throws NotFoundException;

    /**
     * Returns a user matching the given authentication.
     *
     * @param authentication user authentication information
     * @return user matching the authentication
     * @throws NotFoundException if did not found any users matching the principal
     */
    User getAuthenticatedUser(Authentication authentication)
            throws NotFoundException;
}
