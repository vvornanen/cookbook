package com.versatackle.cookbook.user;

import com.google.common.base.MoreObjects;
import static java.util.Objects.requireNonNull;
import java.util.UUID;
import javax.annotation.Nullable;
import org.hibernate.validator.constraints.NotEmpty;

public class UserDTO {

    private UUID id;

    @NotEmpty
    private String username;

    private String password;

    @NotEmpty
    private String name;

    public static UserDTO fromUser(User user) {
        requireNonNull(user);

        UserDTO dto = new UserDTO();
        dto.setId(user.getId());
        dto.setUsername(user.getUsername());
        dto.setPassword(user.getPassword());
        dto.setName(user.getName());

        return dto;
    }

    /**
     *
     * @return {@code null} if identifier not defined
     */
    public UUID getId() {
        return id;
    }

    public void setId(@Nullable UUID id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("username", username)
                .toString();
    }
}
