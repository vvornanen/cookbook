package com.versatackle.cookbook.persistence;

import org.hibernate.cfg.Configuration;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.integrator.spi.Integrator;
import org.hibernate.metamodel.source.MetadataImplementor;
import org.hibernate.service.spi.SessionFactoryServiceRegistry;
import org.hibernate.usertype.UserType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Registers custom user types using Hibernate Integrator SPI.
 *
 * @author Ville Vornanen
 */
public class UserTypeIntegrator implements Integrator {

    private static final Logger LOGGER
            = LoggerFactory.getLogger(UserTypeIntegrator.class);

    @Override
    public void integrate(Configuration configuration,
            SessionFactoryImplementor sessionFactory,
            SessionFactoryServiceRegistry serviceRegistry) {
        for (UserType type: getUserTypes()) {
            registerType(configuration, type);
        }
    }

    @Override
    public void integrate(MetadataImplementor metadata,
            SessionFactoryImplementor sessionFactory,
            SessionFactoryServiceRegistry serviceRegistry) {
    }

    @Override
    public void disintegrate(SessionFactoryImplementor sessionFactory,
            SessionFactoryServiceRegistry serviceRegistry) {
    }

    private void registerType(Configuration configuration, UserType type) {
        String className = type.returnedClass().getName();

        LOGGER.debug("Registering type override for {}", className);

        configuration.registerTypeOverride(type, new String[] {className});
    }

    private UserType[] getUserTypes() {
        return new UserType[] {
            PersistentLabel.INSTANCE
        };
    }
}
