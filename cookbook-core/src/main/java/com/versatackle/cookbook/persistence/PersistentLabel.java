package com.versatackle.cookbook.persistence;

import com.versatackle.cookbook.recipe.Label;
import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Objects;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.type.StringType;
import org.hibernate.usertype.UserType;

/**
 * This class defines a {@link UserType} for Label because
 * {@link javax.persistence.Embeddable} annotation cannot be used with immutable
 * classes.
 *
 * @author Ville Vornanen
 */
public class PersistentLabel implements UserType {

    public static final PersistentLabel INSTANCE = new PersistentLabel();

    @Override
    public int[] sqlTypes() {
        return new int[] {
            Types.VARCHAR
        };
    }

    @Override
    public Class returnedClass() {
        return Label.class;
    }

    @Override
    public boolean equals(Object x, Object y) throws HibernateException {
        return Objects.equals(x, y);
    }

    @Override
    public int hashCode(Object x) throws HibernateException {
        return Objects.hashCode(x);
    }

    @Override
    public Object nullSafeGet(ResultSet rs, String[] names,
            SessionImplementor session, Object owner)
            throws HibernateException, SQLException {
        String value = StringType.INSTANCE.nullSafeGet(rs, names[0], session);
        return value != null ? Label.valueOf(value) : null;
    }

    @Override
    public void nullSafeSet(PreparedStatement st, Object value, int index,
            SessionImplementor session) throws HibernateException, SQLException {
        StringType.INSTANCE.nullSafeSet(st, Objects.toString(value), index, session);
    }

    @Override
    public Object deepCopy(Object value) throws HibernateException {
        return value;
    }

    @Override
    public boolean isMutable() {
        return false;
    }

    @Override
    public Serializable disassemble(Object value) throws HibernateException {
        return (Serializable) value;
    }

    @Override
    public Object assemble(Serializable cached, Object owner)
            throws HibernateException {
        return cached;
    }

    @Override
    public Object replace(Object original, Object target, Object owner)
            throws HibernateException {
        return original;
    }

}
