/**
 * Includes type definitions for custom UserTypes.
 *
 * These explicit type definitions are required to get Hibernate UserTypes work with
 * JPA annotations, since the annotations are processed before
 * {@link UserTypeIntegrator} is run.
 */
@TypeDefs({
    @TypeDef(defaultForType = Label.class, typeClass = PersistentLabel.class)
})
package com.versatackle.cookbook.persistence;

import com.versatackle.cookbook.recipe.Label;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
