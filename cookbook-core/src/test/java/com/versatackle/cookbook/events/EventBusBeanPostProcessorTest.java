package com.versatackle.cookbook.events;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Unit test for {@link EventBusBeanPostProcessor}.
 *
 * @author Ville Vornanen
 */
public class EventBusBeanPostProcessorTest {

    @Mock
    private EventBus eventBusMock;

    @InjectMocks
    private EventBusBeanPostProcessor processor;

    private static class TestListener {

        @Subscribe
        public void onEvent(Object event) {
        }

    }

    @BeforeMethod(alwaysRun = true)
    public void setUp() {
        processor = new EventBusBeanPostProcessor();
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testRegisterSubscriber() {
        TestListener bean = new TestListener();
        processor.postProcessAfterInitialization(bean, null);

        verify(eventBusMock).register(bean);
    }

    @Test
    public void testRegisterAlreadyRegistered() {
        TestListener bean = new TestListener();
        processor.postProcessAfterInitialization(bean, null);
        processor.postProcessAfterInitialization(bean, null);

        verify(eventBusMock).register(bean);
        verifyNoMoreInteractions(eventBusMock);
    }

    @Test
    public void testRegisterNotSubscriber() {
        processor.postProcessAfterInitialization(new Object(), null);

        verifyZeroInteractions(eventBusMock);
    }

    @Test
    public void testUnregisterSubscriber() {
        TestListener bean = new TestListener();
        processor.postProcessAfterInitialization(bean, null);
        processor.postProcessBeforeDestruction(bean, null);

        verify(eventBusMock).register(bean);
        verify(eventBusMock).unregister(bean);
        verifyNoMoreInteractions(eventBusMock);
    }

    @Test
    public void testUnregisterAlreadyUnregistered() {
        TestListener bean = new TestListener();
        processor.postProcessAfterInitialization(bean, null);
        processor.postProcessBeforeDestruction(bean, null);
        processor.postProcessBeforeDestruction(bean, null);

        verify(eventBusMock).register(bean);
        verify(eventBusMock).unregister(bean);
        verifyNoMoreInteractions(eventBusMock);
    }

    @Test
    public void testUnregisterNotSubscriber() {
        Object object = new Object();
        processor.postProcessAfterInitialization(object, null);
        processor.postProcessBeforeDestruction(object, null);

        verifyZeroInteractions(eventBusMock);
    }

}
