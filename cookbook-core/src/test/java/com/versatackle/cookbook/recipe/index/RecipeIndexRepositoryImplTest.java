package com.versatackle.cookbook.recipe.index;

import com.versatackle.cookbook.core.NotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import static org.hamcrest.MatcherAssert.assertThat;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.*;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.solr.core.SolrTemplate;
import org.springframework.data.solr.core.query.Criteria;
import org.springframework.data.solr.core.query.FacetQuery;
import org.springframework.data.solr.core.query.Field;
import org.springframework.data.solr.core.query.HighlightQuery;
import org.springframework.data.solr.core.query.Query;
import org.springframework.data.solr.core.query.SimpleStringCriteria;
import org.springframework.data.solr.core.query.result.FacetFieldEntry;
import org.springframework.data.solr.core.query.result.FacetPage;
import org.springframework.data.solr.core.query.result.HighlightPage;
import org.springframework.data.solr.core.query.result.ScoredPage;
import static org.testng.Assert.*;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Unit test for {@link RecipeIndexRepositoryImpl}.
 *
 * @author Ville Vornanen
 */
public class RecipeIndexRepositoryImplTest {

    private static final String QUERY_STRING = "foo";
    private static final Pageable PAGE_REQUEST = new PageRequest(0, 1);
    private static final Sort SORT = new Sort("name");

    @Mock
    private SolrTemplate solrTemplateMock;

    @InjectMocks
    private RecipeIndexRepositoryImpl repository;

    @BeforeMethod
    public void setUp() {
        repository = new RecipeIndexRepositoryImpl();
        // Don't want to init mocks here for testSetSolrServer()
    }

    @Test
    public void testSetSolrServer() {
        HttpSolrServer server = new HttpSolrServer("http://example.com");
        HttpSolrServer expected = new HttpSolrServer("http://example.com/recipes");
        repository.setSolrServer(server);

        assertSolrServerEquals(repository.getSolrTemplate().getSolrServer(),
                expected);
        assertEquals(repository.getSolrTemplate().getSolrCore(),
                RecipeIndexRepositoryImpl.RECIPES_CORE);
    }

    @Test
    public void testSearchQueryEqualsExpected() {
        MockitoAnnotations.initMocks(this);

        repository.search(QUERY_STRING, PAGE_REQUEST, SORT);

        ArgumentCaptor<HighlightQuery> argumentCaptor
                = ArgumentCaptor.forClass(HighlightQuery.class);
        verify(solrTemplateMock, times(1)).queryForHighlightPage(
                argumentCaptor.capture(), eq(RecipeDocument.class));
        verifyNoMoreInteractions(solrTemplateMock);

        assertHighlightQuery(argumentCaptor.getValue());
    }

    @Test
    public void testSearchReturnsPage() {
        MockitoAnnotations.initMocks(this);

        HighlightPage<RecipeDocument> expected = Mockito.mock(HighlightPage.class);

        when(solrTemplateMock.queryForHighlightPage(
                any(HighlightQuery.class), eq(RecipeDocument.class)))
                .thenReturn(expected);
        HighlightPage<RecipeDocument> actual = repository.search(
                QUERY_STRING, PAGE_REQUEST, SORT);

        assertSame(actual, expected);
    }

    @Test
    public void testFindLabelsQueryEqualsExpected() {
        MockitoAnnotations.initMocks(this);

        @SuppressWarnings("unchecked")
        FacetPage<RecipeDocument> page = mock(FacetPage.class);

        when(solrTemplateMock.queryForFacetPage(any(FacetQuery.class),
                eq(RecipeDocument.class)))
                .thenReturn(page);

        repository.findLabelsBeginningWith(QUERY_STRING, PAGE_REQUEST);

        ArgumentCaptor<FacetQuery> argumentCaptor
                = ArgumentCaptor.forClass(FacetQuery.class);
        verify(solrTemplateMock).queryForFacetPage(
                argumentCaptor.capture(), eq(RecipeDocument.class));
        verifyNoMoreInteractions(solrTemplateMock);

        assertFacetQuery(argumentCaptor.getValue());
    }

    @Test
    public void testFindLabelsReturnsExpectedResult() {
        MockitoAnnotations.initMocks(this);

        @SuppressWarnings("unchecked")
        Page<FacetFieldEntry> expected = mock(Page.class);

        @SuppressWarnings("unchecked")
        FacetPage<RecipeDocument> facetPageMock = mock(FacetPage.class);

        when(facetPageMock.getFacetResultPage(any(String.class)))
                .thenReturn(expected);

        when(solrTemplateMock.queryForFacetPage(any(FacetQuery.class),
                eq(RecipeDocument.class)))
                .thenReturn(facetPageMock);

        Page<FacetFieldEntry> actual = repository.findLabelsBeginningWith(
                QUERY_STRING, PAGE_REQUEST);

        assertSame(actual, expected);
    }

    @Test
    public void testFindLabelQueryEqualsExpected() throws Exception {
        MockitoAnnotations.initMocks(this);

        String label = "test";

        @SuppressWarnings("unchecked")
        ScoredPage<RecipeDocument> page = mock(ScoredPage.class);

        when(page.getTotalElements()).thenReturn(1L);

        when(solrTemplateMock.queryForPage(
                any(Query.class), eq(RecipeDocument.class)))
                .thenReturn(page);

        repository.findLabel(label);

        ArgumentCaptor<Query> argumentCaptor = ArgumentCaptor.forClass(Query.class);

        verify(solrTemplateMock).queryForPage(
                argumentCaptor.capture(), eq(RecipeDocument.class));


        Query actual = argumentCaptor.getValue();

        Criteria expectedCriteria = Criteria
                .where(RecipeIndexRepositoryImpl.FIELD_LABEL).is(label);
        assertThat(actual.getCriteria(), CriteriaMatchers.eq(expectedCriteria));
        assertEquals(actual.getRows(), Integer.valueOf(0));
    }

    @Test
    public void testFindLabelReturnsExpectedResult() throws Exception {
        MockitoAnnotations.initMocks(this);

        String label = "test";
        long numberOfRecipes = 5L;

        @SuppressWarnings("unchecked")
        ScoredPage<RecipeDocument> page = mock(ScoredPage.class);

        when(page.getTotalElements()).thenReturn(numberOfRecipes);

        when(solrTemplateMock.queryForPage(
                any(Query.class), eq(RecipeDocument.class)))
                .thenReturn(page);

        FacetFieldEntry result = repository.findLabel(label);

        assertEquals(result.getField().getName(),
                RecipeIndexRepositoryImpl.FIELD_LABEL);
        assertEquals(result.getValue(), label);
        assertEquals(result.getValueCount(), numberOfRecipes);
    }

    @Test(expectedExceptions = NotFoundException.class)
    public void testFindLabelNotFound() throws Exception {
        MockitoAnnotations.initMocks(this);

        String label = "doesnotexist";

        @SuppressWarnings("unchecked")
        ScoredPage<RecipeDocument> page = mock(ScoredPage.class);

        when(page.getTotalElements()).thenReturn(0L);

        when(solrTemplateMock.queryForPage(
                any(Query.class), eq(RecipeDocument.class)))
                .thenReturn(page);

        repository.findLabel(label);
    }

    private void assertSolrServerEquals(SolrServer actual, HttpSolrServer expected) {
        assertTrue(actual instanceof HttpSolrServer);
        assertEquals(((HttpSolrServer) actual).getBaseURL(), expected.getBaseURL());
    }

    private void assertHighlightQuery(HighlightQuery actual) {
        List<String> expectedFields = Arrays.asList(
                RecipeIndexRepositoryImpl.FIELD_ID,
                RecipeIndexRepositoryImpl.FIELD_NAME,
                RecipeIndexRepositoryImpl.FIELD_SUMMARY,
                RecipeIndexRepositoryImpl.FIELD_LABEL);

        assertNotNull(actual);
        assertThat(actual.getCriteria(),
                CriteriaMatchers.eq(new SimpleStringCriteria(QUERY_STRING)));
        assertEquals(actual.getOffset(), Integer.valueOf(0));
        assertEquals(actual.getRows(), Integer.valueOf(1));
        assertFieldNamesEqual(actual.getProjectionOnFields(), expectedFields);
        assertEquals(actual.getDefType(),
                RecipeIndexRepositoryImpl.DEF_TYPE_EDISMAX);
        assertEquals(actual.getHighlightOptions().getSimplePrefix(), "<strong>");
        assertEquals(actual.getHighlightOptions().getSimplePostfix(), "</strong>");
        assertSame(actual.getSort(), SORT);
    }

    private void assertFacetQuery(FacetQuery actual) {
        assertNotNull(actual);

        Criteria expectedCriteria = Criteria.where(Criteria.WILDCARD)
                .expression(Criteria.WILDCARD);
        assertThat(actual.getCriteria(), CriteriaMatchers.eq(expectedCriteria));
        assertEquals(actual.getDefType(),
                RecipeIndexRepositoryImpl.DEF_TYPE_EDISMAX);
        assertFieldNamesEqual(actual.getFacetOptions().getFacetOnFields(),
                Arrays.asList(RecipeIndexRepositoryImpl.FIELD_LABEL));
        assertEquals(actual.getFacetOptions().getFacetPrefix(), QUERY_STRING);
        assertSame(actual.getFacetOptions().getPageable(), PAGE_REQUEST);
        assertEquals(actual.getRows(), Integer.valueOf(0));
    }

    private void assertFieldNamesEqual(
            Iterable<Field> actual, Iterable<String> expected) {
        List<String> actualNames = new ArrayList<>();
        actual.forEach(f -> actualNames.add(f.getName()));

        assertEquals(actualNames, expected);
    }
}
