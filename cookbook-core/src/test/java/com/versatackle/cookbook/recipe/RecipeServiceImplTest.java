package com.versatackle.cookbook.recipe;

import com.google.common.collect.Lists;
import com.google.common.eventbus.EventBus;
import com.versatackle.cookbook.core.NotFoundException;
import static com.versatackle.cookbook.core.fixtures.CommonFixtures.getStandardId;
import com.versatackle.cookbook.recipe.events.AllRecipesDeletedEvent;
import com.versatackle.cookbook.recipe.events.RecipeAddedEvent;
import com.versatackle.cookbook.recipe.events.RecipeBatchAddedEvent;
import com.versatackle.cookbook.recipe.events.RecipeBatchDeletedEvent;
import com.versatackle.cookbook.recipe.events.RecipeDeletedEvent;
import com.versatackle.cookbook.recipe.events.RecipeUpdatedEvent;
import static com.versatackle.cookbook.recipe.fixtures.RecipeFixtures.*;
import java.util.List;
import java.util.UUID;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.MockitoAnnotations;
import static org.testng.Assert.*;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Unit test for RecipeServiceImpl.
 *
 * @author Ville Vornanen
 */
public class RecipeServiceImplTest {

    @InjectMocks
    private RecipeServiceImpl service;

    @Mock
    private RecipeRepository repositoryMock;

    @Mock
    private EventBus eventBusMock;

    @BeforeMethod(alwaysRun = true)
    public void setUp() {
        service = new RecipeServiceImpl();

        MockitoAnnotations.initMocks(this);

        when(repositoryMock.save(any(Iterable.class)))
                .thenAnswer(a -> Lists.newArrayList((Iterable) a.getArguments()[0]));
    }

    private Recipe performAdd(RecipeDTO argument, Recipe expected) {
        when(repositoryMock.save(expected)).thenReturn(expected);
        return service.add(argument);
    }

    @Test
    public void testAddSavesEntity() {
        Recipe expected = newStandardRecipe();
        performAdd(newStandardRecipeDTO(), expected);

        verify(repositoryMock).save(eq(expected));
        verifyNoMoreInteractions(repositoryMock);
    }

    @Test
    public void testAddPostsEvent() {
        Recipe expected = newStandardRecipe();
        performAdd(newStandardRecipeDTO(), expected);

        ArgumentCaptor<RecipeAddedEvent> argumentCaptor
                = ArgumentCaptor.forClass(RecipeAddedEvent.class);

        verify(eventBusMock).post(argumentCaptor.capture());
        verifyNoMoreInteractions(eventBusMock);

        assertEquals(argumentCaptor.getValue().getRecipe(), expected);
    }

    @Test
    public void testAddReturnsSavedEntity() {
        Recipe expected = newStandardRecipe();
        Recipe actual = performAdd(newStandardRecipeDTO(), expected);

        assertSame(actual, expected);
    }

    @Test
    public void testAddAllSavesEntities() {
        List<Recipe> expected = newStandardRecipeList();
        service.addAll(newStandardRecipeDTOList());

        verify(repositoryMock).save(expected);
        verifyNoMoreInteractions(repositoryMock);
    }

    @Test
    public void testAddAllPostsEvent() {
        List<Recipe> expected = newStandardRecipeList();
        service.addAll(newStandardRecipeDTOList());

        ArgumentCaptor<RecipeBatchAddedEvent> argumentCaptor
                = ArgumentCaptor.forClass(RecipeBatchAddedEvent.class);

        verify(eventBusMock).post(argumentCaptor.capture());
        verifyNoMoreInteractions(eventBusMock);

        assertEquals(argumentCaptor.getValue().getRecipes(), expected);
    }

    @Test
    public void testAddAllReturnsSavedEntities() {
        List<Recipe> expected = newStandardRecipeList();
        List<Recipe> actual = service.addAll(newStandardRecipeDTOList());

        assertEquals(actual, expected);
    }

    private Recipe performDeleteById(UUID id, Recipe expected)
            throws NotFoundException {
        when(repositoryMock.findOne(id)).thenReturn(expected);
        return service.deleteById(id);
    }

    @Test
    public void testDeleteByIdDeletesEntity() throws NotFoundException {
        Recipe expected = newStandardRecipe();
        performDeleteById(expected.getId(), expected);

        verify(repositoryMock).findOne(expected.getId());
        verify(repositoryMock).delete(expected);
        verifyNoMoreInteractions(repositoryMock);
    }

    @Test
    public void testDeleteByIdPostsEvent() throws NotFoundException {
        Recipe expected = newStandardRecipe();
        performDeleteById(expected.getId(), expected);

        ArgumentCaptor<RecipeDeletedEvent> argumentCaptor
                = ArgumentCaptor.forClass(RecipeDeletedEvent.class);

        verify(eventBusMock).post(argumentCaptor.capture());
        verifyNoMoreInteractions(eventBusMock);

        assertEquals(argumentCaptor.getValue().getRecipe(), expected);
    }

    @Test
    public void testDeleteByIdReturnsEntity() throws NotFoundException {
        Recipe expected = newStandardRecipe();
        Recipe actual = performDeleteById(expected.getId(), expected);

        assertEquals(actual, expected);
    }

    @Test(expectedExceptions = {NotFoundException.class})
    public void testDeleteByIdRecipeNotFound() throws NotFoundException {
        try {
            performDeleteById(getStandardId(), null);
        } finally {
            verify(repositoryMock).findOne(getStandardId());
            verifyNoMoreInteractions(repositoryMock);
        }
    }

    @Test(expectedExceptions = {NotFoundException.class})
    public void testDeleteByIdRecipeNotFoundDoesNotPostEvent()
            throws NotFoundException {
        try {
            performDeleteById(getStandardId(), null);
        } finally {
            verifyZeroInteractions(eventBusMock);
        }
    }

    @Test
    public void testDeleteDeletesEntities() {
        List<Recipe> recipes = newStandardRecipeList();
        service.delete(recipes);

        verify(repositoryMock).delete(recipes);
        verifyNoMoreInteractions(repositoryMock);
    }

    @Test
    public void testDeletePostsEvent() {
        Iterable<Recipe> recipes = newStandardRecipeList();
        service.delete(recipes);

        ArgumentCaptor<RecipeBatchDeletedEvent> argumentCaptor
                = ArgumentCaptor.forClass(RecipeBatchDeletedEvent.class);

        verify(eventBusMock).post(argumentCaptor.capture());
        verifyNoMoreInteractions(eventBusMock);

        assertEquals(argumentCaptor.getValue().getRecipes(), recipes);
    }

    @Test
    public void testDeleteAllDeletesEntities() {
        service.deleteAll();

        verify(repositoryMock, times(1)).deleteAll();
        verifyNoMoreInteractions(repositoryMock);
    }

    @Test
    public void testDeleteAllPostsEvent() {
        service.deleteAll();

        verify(eventBusMock).post(any(AllRecipesDeletedEvent.class));
        verifyNoMoreInteractions(eventBusMock);
    }

    @Test
    public void testFindAll() {
        List<Recipe> expected = newStandardRecipeList();
        when(repositoryMock.findAll()).thenReturn(expected);

        List<Recipe> actual = service.findAll();

        verify(repositoryMock).findAll();
        verifyNoMoreInteractions(repositoryMock);

        assertEquals(actual, expected);
    }

    @Test
    public void testFindById() throws NotFoundException {
        Recipe expected = newStandardRecipe();
        when(repositoryMock.findOne(expected.getId())).thenReturn(expected);

        Recipe actual = service.findById(expected.getId());

        verify(repositoryMock).findOne(expected.getId());
        verifyNoMoreInteractions(repositoryMock);

        assertEquals(actual, expected);
    }

    @Test(expectedExceptions = {NotFoundException.class})
    public void testFindByIdRecipeNotFound() throws NotFoundException {
        when(repositoryMock.findOne(getStandardId()))
                .thenReturn(null);

        try {
            service.findById(getStandardId());
        } finally {
            verify(repositoryMock).findOne(getStandardId());
            verifyNoMoreInteractions(repositoryMock);
        }
    }

    private Recipe performUpdate(RecipeDTO argument, Recipe expected)
            throws NotFoundException {
        when(repositoryMock.findOne(argument.getId())).thenReturn(expected);
        when(repositoryMock.save(expected)).thenReturn(expected);

        return service.update(argument);
    }

    @Test
    public void testUpdateSavesEntity() throws NotFoundException {
        Recipe expected = newStandardRecipe();
        performUpdate(newStandardRecipeDTO(), expected);

        verify(repositoryMock).findOne(expected.getId());
        verify(repositoryMock).save(expected);
        verifyNoMoreInteractions(repositoryMock);
    }

    @Test
    public void testUpdatePostsEvent() throws NotFoundException {
        Recipe expected = newStandardRecipe();
        performUpdate(newStandardRecipeDTO(), expected);

        ArgumentCaptor<RecipeUpdatedEvent> argumentCaptor
                = ArgumentCaptor.forClass(RecipeUpdatedEvent.class);

        verify(eventBusMock).post(argumentCaptor.capture());
        verifyNoMoreInteractions(eventBusMock);

        assertEquals(argumentCaptor.getValue().getRecipe(), expected);
    }

    @Test
    public void testUpdateEntityIsUpdated() throws NotFoundException {
        Recipe expected = newStandardRecipe();
        Recipe actual = performUpdate(newStandardRecipeDTO(), expected);

        assertEquals(actual, expected);
    }

    @Test(expectedExceptions = {NotFoundException.class})
    public void testUpdateRecipeNotFound() throws NotFoundException {
        try {
            performUpdate(newStandardRecipeDTO(), null);
        } finally {
            verify(repositoryMock).findOne(getStandardId());
            verifyNoMoreInteractions(repositoryMock);
        }
    }

    @Test(expectedExceptions = {NotFoundException.class})
    public void testUpdateRecipeNotFoundDoesNotPostEvent()
            throws NotFoundException {
        try {
            performUpdate(newStandardRecipeDTO(), null);
        } finally {
            verifyZeroInteractions(eventBusMock);
        }
    }
}
