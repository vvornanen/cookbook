package com.versatackle.cookbook.recipe.index;

import com.codepoetics.protonpack.StreamUtils;
import static com.versatackle.cookbook.recipe.index.CriteriaMatchers.eq;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;
import org.springframework.data.solr.core.query.Criteria;

/**
 *
 * @author Ville Vornanen
 */
class CriteriaMatcher extends TypeSafeMatcher<Criteria> {

    private final Criteria expectedCriteria;

    public CriteriaMatcher(Criteria expected) {
        this.expectedCriteria = expected;
    }

    @Override
    protected boolean matchesSafely(Criteria actualCriteria) {
        if (actualCriteria.getPredicates().size()
                != expectedCriteria.getPredicates().size()) {
            return false;
        }

        return StreamUtils.zip(
                actualCriteria.getPredicates().stream(),
                expectedCriteria.getPredicates().stream(),
                (actual, expected) -> eq(expected).matches(actual))
                .allMatch(o -> o);
    }

    @Override
    public void describeTo(Description description) {
        description.appendText(expectedCriteria.toString());
    }

}
