package com.versatackle.cookbook.recipe.index;

import org.hamcrest.Matcher;
import org.springframework.data.solr.core.query.Criteria;

/**
 * Matchers for Spring Data Solr {@link Criteria}.
 *
 * @author Ville Vornanen
 */
class CriteriaMatchers {

    static Matcher<Criteria> eq(Criteria expected) {
        return new CriteriaMatcher(expected);
    }

    static Matcher<Criteria.Predicate> eq(Criteria.Predicate expected) {
        return new PredicateMatcher(expected);
    }

}
