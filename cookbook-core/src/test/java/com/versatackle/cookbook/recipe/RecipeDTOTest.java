package com.versatackle.cookbook.recipe;

import com.google.common.collect.Lists;
import com.versatackle.cookbook.recipe.fixtures.RecipeFixtures;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import org.springframework.util.xml.SimpleNamespaceContext;
import static org.testng.Assert.*;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

/**
 * JAXB unit tests for RecipeDTO.
 *
 * @author Ville Vornanen
 */
public class RecipeDTOTest {
    private static final String RECIPE_NS
            = "http://cookbook.versatackle.com/ns/2014/recipe";
    private static final String STANDARD_RECIPE_XML
            = "com/versatackle/cookbook/recipe/fixtures/standard-recipe.xml";

    private NamespaceContext ns;

    private JAXBContext context;

    @BeforeMethod
    public void setUp() throws JAXBException {
        SimpleNamespaceContext namespaceContext = new SimpleNamespaceContext();
        namespaceContext.bindNamespaceUri("recipe", RECIPE_NS);
        ns = namespaceContext;

        context = JAXBContext.newInstance(
                RecipeDTO.class, RecipeDTO.RecipeList.class);
    }

    @Test
    public void testMarshal() throws Exception {
        RecipeDTO dto = RecipeFixtures.newStandardRecipeDTO();

        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

        Document document = newDocument();
        marshaller.marshal(RecipeDTO.RecipeList.of(dto), document);
        marshaller.marshal(RecipeDTO.RecipeList.of(dto), System.out);

        assertThat(document, hasXPath("/recipe:recipes", ns));
        assertThat(document, hasXPath("/recipe:recipes/recipe:recipe", ns));

        Node actual = document.getFirstChild().getFirstChild();
        assertNodeEqualsRecipeDTO(actual, dto);
    }

    @Test
    public void testUnmarshal() throws Exception {
        RecipeDTO.RecipeList actual;

        try (InputStream xml = getResource(STANDARD_RECIPE_XML)) {
            InputStreamReader reader = new InputStreamReader(xml);
            BufferedReader buf = new BufferedReader(reader);

            String line = buf.readLine();
            while (line != null) {
                System.out.println(line);
                line = buf.readLine();
            }
        }

        try (InputStream xml = getResource(STANDARD_RECIPE_XML)) {
            Unmarshaller unmarshaller = context.createUnmarshaller();
            actual = (RecipeDTO.RecipeList) unmarshaller.unmarshal(xml);
        }

        assertEquals(actual.getRecipes().size(), 1, "Should contain one recipe");
        assertThat(actual.getRecipes().get(0),
                RecipeMatchers.eq(RecipeFixtures.newStandardRecipeDTO()));
    }

    private void assertNodeEqualsRecipeDTO(Node actual, RecipeDTO expected) {
        assertThat(actual, hasXPath("./@id", ns, is(expected.getId().toString())));
        assertThat(actual, hasXPath("./recipe:name", ns, is(expected.getName())));
        assertThat(actual, hasXPath("./recipe:author[1]", ns,
                is(expected.getAuthorsAsList().get(0))));
        assertThat(actual, hasXPath("./recipe:author[2]", ns,
                is(expected.getAuthorsAsList().get(1))));
        assertThat(actual, hasXPath("./recipe:instructions", ns,
                is(expected.getInstructions())));

        List<String> labels = Lists.newArrayList(expected.getLabelSet());
        assertThat(actual, hasXPath("./recipe:label[1]", ns, is(labels.get(0))));
        assertThat(actual, hasXPath("./recipe:label[2]", ns, is(labels.get(1))));
        assertThat(actual, hasXPath("./recipe:label[3]", ns, is(labels.get(2))));

        assertThat(actual, hasXPath("./recipe:summary", ns,
                is(expected.getSummary())));
        assertThat(actual, hasXPath("./recipe:yield", ns, is(expected.getYield())));
    }

    private InputStream getResource(String path) {
        return Thread.currentThread().getContextClassLoader()
                .getResourceAsStream(path);
    }

    private Document newDocument() throws ParserConfigurationException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        return dbf.newDocumentBuilder().newDocument();
    }
}
