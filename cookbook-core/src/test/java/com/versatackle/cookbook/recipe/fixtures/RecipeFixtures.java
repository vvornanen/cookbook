package com.versatackle.cookbook.recipe.fixtures;

import com.google.common.collect.ImmutableList;
import com.versatackle.cookbook.core.fixtures.CommonFixtures;
import com.versatackle.cookbook.recipe.Label;
import com.versatackle.cookbook.recipe.Recipe;
import com.versatackle.cookbook.recipe.RecipeDTO;
import com.versatackle.cookbook.recipe.index.RecipeDocument;
import java.util.Arrays;
import java.util.List;

/**
 * Contains a few useful factory methods to provide some test data.
 *
 * @author Ville Vornanen
 */
public class RecipeFixtures {

    /**
     * Provides a new Recipe instance with fixed data.
     *
     * All recipe fields are set as non-empty.
     *
     * @return a new instance of Recipe
     */
    public static Recipe newStandardRecipe() {
        Recipe recipe = new Recipe(
                CommonFixtures.getStandardId(),
                "My Standard Recipe");
        recipe.setSummary("An overview for My Standard Recipe");
        recipe.setYield("Yield");
        recipe.addLabel(Label.valueOf("yummy"));
        recipe.addLabel(Label.valueOf("low-fat"));
        recipe.addLabel(Label.valueOf("fresh"));
        recipe.addAuthor("Standard Author");
        recipe.addAuthor("Second Author");
        recipe.setInstructions(
                "<p>These are the <em>standard</em> instructions.</p>");

        return recipe;
    }

    /**
     * Provides a new RecipeDTO with fixed data.
     *
     * All recipe fields are set as non-empty.
     *
     * @return a new instance of RecipeDTO
     */
    public static RecipeDTO newStandardRecipeDTO() {
        return RecipeDTO.fromRecipe(newStandardRecipe());
    }

    public static RecipeDocument newStandardRecipeDocument() {
        RecipeDocument recipe = new RecipeDocument();
        recipe.setId(CommonFixtures.getStandardId().toString());
        recipe.setName("My Standard Recipe");
        recipe.setSummary("An overview for My Standard Recipe");
        recipe.setYield("Yield");
        recipe.setLabels(ImmutableList.of("yummy", "low-fat", "fresh"));
        recipe.setAuthors(ImmutableList.of("Standard Author", "Second Author"));
        recipe.setInstructions(
                "<p>These are the <em>standard</em> instructions.</p>");

        return recipe;
    }

    /**
     * Provides a new list of Recipe instances with fixed data.
     *
     * All recipe fields are set as non-empty.
     *
     * @return a new list of Recipe instances.
     */
    public static List<Recipe> newStandardRecipeList() {
        return Arrays.asList(newStandardRecipe());
    }

    /**
     * Provides a new list of RecipeDTO instances with fixed data.
     *
     * All recipe fields are set as non-empty.
     *
     * @return a new list of RecipeDTO instances
     */
    public static List<RecipeDTO> newStandardRecipeDTOList() {
        return Arrays.asList(newStandardRecipeDTO());
    }

    /**
     * Provides a new RecipeDTO without identifier.
     *
     * All recipe fields are set as non-empty except the identifier which
     * is {@code null}.
     *
     * @return a new instance of RecipeDTO with {@code null} identifier
     */
    public static RecipeDTO newStandardRecipeDTONoId() {
        RecipeDTO dto = newStandardRecipeDTO();
        dto.setId(null);

        return dto;
    }

    /**
     * Provides a new empty RecipeDTO.
     *
     * All recipe fields are set as empty (identifier as {@code null}).
     *
     * @return a new empty instance of RecipeDTO
     */
    public static RecipeDTO newEmptyRecipeDTO() {
        return new RecipeDTO();
    }
}
