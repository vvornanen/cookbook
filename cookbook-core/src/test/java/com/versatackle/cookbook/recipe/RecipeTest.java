package com.versatackle.cookbook.recipe;

import com.versatackle.cookbook.recipe.fixtures.RecipeFixtures;
import com.versatackle.cookbook.user.User;
import com.versatackle.cookbook.user.fixtures.UserFixtures;
import java.time.Instant;
import java.util.Arrays;
import java.util.UUID;
import static org.testng.Assert.*;
import org.testng.annotations.Test;

/**
 * Unit test for Recipe.
 *
 * @author Ville Vornanen
 */
public class RecipeTest {

    private static final UUID ID
            = UUID.fromString("0436b838-b72c-4500-8eea-54153685919e");
    private static final String NAME = "My Test Recipe";
    private static final String SUMMARY = "Test summary";
    private static final String YIELD = "Test yield";
    private static final String AUTHOR = "Test author";
    private static final String INSTRUCTIONS = "<p>Test instructions</p>";

    @Test
    public void testConstructWithMandatoryValues() {
        Recipe recipe = new Recipe(ID, NAME);

        assertEquals(recipe.getId(), ID);
        assertEquals(recipe.getName(), NAME);
    }

    @Test
    public void testConstructCreatedNotNull() {
        Recipe recipe = new Recipe(ID, NAME);

        assertNotNull(recipe.getCreated());
    }

    @Test
    public void testConstructSummaryIsNull() {
        Recipe recipe = new Recipe(ID, NAME);

        assertNull(recipe.getSummary());
    }

    @Test
    public void testConstructYieldIsNull() {
        Recipe recipe = new Recipe(ID, NAME);

        assertNull(recipe.getYield());
    }

    @Test
    public void testConstructAuthorsIsEmpty() {
        Recipe recipe = new Recipe(ID, NAME);

        assertTrue(recipe.getAuthors().isEmpty());
    }

    @Test
    public void testUpdateName() {
        Recipe recipe = RecipeFixtures.newStandardRecipe();
        recipe.setName(NAME);

        assertEquals(recipe.getName(), NAME);
    }

    @Test
    public void testUpdateSummary() {
        Recipe recipe = RecipeFixtures.newStandardRecipe();
        recipe.setSummary(SUMMARY);

        assertEquals(recipe.getSummary(), SUMMARY);
    }

    @Test
    public void testUpdateYield() {
        Recipe recipe = RecipeFixtures.newStandardRecipe();
        recipe.setYield(YIELD);

        assertEquals(recipe.getYield(), YIELD);
    }

    @Test
    public void testUpdateAuthors() {
        Recipe recipe = RecipeFixtures.newStandardRecipe();

        int i = recipe.getAuthors().size();

        recipe.addAuthor(AUTHOR);
        assertEquals(recipe.getAuthors().get(i), AUTHOR);

        String author2 = "Test author 2";
        String author3 = "Test author 3";

        recipe.addAuthors(Arrays.asList(author2, author3));
        assertEquals(recipe.getAuthors().get(i + 1), author2);
        assertEquals(recipe.getAuthors().get(i + 2), author3);
    }

    @Test
    public void testUpdateInstructions() {
        Recipe recipe = RecipeFixtures.newStandardRecipe();

        recipe.setInstructions(INSTRUCTIONS);
        assertEquals(recipe.getInstructions(), INSTRUCTIONS);
    }

    /**
     * This test does not guarantee that the input is completely safe after
     * filtering, only that the filter exists.
     */
    @Test
    public void testUpdateInstructionsIsSanitized() {
        Recipe recipe = RecipeFixtures.newStandardRecipe();

        String dirty = "Image: <img src=\"javascript:alert('xss');\"/>";
        String expected = "Image: ";

        recipe.setInstructions(dirty);
        assertEquals(recipe.getInstructions(), expected);
    }

    @Test
    public void testUpdateInstructionsAddsNofollow() {
        Recipe recipe = RecipeFixtures.newStandardRecipe();

        String dirty = "<a href=\"http://example.com\">external link</a>";
        String expected = "<a href=\"http://example.com\" rel=\"nofollow\">"
                + "external link</a>";

        recipe.setInstructions(dirty);
        assertEquals(recipe.getInstructions(), expected);
    }

    @Test
    public void testUpdateInstructionsAllowsFloat() {
        Recipe recipe = RecipeFixtures.newStandardRecipe();

        String expected = "<img src=\"image.png\" style=\"float:right\" />";

        recipe.setInstructions(expected);
        assertEquals(recipe.getInstructions(), expected);
    }

    @Test
    public void testUpdateCreated() {
        Recipe recipe = RecipeFixtures.newStandardRecipe();

        Instant timestamp = Instant.now();
        recipe.setCreated(timestamp);

        assertEquals(recipe.getCreated(), timestamp);
    }

    @Test
    public void testUpdateCreator() {
        Recipe recipe = RecipeFixtures.newStandardRecipe();

        assertNull(recipe.getCreator());

        User user = UserFixtures.newStandardUser();
        recipe.setCreator(user);

        assertEquals(recipe.getCreator(), user);
    }

    @Test
    public void testUpdateModified() {
        Recipe recipe = RecipeFixtures.newStandardRecipe();

        assertNull(recipe.getModified());

        Instant timestamp = Instant.now();
        recipe.setModified(timestamp);

        assertEquals(recipe.getModified(), timestamp);
    }

    @Test
    public void testUpdateModifier() {
        Recipe recipe = RecipeFixtures.newStandardRecipe();

        assertNull(recipe.getModifier());

        User user = UserFixtures.newStandardUser();
        recipe.setModifier(user);

        assertEquals(recipe.getModifier(), user);
    }
}
