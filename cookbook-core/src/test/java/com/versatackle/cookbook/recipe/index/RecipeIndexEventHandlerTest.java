package com.versatackle.cookbook.recipe.index;

import com.google.common.eventbus.EventBus;
import com.versatackle.cookbook.recipe.Recipe;
import com.versatackle.cookbook.recipe.events.AllRecipesDeletedEvent;
import com.versatackle.cookbook.recipe.events.RecipeAddedEvent;
import com.versatackle.cookbook.recipe.events.RecipeBatchAddedEvent;
import com.versatackle.cookbook.recipe.events.RecipeBatchDeletedEvent;
import com.versatackle.cookbook.recipe.events.RecipeBatchUpdatedEvent;
import com.versatackle.cookbook.recipe.events.RecipeDeletedEvent;
import com.versatackle.cookbook.recipe.events.RecipeUpdatedEvent;
import static com.versatackle.cookbook.recipe.fixtures.RecipeFixtures.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.MockitoAnnotations;
import org.springframework.core.task.TaskExecutor;
import org.springframework.test.util.ReflectionTestUtils;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Unit test for {@link RecipeIndexEventHandler}.
 *
 * @author Ville Vornanen
 */
public class RecipeIndexEventHandlerTest {

    private final EventBus eventBus = new EventBus();

    private final TaskExecutor executor = Runnable::run;

    @Mock
    private RecipeIndexService serviceMock;

    @InjectMocks
    private RecipeIndexEventHandler handler;

    @BeforeMethod(alwaysRun = true)
    public void setUp() {
        handler = new RecipeIndexEventHandler();
        MockitoAnnotations.initMocks(this);
        ReflectionTestUtils.setField(handler, "executor", executor);
        eventBus.register(handler);
    }

    @AfterMethod(alwaysRun = true)
    public void teadDown() {
        eventBus.unregister(handler);
    }

    @Test
    public void testOnRecipeAdded() {
        Recipe expected = newStandardRecipe();
        eventBus.post(new RecipeAddedEvent(expected));

        verify(serviceMock).add(expected);
        verifyNoMoreInteractions(serviceMock);
    }

    @Test
    public void testOnRecipeBatchAdded() {
        Iterable<Recipe> expected = newStandardRecipeList();
        eventBus.post(new RecipeBatchAddedEvent(expected));

        verify(serviceMock).addAll(expected);
        verifyNoMoreInteractions(serviceMock);
    }

    @Test
    public void testOnRecipeDeleted() {
        Recipe expected = newStandardRecipe();
        eventBus.post(new RecipeDeletedEvent(expected));

        verify(serviceMock).deleteById(expected.getId());
        verifyNoMoreInteractions(serviceMock);
    }

    @Test
    public void testOnRecipeBatchDeleted() {
        Iterable<Recipe> expected = newStandardRecipeList();
        eventBus.post(new RecipeBatchDeletedEvent(expected));

        verify(serviceMock).delete(expected);
        verifyNoMoreInteractions(serviceMock);
    }

    @Test
    public void testOnAllRecipesDeleted() {
        eventBus.post(new AllRecipesDeletedEvent());

        verify(serviceMock).deleteAll();
        verifyNoMoreInteractions(serviceMock);
    }

    @Test
    public void testOnRecipeUpdated() {
        Recipe expected = newStandardRecipe();
        eventBus.post(new RecipeUpdatedEvent(expected));

        verify(serviceMock).add(expected);
        verifyNoMoreInteractions(serviceMock);
    }

    @Test
    public void testOnRecipeBatchUpdated() {
        Iterable<Recipe> expected = newStandardRecipeList();
        eventBus.post(new RecipeBatchUpdatedEvent(expected));

        verify(serviceMock).addAll(expected);
        verifyNoMoreInteractions(serviceMock);
    }

}
