package com.versatackle.cookbook.recipe.index;

import java.util.Objects;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;
import org.springframework.data.solr.core.query.Criteria.Predicate;

/**
 *
 * @author Ville Vornanen
 */
class PredicateMatcher extends TypeSafeMatcher<Predicate> {

    private final Predicate expected;

    public PredicateMatcher(Predicate expected) {
        this.expected = expected;
    }

    @Override
    protected boolean matchesSafely(Predicate actual) {
        return Objects.equals(actual.getKey(), expected.getKey())
                && Objects.equals(actual.getValue(), expected.getValue());
    }

    @Override
    public void describeTo(Description description) {
        description.appendValue(expected);
    }

}
