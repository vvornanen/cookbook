package com.versatackle.cookbook.recipe;

import java.util.Objects;
import java.util.Set;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeDiagnosingMatcher;

/**
 *
 * @author Ville Vornanen
 */
public abstract class RecipeMatchers {

    public static Matcher<RecipeDTO> eq(final RecipeDTO expected) {
        return new TypeSafeDiagnosingMatcher<RecipeDTO>() {

            @Override
            protected boolean matchesSafely(RecipeDTO item, Description mismatchDescription) {
                boolean match = true;

                if (!Objects.equals(item.getName(), expected.getName())) {
                    RecipeMatchers.describeMismatch(
                            "name", item.getName(), mismatchDescription, match);
                    match = false;
                }

                if (!Objects.equals(item.getSummary(), expected.getSummary())) {
                    RecipeMatchers.describeMismatch(
                            "summary", item.getSummary(), mismatchDescription, match);
                    match = false;
                }

                if (!Objects.equals(item.getYield(), expected.getYield())) {
                    RecipeMatchers.describeMismatch(
                            "yield", item.getYield(), mismatchDescription, match);
                    match = false;
                }

                if (!Objects.equals(item.getLabelSet(), expected.getLabelSet())) {
                    RecipeMatchers.describeMismatch(
                            "labels",
                            item.getLabelSet(),
                            mismatchDescription,
                            match);
                    match = false;
                }

                if (!Objects.equals(
                        item.getAuthorsAsList(), expected.getAuthorsAsList())) {
                    RecipeMatchers.describeMismatch(
                            "authors",
                            item.getAuthorsAsList(),
                            mismatchDescription,
                            match);
                    match = false;
                }

                if (!Objects.equals(
                        item.getInstructions(), expected.getInstructions())) {
                    RecipeMatchers.describeMismatch(
                            "instructions",
                            item.getInstructions(),
                            mismatchDescription,
                            match);
                    match = false;
                }

                assert match == item.equals(expected):
                        "Match should be consistent with RecipeDTO.equals";

                return match;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("a recipe equal to ")
                        .appendValue(expected);
            }

        };
    }

    public static Matcher<RecipeDTO> nameEq(final String name) {
        return new TypeSafeDiagnosingMatcher<RecipeDTO>() {

            @Override
            protected boolean matchesSafely(
                    RecipeDTO item, Description mismatchDescription) {
                if (!Objects.equals(name, item.getName())) {
                    RecipeMatchers.describeMismatch(
                            "name", item.getName(), mismatchDescription, true);
                    return false;
                }

                return true;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("having name ")
                        .appendValue(name);
            }

        };
    }

    public static Matcher<RecipeDTO> summaryEq(final String summary) {
        return new TypeSafeDiagnosingMatcher<RecipeDTO>() {

            @Override
            protected boolean matchesSafely(
                    RecipeDTO item, Description mismatchDescription) {
                if (!Objects.equals(summary, item.getSummary())) {
                    RecipeMatchers.describeMismatch(
                            "summary", item.getSummary(), mismatchDescription, true);
                    return false;
                }

                return true;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("having summary ")
                        .appendValue(summary);
            }

        };
    }

    public static Matcher<RecipeDTO> labelsEq(final Set<String> labels) {
        return new TypeSafeDiagnosingMatcher<RecipeDTO>() {

            @Override
            protected boolean matchesSafely(
                    RecipeDTO item, Description mismatchDescription) {
                if (!Objects.equals(labels, item.getLabelSet())) {
                    RecipeMatchers.describeMismatch(
                            "labels", item.getLabelSet(), mismatchDescription, true);
                    return false;
                }

                return true;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("having labels ")
                        .appendValue(labels);
            }

        };
    }

    private static void describeMismatch(
            String fieldName,
            Object actual,
            Description mismatchDescription,
            boolean firstMismatch) {
        if (!firstMismatch) {
            mismatchDescription.appendText(", ");
        }

        mismatchDescription.appendText(fieldName)
                .appendText(" was ")
                .appendValue(actual);

    }
}
