package com.versatackle.cookbook.recipe;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;
import org.testng.annotations.Test;

/**
 * Unit test for {@link Label}.
 *
 * @author Ville Vornanen
 */
public class LabelTest {
    private static final String LABEL = "label";

    @Test
    public void testEquals() {
        Label first = Label.valueOf(LABEL);
        Label second = Label.valueOf(LABEL);

        assertTrue(first.equals(second));
    }

    @Test
    public void testToString() {
        Label label = Label.valueOf(LABEL);
        assertEquals(label.toString(), LABEL);
    }

    @Test
    public void testCompareTo() {
        Label alpha = Label.valueOf("alpha");
        Label bravo = Label.valueOf("bravo");

        assertTrue(alpha.compareTo(bravo) < 0, "Expected a negative integer");
        assertEquals(alpha.compareTo(alpha), 0, "Expected zero");
        assertTrue(bravo.compareTo(alpha) > 0, "Expected a positive integer");
    }

    @Test
    public void testNaturalSort() {
        Label alpha = Label.valueOf("alpha");
        Label bravo = Label.valueOf("bravo");
        Label charlie = Label.valueOf("charlie");
        Label delta = Label.valueOf("delta");

        List<Label> expected = Arrays.asList(alpha, bravo, charlie, delta);
        List<Label> actual = Arrays.asList(charlie, bravo, delta, alpha);
        Collections.sort(actual);

        assertEquals(actual, expected);
    }

}
