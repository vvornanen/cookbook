package com.versatackle.cookbook.recipe.index;

import com.versatackle.cookbook.core.NotFoundException;
import com.versatackle.cookbook.core.fixtures.CommonFixtures;
import com.versatackle.cookbook.recipe.Recipe;
import static com.versatackle.cookbook.recipe.fixtures.RecipeFixtures.*;
import java.util.UUID;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.*;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.solr.core.query.result.FacetFieldEntry;
import org.springframework.data.solr.core.query.result.HighlightPage;
import static org.testng.Assert.*;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Unit test for {@link RecipeIndexServiceImpl}.
 *
 * @author Ville Vornanen
 */
public class RecipeIndexServiceImplTest {

    @Mock
    private RecipeIndexRepository repositoryMock;

    @InjectMocks
    private RecipeIndexService service;

    @BeforeMethod
    public void setUp() {
        service = new RecipeIndexServiceImpl();
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testSearch() {
        String queryString = "foo";
        Pageable pageRequest = new PageRequest(0, 10);
        Sort sort = new Sort("name");

        @SuppressWarnings("unchecked")
        HighlightPage<RecipeDocument> expected = Mockito.mock(HighlightPage.class);

        when(repositoryMock.search(queryString, pageRequest, sort))
                .thenReturn(expected);

        HighlightPage<RecipeDocument> actual = service.search(
                queryString, pageRequest, sort);

        verify(repositoryMock).search(queryString, pageRequest, sort);
        verifyNoMoreInteractions(repositoryMock);

        assertSame(actual, expected);
    }

    @Test
    public void testFindLabels() {
        String queryString = "foo";
        Pageable pageRequest = new PageRequest(0, 10);

        @SuppressWarnings("unchecked")
        Page<FacetFieldEntry> expected = mock(Page.class);

        when(repositoryMock.findLabelsBeginningWith(queryString, pageRequest))
                .thenReturn(expected);

        Page<FacetFieldEntry> actual = service.findLabels(queryString, pageRequest);

        verify(repositoryMock).findLabelsBeginningWith(
                queryString, pageRequest);
        verifyNoMoreInteractions(repositoryMock);

        assertSame(actual, expected);
    }

    @Test
    public void testFindLabel() throws Exception {
        String label = "test";

        FacetFieldEntry expected = mock(FacetFieldEntry.class);

        when(repositoryMock.findLabel(label)).thenReturn(expected);

        FacetFieldEntry actual = service.findLabel(label);

        assertSame(actual, expected);
    }

    @Test(expectedExceptions = NotFoundException.class)
    public void testFindLabelNotFound() throws Exception {
        when(repositoryMock.findLabel(anyString()))
                .thenThrow(new NotFoundException());

        service.findLabel("does not exist");
    }

    @Test
    public void testAdd() {
        Recipe recipe = newStandardRecipe();
        RecipeDocument expected = RecipeDocument.of(recipe);
        service.add(recipe);

        verify(repositoryMock).save(eq(expected));
        verifyNoMoreInteractions(repositoryMock);
    }

    @Test
    public void testAddAll() {
        Iterable<Recipe> recipes = newStandardRecipeList();
        Iterable<RecipeDocument> expected
                = RecipeIndexServiceImpl.toDocuments(recipes);
        service.addAll(recipes);

        ArgumentCaptor<Iterable<RecipeDocument>> argumentCaptor
                = ArgumentCaptor.forClass((Class) Iterable.class);

        verify(repositoryMock).save(argumentCaptor.capture());
        verifyNoMoreInteractions(repositoryMock);
        assertEquals(argumentCaptor.getValue(), expected);
    }

    @Test
    public void testDeleteById() {
        UUID id = CommonFixtures.getStandardId();
        service.deleteById(id);

        verify(repositoryMock).delete(id);
        verifyNoMoreInteractions(repositoryMock);
    }

    @Test
    public void testDelete() {
        Iterable<Recipe> recipes = newStandardRecipeList();
        Iterable<RecipeDocument> expected
                = RecipeIndexServiceImpl.toDocuments(recipes);
        service.delete(recipes);

        ArgumentCaptor<Iterable<RecipeDocument>> argumentCaptor
                = ArgumentCaptor.forClass((Class) Iterable.class);

        verify(repositoryMock).delete(argumentCaptor.capture());
        verifyNoMoreInteractions(repositoryMock);
        assertEquals(argumentCaptor.getValue(), expected);
    }

    @Test
    public void testDeleteAll() {
        service.deleteAll();

        verify(repositoryMock).deleteAll();
        verifyNoMoreInteractions(repositoryMock);
    }

}
