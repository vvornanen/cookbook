package com.versatackle.cookbook.user;

import com.mysema.query.types.Predicate;
import com.versatackle.cookbook.core.NotFoundException;
import com.versatackle.cookbook.core.fixtures.CommonFixtures;
import com.versatackle.cookbook.user.fixtures.UserFixtures;
import java.util.ArrayList;
import java.util.List;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.MockitoAnnotations;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import static org.testng.Assert.*;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 *
 * @author Ville Vornanen
 */
public class UserServiceImplTest {

    @Mock
    private UserRepository repository;

    @InjectMocks
    private UserService service;

    @BeforeMethod(alwaysRun = true)
    public void setUp() {
        service = new UserServiceImpl();
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testAdd() {
        UserDTO dto = UserFixtures.newStandardUserDTO();
        service.add(dto);

        ArgumentCaptor<User> argument = ArgumentCaptor.forClass(User.class);
        verify(repository).save(argument.capture());
        verifyNoMoreInteractions(repository);

        User user = argument.getValue();
        assertUserEquals(user, dto);
    }

    @Test
    public void testDeleteById() throws NotFoundException {
        User expected = UserFixtures.newStandardUser();
        when(repository.findOne(expected.getId())).thenReturn(expected);

        User actual = service.deleteById(expected.getId());

        verify(repository).findOne(expected.getId());
        verify(repository).delete(expected);
        verifyNoMoreInteractions(repository);

        assertEquals(actual, expected);
    }

    @Test(expectedExceptions = {NotFoundException.class})
    public void testDeleteByIdUserNotFound() throws NotFoundException {
        when(repository.findOne(CommonFixtures.getStandardId()))
                .thenReturn(null);

        try {
            service.deleteById(CommonFixtures.getStandardId());
        } finally {
            verify(repository).findOne(CommonFixtures.getStandardId());
            verifyNoMoreInteractions(repository);
        }
    }

    @Test
    public void testFindAll() {
        List<User> expected = new ArrayList<>();
        when(repository.findAll()).thenReturn(expected);

        List<User> actual = service.findAll();

        verify(repository).findAll();
        verifyNoMoreInteractions(repository);

        assertEquals(actual,expected);
    }

    @Test
    public void testFindById() throws NotFoundException {
        User expected = UserFixtures.newStandardUser();
        when(repository.findOne(expected.getId())).thenReturn(expected);

        User actual = service.findById(expected.getId());

        verify(repository).findOne(expected.getId());
        verifyNoMoreInteractions(repository);

        assertEquals(actual, expected);
    }

    @Test(expectedExceptions = {NotFoundException.class})
    public void testFindByIdUserNotFound() throws NotFoundException {
        when(repository.findOne(CommonFixtures.getStandardId()))
                .thenReturn(null);

        try {
            service.findById(CommonFixtures.getStandardId());
        } finally {
            verify(repository).findOne(CommonFixtures.getStandardId());
            verifyNoMoreInteractions(repository);
        }
    }

    @Test
    public void testExists() {
        boolean expected = true;
        when(repository.exists(CommonFixtures.getStandardId()))
                .thenReturn(expected);

        boolean actual = service.exists(CommonFixtures.getStandardId());

        verify(repository).exists(CommonFixtures.getStandardId());
        verifyNoMoreInteractions(repository);

        assertEquals(actual, expected);
    }

    @Test
    public void testExistsNotFound() {
        boolean expected = false;
        when(repository.exists(CommonFixtures.getStandardId()))
                .thenReturn(expected);

        boolean actual = service.exists(CommonFixtures.getStandardId());

        verify(repository).exists(CommonFixtures.getStandardId());
        verifyNoMoreInteractions(repository);

        assertEquals(actual, expected);
    }

    @Test
    public void testUpdate() throws NotFoundException {
        User expected = UserFixtures.newStandardUser();
        when(repository.findOne(expected.getId())).thenReturn(expected);

        UserDTO dto = UserFixtures.newStandardUserDTO();
        dto.setUsername("new username");
        dto.setName("new name");
        dto.setPassword("new password");
        User actual = service.update(dto);

        verify(repository).findOne(CommonFixtures.getStandardId());
        verifyNoMoreInteractions(repository);

        assertEquals(actual, expected);
    }

    @Test(expectedExceptions = {NotFoundException.class})
    public void testUpdateUserNotFound() throws NotFoundException {
        when(repository.findOne(CommonFixtures.getStandardId()))
                .thenReturn(null);

        UserDTO dto = UserFixtures.newStandardUserDTO();

        try {
            service.update(dto);
        } finally {
            verify(repository).findOne(CommonFixtures.getStandardId());
            verifyNoMoreInteractions(repository);
        }
    }

    @Test
    public void testLoadUserByUsername() throws UsernameNotFoundException {
        User expected = UserFixtures.newStandardUser();
        Predicate predicate = UserPredicates.usernameEquals(
                expected.getUsername());
        when(repository.findOne(predicate)).thenReturn(expected);

        UserDetails actual = service.loadUserByUsername(expected.getUsername());

        verify(repository).findOne(predicate);
        verifyNoMoreInteractions(repository);

        assertSame(actual, expected);
    }

    @Test(expectedExceptions = {UsernameNotFoundException.class})
    public void testLoadUserByUsernameNotFound() throws UsernameNotFoundException {
        String username = "notfound";
        Predicate predicate = UserPredicates.usernameEquals(username);
        when(repository.findOne(predicate)).thenReturn(null);

        try {
            service.loadUserByUsername(username);
        } finally {
            verify(repository).findOne(predicate);
            verifyNoMoreInteractions(repository);
        }
    }

    private void assertUserEquals(User actual, UserDTO expected) {
        assertEquals(actual.getId(), expected.getId());
        assertEquals(actual.getUsername(), expected.getUsername());
        assertEquals(actual.getPassword(), expected.getPassword());
        assertEquals(actual.getName(), expected.getName());
    }
}
