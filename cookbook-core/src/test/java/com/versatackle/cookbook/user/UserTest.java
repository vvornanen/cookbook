package com.versatackle.cookbook.user;

import com.versatackle.cookbook.core.AbstractEntityTest;
import com.versatackle.cookbook.core.fixtures.CommonFixtures;
import com.versatackle.cookbook.user.fixtures.UserFixtures;
import static org.mockito.Mockito.*;
import static org.testng.Assert.*;
import org.testng.annotations.Test;

/**
 *
 * @author Ville Vornanen
 */
public class UserTest extends AbstractEntityTest {
    private static final String USERNAME = "testuser";
    private static final String NAME = "Test user";
    private static final String PASSWORD = "Test password";

    @Test
    public void testConstructRandomId() {
        when(idGeneratorMock.newId()).thenReturn(CommonFixtures.getStandardId());
        User user = new User(USERNAME, NAME);

        assertEquals(user.getId(), CommonFixtures.getStandardId());
        verify(idGeneratorMock).newId();
    }

    @Test
    public void testConstructNullId() {
        when(idGeneratorMock.newId()).thenReturn(CommonFixtures.getStandardId());
        User user = new User(null, USERNAME, NAME);

        assertEquals(user.getId(), CommonFixtures.getStandardId());
        verify(idGeneratorMock).newId();
    }

    @Test
    public void testConstructExplicitId() {
        User user = new User(CommonFixtures.getStandardId(), USERNAME, NAME);

        assertEquals(user.getId(), CommonFixtures.getStandardId());
        verifyZeroInteractions(idGeneratorMock);
    }

    @Test
    public void testConstructMandatoryValues() {
        when(idGeneratorMock.newId()).thenReturn(CommonFixtures.getStandardId());
        User user = new User(USERNAME, NAME);

        assertEquals(user.getUsername(), USERNAME);
        assertEquals(user.getName(), NAME);
        assertNull(user.getPassword());
    }

    @Test
    public void testUpdateUsername() {
        User user = UserFixtures.newStandardUser();
        user.setUsername(USERNAME);
        assertEquals(user.getUsername(), USERNAME);
    }

    @Test
    public void testUpdatePassword() {
        User user = UserFixtures.newStandardUser();
        user.setPassword(PASSWORD);
        assertEquals(user.getPassword(), PASSWORD);
    }

    @Test
    public void testUpdateName() {
        User user = UserFixtures.newStandardUser();
        user.setName(NAME);
        assertEquals(user.getName(), NAME);
    }
}
