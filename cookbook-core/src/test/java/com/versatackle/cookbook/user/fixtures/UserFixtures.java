package com.versatackle.cookbook.user.fixtures;

import com.versatackle.cookbook.core.fixtures.CommonFixtures;
import com.versatackle.cookbook.user.User;
import com.versatackle.cookbook.user.UserDTO;
import java.util.UUID;

/**
 * Contains a few useful factory methods to provide some test data.
 *
 * @author Ville Vornanen
 */
public class UserFixtures {

    /**
     * Provides a new {@link User} instance with fixed data.
     *
     * All user fields are set as non-empty.
     *
     * @return a new instance of {@link User}
     */
    public static User newStandardUser() {
        User user = new User(CommonFixtures.getStandardId(),
                "user", "Standard User");
        user.setPassword("password");

        return user;
    }

    /**
     * Provides a new {@link User} instance for documentation examples.
     *
     * @return a new user instance with fixed id
     */
    public static User mickeyWalmsley() {
        User user = new User(
                UUID.fromString("613028ea-2137-4875-aad9-7fd8b0336cf6"),
                "mickey.walmsley",
                "Mickey Walmsley");
        user.setPassword("mickey");

        return user;
    }

    /**
     * Provides a new {@link User} instance for documentation examples.
     *
     * @return a new user instance with fixed id
     */
    public static User joslynHightower() {
        User user = new User(
                UUID.fromString("bf9426f0-b63f-46e5-9e2b-8cbfbe503056"),
                "joslyn.hightower",
                "Joslyn Hightower");
        user.setPassword("joslyn");

        return user;
    }

    /**
     * Provides a new {@link UserDTO} instance with fixed data.
     *
     * All user fields are set as non-empty.
     *
     * @return a new instance of {@link UserDTO}
     */
    public static UserDTO newStandardUserDTO() {
        return UserDTO.fromUser(newStandardUser());
    }
}
