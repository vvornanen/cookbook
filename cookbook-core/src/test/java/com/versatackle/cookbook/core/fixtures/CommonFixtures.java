package com.versatackle.cookbook.core.fixtures;

import java.util.UUID;

/**
 * Contains a few useful factory methods to provide some test data.
 *
 * @author Ville Vornanen
 */
public class CommonFixtures {
    private static final UUID ID
            = UUID.fromString("b0482e46-e68e-46e0-b148-d2d273ece5f3");

    /**
     * Provides a fixed identifier.
     *
     * @return guaranteed to return always the same instance
     */
    public static UUID getStandardId() {
        return ID;
    }
}
