package com.versatackle.cookbook.core;

import com.versatackle.cookbook.identifier.IdentifierGenerator;
import com.versatackle.cookbook.identifier.IdentifierGeneratorRegistry;
import java.util.UUID;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;

/**
 * Includes common helpers for entity unit tests.
 *
 * @author Ville Vornanen
 */
public abstract class AbstractEntityTest {

    @Mock
    protected IdentifierGenerator<UUID> idGeneratorMock;

    @BeforeMethod(alwaysRun = true)
    public void setUpIdentifierGeneratorMock() {
        MockitoAnnotations.initMocks(this);
        IdentifierGeneratorRegistry.getInstance()
                .registerGenerator(UUID.class, idGeneratorMock);
    }
}
