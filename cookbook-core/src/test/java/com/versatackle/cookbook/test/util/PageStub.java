package com.versatackle.cookbook.test.util;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

/**
 * An empty {@link Page} implementation which can be used for testing JSON responses
 * of the REST API.
 *
 * @author Ville Vornanen
 * @param <T> page content type
 */
public class PageStub<T> implements Page<T> {

    @Override
    public int getNumber() {
        return 0;
    }

    @Override
    public int getSize() {
        return 0;
    }

    @Override
    public int getTotalPages() {
        return 1;
    }

    @Override
    public int getNumberOfElements() {
        return 0;
    }

    @Override
    public long getTotalElements() {
        return 0;
    }

    @Override
    public boolean hasPrevious() {
        return false;
    }

    @Override
    public boolean isFirst() {
        return true;
    }

    @Override
    public boolean hasNext() {
        return false;
    }

    @Override
    public boolean isLast() {
        return true;
    }

    @Override
    public Pageable nextPageable() {
        return null;
    }

    @Override
    public Pageable previousPageable() {
        return null;
    }

    @Override
    public Iterator<T> iterator() {
        return Collections.emptyListIterator();
    }

    @Override
    public List<T> getContent() {
        return Collections.emptyList();
    }

    @Override
    public boolean hasContent() {
        return false;
    }

    @Override
    public Sort getSort() {
        return null;
    }

    @Override
    public <S> Page<S> map(Converter<? super T, ? extends S> cnvrtr) {
        return new PageStub<>();
    }

};
