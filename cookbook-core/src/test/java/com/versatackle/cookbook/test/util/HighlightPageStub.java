package com.versatackle.cookbook.test.util;

import java.util.Collections;
import java.util.List;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;
import org.springframework.data.solr.core.query.result.HighlightEntry;
import org.springframework.data.solr.core.query.result.HighlightPage;

/**
 * An empty {@link HighlightPage} implementation which can be used for testing JSON
 * responses of the REST API.
 *
 * @author Ville Vornanen
 * @param <T> page content type
 */
public class HighlightPageStub<T> extends PageStub<T>
        implements HighlightPage<T> {

    @Override
    public List<HighlightEntry<T>> getHighlighted() {
        return Collections.emptyList();
    }

    @Override
    public List<HighlightEntry.Highlight> getHighlights(T entity) {
        return Collections.emptyList();
    }

    @Override
    public <S> Page<S> map(Converter<? super T, ? extends S> cnvrtr) {
        return new HighlightPageStub<>();
    }

};
