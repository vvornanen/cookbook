package com.versatackle.cookbook.identifier;

import static org.mockito.Mockito.mock;
import static org.testng.Assert.*;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Unit test for {@link IdentifierGeneratorRegistry}.
 *
 * @author Ville Vornanen
 */
public class IdentifierGeneratorRegistryTest {

    private final IdentifierGeneratorRegistry registry
            = IdentifierGeneratorRegistry.getInstance();

    @BeforeMethod(alwaysRun = true)
    public void setUp() {
        registry.clearRegistry();
    }

    @Test
    public void testRegisteredProcessorIsFound() {
        IdentifierGenerator<Long> expected = mock(IdentifierGenerator.class);

        registry.registerGenerator(Long.class, expected);

        IdentifierGenerator actual = registry.getIdGenerator(Long.class);

        assertSame(actual, expected);
    }

    @Test(expectedExceptions = {IllegalArgumentException.class})
    public void testUnregisteredProcessorThrowsException() {
        IdentifierGeneratorRegistry.getInstance().getIdGenerator(Long.class);
    }
}
