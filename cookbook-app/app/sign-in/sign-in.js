'use strict';

var module = angular.module('cookbook.signIn', ['ngRoute', 'ngSanitize']);

module.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/sign-in', {
        templateUrl: 'sign-in/sign-in.html',
        controller: 'SignInController'
    });
}]);

module.controller('SignInController', function($scope, $location) {
    $scope.welcomeMessage = {
        title: "Welcome to Cookbook Demo!",
        content: "<p>Please sign in using the following credentials:</p>" +
        "<p>Username: <code>user</code> Password: <code>user</code></p>" +
        "<p>The database is restored automatically each hour sharp.</p>"
    };
    $scope.signedOut = false;
    $scope.signInError = false;

    $scope.signIn = function() {
        $scope.signedOut = false;
        $scope.signInError = false;
        $scope.$parent.signIn();
        $location.path('/recipes');
    }
});
