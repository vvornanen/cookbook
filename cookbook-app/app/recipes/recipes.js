'use strict';

var module = angular.module('cookbook.recipes', [
    'ngRoute', 'ngResource', 'ngSanitize']);

module.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/recipes', {
        templateUrl: 'recipes/recipe-list.html',
        controller: 'RecipeListController'
    });
    $routeProvider.when('/recipes/biscuits', {
        templateUrl: 'recipes/recipe.html',
        controller: 'RecipeController'
    });
    $routeProvider.when('/recipes/biscuits/edit', {
        templateUrl: 'recipes/edit-recipe.html',
        controller: 'EditRecipeController'
    });
    $routeProvider.when('/recipes/create', {
        templateUrl: 'recipes/create-recipe.html',
        controller: 'CreateRecipeController'
    });
}]);

module.controller('RecipeListController', function(
    $scope, $resource, $routeParams) {
    $scope.recipes = {"totalElements":6,"totalPages":1,"size":25,"page":0,"items":[{"item":{"name":"Biscuits","summary":"These biscuits are tall, light, and flaky. They are less salty than many bought mixes. They taste good plain or with butter, honey, jam, or sausage/ham gravy.","yield":null,"labels":["bread","breakfast"],"authors":[],"instructions":null,"created":null,"modified":null,"_links":{"self":{"href":"http://demo.cookbook.versatackle.com/cookbook/api/1.0/recipes/ba8cff28-0fd3-4437-a6a7-b36ae53fdec2","templated":false}}},"highlights":[]},{"item":{"name":"Coq au Vin","summary":"Many French regions claim coq au vin, or chicken stew, as their own, but legend has it that the recipe originated with Caesar's chef. Different variants exist throughout the country; the following recipe is one.","yield":null,"labels":["chicken","french","gluten-free","stew"],"authors":[],"instructions":null,"created":null,"modified":null,"_links":{"self":{"href":"http://demo.cookbook.versatackle.com/cookbook/api/1.0/recipes/405225e4-80d0-48b1-b168-ce5a8da823ce","templated":false}}},"highlights":[]},{"item":{"name":"Crème Brûlée","summary":"These instructions should provide a crackly crust over a cold custard, balanced in sweetness, egg and cream content. Few can resist its light, silky texture. The procedure is due to Dawn Yanagihara.","yield":null,"labels":["dessert","gluten-free","pudding-and-custard"],"authors":[],"instructions":null,"created":null,"modified":null,"_links":{"self":{"href":"http://demo.cookbook.versatackle.com/cookbook/api/1.0/recipes/f8a07e88-306b-46ab-96c4-403e24486380","templated":false}}},"highlights":[]},{"item":{"name":"Cupcakes","summary":"Cupcakes are small cakes placed in cake cases.","yield":null,"labels":["cake"],"authors":[],"instructions":null,"created":null,"modified":null,"_links":{"self":{"href":"http://demo.cookbook.versatackle.com/cookbook/api/1.0/recipes/2196f0f1-b86f-417e-98b8-84670baa5260","templated":false}}},"highlights":[]},{"item":{"name":"Ratatouille","summary":"Ratatouille is a traditional Provençale stewed vegetable dish that can be served as a meal on its own, accompanied by rice, potatoes or French bread, or as a side dish. It can be served hot or cold.","yield":null,"labels":["eggplant","french","tomato","vegan","vegetables","zucchini"],"authors":[],"instructions":null,"created":null,"modified":null,"_links":{"self":{"href":"http://demo.cookbook.versatackle.com/cookbook/api/1.0/recipes/a5715847-3d9c-4d97-bfae-17ac60274e38","templated":false}}},"highlights":[]},{"item":{"name":"Swedish Meatballs","summary":"All recipes are based on the following basic, or minimum, recipe for Swedish meatballs.","yield":null,"labels":["beef","swedish"],"authors":[],"instructions":null,"created":null,"modified":null,"_links":{"self":{"href":"http://demo.cookbook.versatackle.com/cookbook/api/1.0/recipes/10391bd2-7df0-4051-839d-50eb3675800a","templated":false}}},"highlights":[]}],"_links":{"self":{"href":"http://demo.cookbook.versatackle.com/cookbook/api/1.0/recipes?q=&page=0&size=25","templated":false},"first":{"href":"http://demo.cookbook.versatackle.com/cookbook/api/1.0/recipes?q=&page=0&size=25","templated":false},"last":{"href":"http://demo.cookbook.versatackle.com/cookbook/api/1.0/recipes?q=&page=0&size=25","templated":false}}};

    $scope.query = $routeParams.q;
});

module.controller('RecipeController', function(
    $scope, $resource, $translate, $timeout) {
    $scope.recipe = {"name":"Biscuits","summary":"These biscuits are tall, light, and flaky. They are less salty than many bought mixes. They taste good plain or with butter, honey, jam, or sausage/ham gravy.","yield":null,"labels":["bread","breakfast"],"authors":["Wikibooks: http://en.wikibooks.org/wiki/Cookbook:Biscuits CC-BY-SA"],"instructions":"<h3>Ingredients</h3>\r\n<ul><li>3 cups flour\r\n (Preferably a soft wheat flour; look for flour labeled &#34;soft wheat&#34; or\r\n&#34;better for biscuits&#34;. If no soft flour is available, mixing three parts\r\n all-purpose with one part cake flour will get you close.)</li><li>4 1/2 tsp. baking powder</li><li>3/4 tsp. cream of tartar</li><li>2 1/2 tablespoons sugar</li><li>3/4 tsp. salt</li><li>3/4 cup shortening</li><li>1 egg, beaten</li><li>1 cup milk</li></ul>\r\n<h3>Procedure</h3>\r\n<ol><li>Preheat oven.</li><li>Sift all dry ingredients into bowl.</li><li>Cut in shortening until like coarse meal.</li><li>Beat egg lightly and add to milk.</li><li>Add liquid to dry ingredients and mix with fork until dough holds together.</li><li>Turn out onto floured board and knead lightly with floured fingers.</li><li>Roll out 3/4 inch thick and cut with floured cutter (a round cookie cutter).</li><li>Place on baking sheet and bake at 450°F for 12 minutes.</li></ol>","created":"2016-03-05T12:00:00.086Z","modified":"2016-03-05T12:19:14.680Z","_links":{"self":{"href":"http://demo.cookbook.versatackle.com/cookbook/api/1.0/recipes/ba8cff28-0fd3-4437-a6a7-b36ae53fdec2","templated":false},"modifier":{"href":"http://demo.cookbook.versatackle.com/cookbook/api/1.0/people/565577af-4640-42f6-bc05-1ef67b7f7b69","templated":false}}};
    $scope.recipe.modified = "2016-03-05T17:08:00+02:00";
    $scope.recipe._links.creator = $scope.recipe._links.modifier;

    $scope.creator = {"username":"user","name":"Demo user","created":"2016-03-02T20:44:30.156Z","modified":null,"_links":{"self":{"href":"http://demo.cookbook.versatackle.com/cookbook/api/1.0/people/565577af-4640-42f6-bc05-1ef67b7f7b69","templated":false}}};
    $scope.modifier = $scope.creator;

    var updateCreatedText = function() {
        var code = $scope.recipe._links.creator ?
            'meta.created' : 'meta.createdByAnonymous';

        $translate(code, {
            timestamp: $scope.recipe.created,
            user: $scope.creator
        }).then(function(createdText) {
            $scope.createdText = createdText;
        });
    };

    var updateModifiedText = function() {
        if (!$scope.recipe.modified) {
            $scope.modifiedText = '';
        }

        var code = $scope.recipe._links.modifier ?
            'meta.modified' : 'meta.modifiedByAnonymous';

        $translate(code, {
            timestamp: $scope.recipe.modified,
            user: $scope.modifier
        }).then(function(createdText) {
            $scope.modifiedText = createdText;
        });
    };

    updateCreatedText();
    updateModifiedText();

    var refreshDates = function() {
        updateCreatedText();
        updateModifiedText();
        $timeout(refreshDates, 30000);
    };

    refreshDates();
});

module.controller('EditRecipeController', function($scope) {
    $scope.recipe = {"name":"Biscuits","summary":"These biscuits are tall, light, and flaky. They are less salty than many bought mixes. They taste good plain or with butter, honey, jam, or sausage/ham gravy.","yield":null,"labels":["bread","breakfast"],"authors":["Wikibooks: http://en.wikibooks.org/wiki/Cookbook:Biscuits CC-BY-SA"],"instructions":"<h3>Ingredients</h3>\r\n<ul><li>3 cups flour\r\n (Preferably a soft wheat flour; look for flour labeled &#34;soft wheat&#34; or\r\n&#34;better for biscuits&#34;. If no soft flour is available, mixing three parts\r\n all-purpose with one part cake flour will get you close.)</li><li>4 1/2 tsp. baking powder</li><li>3/4 tsp. cream of tartar</li><li>2 1/2 tablespoons sugar</li><li>3/4 tsp. salt</li><li>3/4 cup shortening</li><li>1 egg, beaten</li><li>1 cup milk</li></ul>\r\n<h3>Procedure</h3>\r\n<ol><li>Preheat oven.</li><li>Sift all dry ingredients into bowl.</li><li>Cut in shortening until like coarse meal.</li><li>Beat egg lightly and add to milk.</li><li>Add liquid to dry ingredients and mix with fork until dough holds together.</li><li>Turn out onto floured board and knead lightly with floured fingers.</li><li>Roll out 3/4 inch thick and cut with floured cutter (a round cookie cutter).</li><li>Place on baking sheet and bake at 450°F for 12 minutes.</li></ol>","created":"2016-03-05T12:00:00.086Z","modified":"2016-03-05T12:19:14.680Z","_links":{"self":{"href":"http://demo.cookbook.versatackle.com/cookbook/api/1.0/recipes/ba8cff28-0fd3-4437-a6a7-b36ae53fdec2","templated":false},"modifier":{"href":"http://demo.cookbook.versatackle.com/cookbook/api/1.0/people/565577af-4640-42f6-bc05-1ef67b7f7b69","templated":false}}};
});

module.controller('CreateRecipeController', function($scope) {
    $scope.recipe = {};
});

module.directive('recipeForm', function() {
    return {
        restrict: 'E',
        scope: {
            recipe: '='
        },
        templateUrl: 'recipes/edit-recipe-form.html',
        controller: function($scope) {
            $scope.messages = [];

            function getControl(controlName) {
                return $scope.editRecipeForm[controlName];
            }

            $scope.hasError = function(controlName, errorType) {
                if (errorType) {
                    return getControl(controlName).$error[errorType]
                        && getControl(controlName).$dirty;
                } else {
                    return getControl(controlName).$invalid
                        && getControl(controlName).$dirty;
                }
            };
        }
    };
});

module.directive('tooltip', function($translate) {
    return {
        restrict: 'A',
        scope: {
            tooltip: '@'
        },
        link: function($scope, $el) {
            $el.attr('title', $scope.tooltip);
            $el.tooltip();
        }
    }
});

module.directive('translateTooltip', function($translate) {
    return {
        restrict: 'A',
        link: function($scope, $el, $attrs) {

            $translate($attrs.title).then(function(translatedTitle) {
                $el.attr('title', translatedTitle);
                $el.tooltip();
            });

        }
    }
});

module.directive('recipeLabels', function() {
   return {
       restrict: 'E',
       scope: {
           labels: '='
       },
       templateUrl: 'recipes/labels.html'
   }
});

module.filter('fromNow', function($window) {
    return function(value) {
        return $window.moment(value).fromNow();
    }
});

module.filter('dateTime', function($window) {
    return function(value) {
        return $window.moment(value).format('LLLL');
    }
});

module.filter('linkToProfile', function() {
    return function(user) {
        return "<a href=\"#\">" + user.name + "</a>";
    }
});
