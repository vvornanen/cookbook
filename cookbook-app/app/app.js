'use strict';

// Declare app level module which depends on views, and components
var app = angular.module('cookbook', [
    'ngRoute',
    'pascalprecht.translate',
    'cookbook.signIn',
    'cookbook.recipes'
]);

app.config(function ($routeProvider, $httpProvider, $translateProvider) {
    $routeProvider.otherwise({redirectTo: '/sign-in'});

    $httpProvider.defaults.withCredentials = true;

    var locale = 'en';

    $translateProvider.useStaticFilesLoader({
        prefix: 'i18n/messages_',
        suffix: '.json'
    });
    $translateProvider.addInterpolation('$translateMessageFormatInterpolation');
    $translateProvider.preferredLanguage(locale);

    moment.locale(locale);
});

app.controller('ApplicationController', function ($scope, $resource) {
    var ServiceDirectory = $resource(
        'http://demo.cookbook.versatackle.com/cookbook/api/1.0/',
        null, null,
        {
            stripTrailingSlashes: false
        });

    $scope.serviceDirectory = ServiceDirectory.get();

    $scope.signIn = function() {
        $scope.authenticated = true;
        $scope.currentUser = {"username":"user","name":"Demo user","created":"2016-03-02T20:44:30.156Z","modified":null,"_links":{"self":{"href":"http://demo.cookbook.versatackle.com/cookbook/api/1.0/people/565577af-4640-42f6-bc05-1ef67b7f7b69","templated":false}}};
    };

    $scope.signOut = function() {
        $scope.authenticated = false;
        $scope.currentUser = {
            username: "anonymous",
            name: "Anonymous"
        };
    };
});
