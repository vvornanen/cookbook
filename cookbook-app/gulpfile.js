var postcss = require('gulp-postcss');
var gulp = require('gulp');
var autoprefixer = require('autoprefixer');
var mqpacker = require('css-mqpacker');
var csswring = require('csswring');
var sass = require('gulp-sass');
var props = require('gulp-props');

gulp.task('css', function () {
    var processors = [
        autoprefixer({browsers: ['last 1 version']}),
        mqpacker,
        csswring
    ];
    return gulp.src('src/main/scss/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(postcss(processors))
        .pipe(gulp.dest('app/css'));
});

gulp.task('messages', function() {
    gulp.src('src/main/resources/i18n/*.properties')
        .pipe(props({namespace: false}))
        .pipe(gulp.dest('app/i18n'));
});

gulp.task('watch', function () {
    gulp.watch('src/main/scss/**/*.scss', ['css']);
    gulp.watch('src/main/resources/i18n/*.properties', ['messages']);
});
