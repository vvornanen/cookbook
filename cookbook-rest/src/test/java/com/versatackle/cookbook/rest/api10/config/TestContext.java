package com.versatackle.cookbook.rest.api10.config;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.versatackle.cookbook.recipe.RecipeService;
import com.versatackle.cookbook.recipe.index.RecipeIndexService;
import com.versatackle.cookbook.user.UserService;
import javax.inject.Inject;
import static org.mockito.Mockito.mock;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author Ville Vornanen
 */
@Configuration
public class TestContext {

    @Inject
    private ObjectMapper objectMapper;

    @Bean
    public UserService userService() {
        return mock(UserService.class);
    }

    @Bean
    public RecipeService recipeService() {
        return mock(RecipeService.class);
    }

    @Bean
    public RecipeIndexService recipeIndexService() {
        return mock(RecipeIndexService.class);
    }

    @Bean
    public ObjectMapper objectMapper() {
        objectMapper.findAndRegisterModules();
        objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
        objectMapper.configure(
                SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);

        return objectMapper;
    }

}
