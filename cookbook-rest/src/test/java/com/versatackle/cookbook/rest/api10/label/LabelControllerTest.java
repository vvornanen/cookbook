package com.versatackle.cookbook.rest.api10.label;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableList;
import com.versatackle.cookbook.core.NotFoundException;
import com.versatackle.cookbook.recipe.index.RecipeIndexService;
import com.versatackle.cookbook.rest.api10.config.RestApi10Context;
import com.versatackle.cookbook.rest.api10.config.TestContext;
import java.util.List;
import javax.inject.Inject;
import static org.hamcrest.Matchers.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import org.mockito.Mockito;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.solr.core.query.Field;
import org.springframework.data.solr.core.query.result.FacetFieldEntry;
import static org.springframework.restdocs.RestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.RestDocumentation.modifyResponseTo;
import static org.springframework.restdocs.hypermedia.HypermediaDocumentation.linkWithRel;
import static org.springframework.restdocs.hypermedia.LinkExtractors.halLinks;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.response.ResponsePostProcessors.prettyPrintContent;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultHandler;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 *
 * @author Ville Vornanen
 */
@ContextConfiguration(classes = {
    RestApi10Context.class, TestContext.class
})
@WebAppConfiguration
public class LabelControllerTest extends AbstractTestNGSpringContextTests {

    private static final String CONTENT_TYPE_JSON = "application/json;charset=UTF-8";
    private static final String CONTENT_TYPE_HAL_JSON = "application/hal+json";

    private MockMvc mockMvc;

    @Inject
    private RecipeIndexService recipeIndexService;

    @Inject
    private WebApplicationContext webApplicationContext;

    @Inject
    private ObjectMapper objectMapper;

    @BeforeMethod
    public void setUp() {
        Mockito.reset(recipeIndexService);

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                .apply(documentationConfiguration()
                    .uris().withContextPath("/cookbook/api/1.0"))
                .build();
    }

    @Test
    public void testGetLabels() throws Exception {
        List<FacetFieldEntry> labels = ImmutableList.of(
                mockLabel("fresh", 3),
                mockLabel("low-fat", 5),
                mockLabel("yummy", 1));

        @SuppressWarnings("unchecked")
        Page<FacetFieldEntry> page = mock(Page.class);

        when(recipeIndexService.findLabels(anyString(), any(Pageable.class)))
                .thenReturn(page);

        when(page.getSize()).thenReturn(25);
        when(page.getNumberOfElements()).thenReturn(labels.size());
        when(page.getTotalPages()).thenReturn(1);
        when(page.getTotalElements()).thenReturn((long) labels.size());
        when(page.getContent()).thenReturn(labels);

        ResultHandler documentation = modifyResponseTo(prettyPrintContent())
                .andDocument("labels-get")
                .withLinks(halLinks(),
                        linkWithRel("self")
                                .description("Use this URL to reference this "
                                        + "collection"),
                        linkWithRel(LabelController.REL_FIRST)
                                .description("First page of this collection"),
                        linkWithRel(LabelController.REL_PREVIOUS)
                                .description("Previous page of this collection")
                                .optional(),
                        linkWithRel(LabelController.REL_NEXT)
                                .description("Next page of this collection")
                                .optional(),
                        linkWithRel(LabelController.REL_LAST)
                                .description("Last page of this collection"))
                .withQueryParameters(
                        parameterWithName(LabelController.PARAM_Q)
                                .description("Search query to filter results"),
                        parameterWithName(LabelController.PARAM_PAGE)
                                .description("Zero-based page number"),
                        parameterWithName(LabelController.PARAM_SIZE)
                                .description("Maximum number of items per page"));

        mockMvc.perform(get("/labels")
                        .param(LabelController.PARAM_Q, "")
                        .param(LabelController.PARAM_PAGE, "0")
                        .param(LabelController.PARAM_SIZE, "25"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(CONTENT_TYPE_HAL_JSON))
                .andExpect(jsonPath("$.totalElements", is(3)))
                .andExpect(jsonPath("$.totalPages", is(1)))
                .andExpect(jsonPath("$.page", is(0)))
                .andExpect(jsonPath("$.size", is(25)))
                .andExpect(jsonPath("$.items", not(empty())))
                .andDo(documentation);
    }

    @Test
    public void testGetRecipesIllegalPageNumber() throws Exception {
        mockMvc.perform(get("/labels")
                    .param(LabelController.PARAM_PAGE, "-1"))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(CONTENT_TYPE_JSON))
                .andExpect(jsonPath("$.status", is(400)))
                .andExpect(jsonPath("$.message",
                        is("Request could not be completed")))
                .andDo(modifyResponseTo(prettyPrintContent())
                        .andDocument("labels-get-illegal-page"));
    }

    @Test
    public void testGetRecipesIllegalPageSize() throws Exception {
        mockMvc.perform(get("/labels")
                    .param(LabelController.PARAM_SIZE, "0"))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(CONTENT_TYPE_JSON))
                .andExpect(jsonPath("$.status", is(400)))
                .andExpect(jsonPath("$.message",
                        is("Request could not be completed")))
                .andDo(modifyResponseTo(prettyPrintContent())
                        .andDocument("labels-get-illegal-size"));
    }

    @Test
    public void testGetLabel() throws Exception {
        String label = "yummy";
        int numberOfRecipes = 5;
        FacetFieldEntry entry = mockLabel(label, numberOfRecipes);

        when(recipeIndexService.findLabel(label))
                .thenReturn(entry);

        String expectedRecipesUrl = "http://localhost:8080/cookbook/api/1.0/recipes"
                + "?q=label:yummy&page=0&size=25";

        ResultHandler documentation = modifyResponseTo(prettyPrintContent())
                .andDocument("label-get")
                .withLinks(halLinks(),
                        linkWithRel("self")
                                .description("Use this URL to reference this "
                                        + "collection"),
                        linkWithRel(LabelResourceAssembler.REL_RECIPES)
                                .description("Search for recipes having this "
                                        + "label"));

        mockMvc.perform(get("/labels/{label}", label))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(CONTENT_TYPE_HAL_JSON))
                .andExpect(jsonPath("$.label", is(label)))
                .andExpect(jsonPath("$.numberOfRecipes", is(numberOfRecipes)))
                .andExpect(jsonPath("$._links.recipes.href",
                        is(expectedRecipesUrl)))
                .andDo(documentation);
    }

    @Test
    public void testGetLabelNotFound() throws Exception {
        String label = "yummy";

        String developerMessage = String.format("Label [%s] was not found", label);

        when(recipeIndexService.findLabel(anyString()))
                .thenThrow(new NotFoundException(developerMessage));

        mockMvc.perform(get("/labels/{label}", label))
                .andDo(modifyResponseTo(prettyPrintContent())
                        .andDocument("label-get-not-found"))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(CONTENT_TYPE_JSON))
                .andExpect(jsonPath("$.status", is(404)))
                .andExpect(jsonPath("$.message",
                        is("The requested resource was not found")))
                .andExpect(jsonPath("$.developerMessage", is(developerMessage)));
    }

    private static FacetFieldEntry mockLabel(String label, long numberOfRecipes) {
        Field field = mock(Field.class);
        when(field.getName()).thenReturn("label");

        FacetFieldEntry entry = mock(FacetFieldEntry.class);
        when(entry.getKey()).thenReturn(field);
        when(entry.getField()).thenReturn(field);
        when(entry.getValue()).thenReturn(label);
        when(entry.getValueCount()).thenReturn(numberOfRecipes);

        return entry;
    }

}
