package com.versatackle.cookbook.rest.api10;

import java.time.Instant;
import static org.testng.Assert.*;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Unit test for {@link AbstractResource}.
 *
 * @author Ville Vornanen
 */
public class AbstractResourceTest {

    private static class SimpleResource extends AbstractResource {}

    private SimpleResource resource;

    @BeforeMethod
    public void setUp() {
        resource = new SimpleResource();
    }

    @Test
    public void testCreated() {
        Instant expected = Instant.now();
        resource.setCreated(expected);

        assertEquals(resource.getCreated(), expected);
    }

    @Test
    public void testCreatedIsNull() {
        assertNull(resource.getCreated());
    }

    @Test
    public void testModifiedIsNull() {
        assertNull(resource.getModified());
    }

    @Test
    public void testModified() {
        Instant expected = Instant.now();
        resource.setModified(expected);

        assertEquals(resource.getModified(), expected);
    }

}
