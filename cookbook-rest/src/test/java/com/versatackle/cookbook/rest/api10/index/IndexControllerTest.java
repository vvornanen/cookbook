package com.versatackle.cookbook.rest.api10.index;

import com.versatackle.cookbook.rest.api10.config.RestApi10Context;
import com.versatackle.cookbook.rest.api10.config.TestContext;
import javax.inject.Inject;
import static org.springframework.restdocs.RestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.RestDocumentation.modifyResponseTo;
import static org.springframework.restdocs.hypermedia.HypermediaDocumentation.linkWithRel;
import static org.springframework.restdocs.hypermedia.LinkExtractors.halLinks;
import static org.springframework.restdocs.response.ResponsePostProcessors.prettyPrintContent;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultHandler;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 *
 * @author Ville Vornanen
 */
@ContextConfiguration(classes = {
    RestApi10Context.class, TestContext.class
})
@WebAppConfiguration
public class IndexControllerTest extends AbstractTestNGSpringContextTests {

    private static final String CONTENT_TYPE_HAL_JSON = "application/hal+json";

    private MockMvc mockMvc;

    @Inject
    private WebApplicationContext webApplicationContext;

    @BeforeMethod
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                .apply(documentationConfiguration()
                    .uris().withContextPath("/cookbook/api/1.0"))
                .build();
    }

    @Test
    public void testGetIndex() throws Exception {
        ResultHandler documentation = modifyResponseTo(prettyPrintContent())
                .andDocument("index-get")
                .withLinks(halLinks(),
                        linkWithRel(IndexController.REL_PEOPLE)
                                .description("People collection"),
                        linkWithRel(IndexController.REL_RECIPES)
                                .description("Recipes collection"),
                        linkWithRel(IndexController.REL_LABELS)
                                .description("Labels collection"));

        mockMvc.perform(get("/"))
                .andDo(documentation)
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(CONTENT_TYPE_HAL_JSON));
    }

}
