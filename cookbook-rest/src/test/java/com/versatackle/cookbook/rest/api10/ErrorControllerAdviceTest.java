package com.versatackle.cookbook.rest.api10;

import com.versatackle.cookbook.core.NotFoundException;
import static org.testng.Assert.*;
import org.testng.annotations.Test;

/**
 *
 * @author Ville Vornanen
 */
public class ErrorControllerAdviceTest {

    private final ErrorControllerAdvice errorControllerAdvice
            = new ErrorControllerAdvice();

    @Test
    public void testNotFound() {
        NotFoundException ex = new NotFoundException("Testing not found");
        ResourceError resource = errorControllerAdvice.notFound(ex);

        assertEquals(resource.getStatus(), 404);
        assertEquals(resource.getCode(), ResourceErrors.NOT_FOUND.getCode());
        assertEquals(resource.getMessage(), "The requested resource was not found");
        assertEquals(resource.getDeveloperMessage(), ex.getMessage());
    }

}
