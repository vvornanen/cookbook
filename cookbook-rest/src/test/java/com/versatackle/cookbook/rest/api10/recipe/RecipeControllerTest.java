package com.versatackle.cookbook.rest.api10.recipe;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.versatackle.cookbook.core.NotFoundException;
import com.versatackle.cookbook.recipe.Label;
import com.versatackle.cookbook.recipe.Recipe;
import com.versatackle.cookbook.recipe.RecipeDTO;
import com.versatackle.cookbook.recipe.RecipeMatchers;
import com.versatackle.cookbook.recipe.RecipeService;
import com.versatackle.cookbook.recipe.fixtures.RecipeFixtures;
import com.versatackle.cookbook.recipe.index.RecipeDocument;
import com.versatackle.cookbook.recipe.index.RecipeIndexService;
import com.versatackle.cookbook.rest.api10.config.RestApi10Context;
import com.versatackle.cookbook.rest.api10.config.TestContext;
import com.versatackle.cookbook.user.fixtures.UserFixtures;
import java.time.Instant;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import static java.util.stream.Collectors.toSet;
import javax.inject.Inject;
import static org.hamcrest.MatcherAssert.assertThat;
import org.hamcrest.Matchers;
import static org.hamcrest.Matchers.*;
import org.mockito.ArgumentCaptor;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import org.mockito.Mockito;
import static org.mockito.Mockito.*;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.solr.core.query.result.HighlightEntry;
import org.springframework.data.solr.core.query.result.HighlightPage;
import static org.springframework.restdocs.RestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.RestDocumentation.modifyResponseTo;
import static org.springframework.restdocs.hypermedia.HypermediaDocumentation.linkWithRel;
import static org.springframework.restdocs.hypermedia.LinkExtractors.halLinks;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.response.ResponsePostProcessors.prettyPrintContent;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultHandler;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import static org.testng.Assert.assertEquals;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Unit test for {@link RecipesController}.
 *
 * @author Ville Vornanen
 */
@ContextConfiguration(classes = {
    RestApi10Context.class, TestContext.class
})
@WebAppConfiguration
public class RecipeControllerTest extends AbstractTestNGSpringContextTests {

    private static final String CONTENT_TYPE_JSON = "application/json;charset=UTF-8";
    private static final String CONTENT_TYPE_HAL_JSON = "application/hal+json";

    private MockMvc mockMvc;

    @Inject
    private RecipeService recipeService;

    @Inject
    private RecipeIndexService recipeIndexService;

    @Inject
    private WebApplicationContext webApplicationContext;

    @Inject
    private ObjectMapper objectMapper;

    private final Recipe recipe = RecipeFixtures.newStandardRecipe();

    @BeforeMethod
    public void setUp() throws Exception {
        Mockito.reset(recipeService);

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                .apply(documentationConfiguration()
                    .uris().withContextPath("/cookbook/api/1.0"))
                .build();

        when(recipeService.findById(recipe.getId())).thenReturn(recipe);

        recipe.setCreator(UserFixtures.mickeyWalmsley());
    }

    @Test
    public void testGetRecipes() throws Exception {
        RecipeDocument recipeDocument = RecipeFixtures.newStandardRecipeDocument();
        recipeDocument.setCreated(Instant.now());
        recipeDocument.setModified(Instant.now());

        @SuppressWarnings("unchecked")
        HighlightPage<RecipeDocument> page = mock(HighlightPage.class);

        HighlightEntry<RecipeDocument> highlight
                = new HighlightEntry<>(recipeDocument);

        when(page.getHighlighted())
                .thenReturn(ImmutableList.of(highlight));
        when(page.getSize()).thenReturn(25);
        when(page.getNumberOfElements()).thenReturn(1);
        when(page.getTotalPages()).thenReturn(1);
        when(page.getTotalElements()).thenReturn(1L);

        when(recipeIndexService.search(
                anyString(), any(Pageable.class), any(Sort.class)))
                .thenReturn(page);

        ResultHandler documentation = modifyResponseTo(prettyPrintContent())
                .andDocument("recipes-get")
                .withLinks(halLinks(),
                        linkWithRel("self")
                                .description("Use this URL to reference this "
                                        + "collection"),
                        linkWithRel(RecipeController.REL_FIRST)
                                .description("First page of this collection"),
                        linkWithRel(RecipeController.REL_PREVIOUS)
                                .description("Previous page of this collection")
                                .optional(),
                        linkWithRel(RecipeController.REL_NEXT)
                                .description("Next page of this collection")
                                .optional(),
                        linkWithRel(RecipeController.REL_LAST)
                                .description("Last page of this collection"))
                .withQueryParameters(
                        parameterWithName(RecipeController.PARAM_Q)
                                .description("Search query to filter results"),
                        parameterWithName(RecipeController.PARAM_PAGE)
                                .description("Zero-based page number"),
                        parameterWithName(RecipeController.PARAM_SIZE)
                                .description("Maximum number of items per page"));

        mockMvc.perform(get("/recipes")
                        .param(RecipeController.PARAM_Q, "")
                        .param(RecipeController.PARAM_PAGE, "0")
                        .param(RecipeController.PARAM_SIZE, "25"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(CONTENT_TYPE_HAL_JSON))
                .andExpect(jsonPath("$.totalElements", is(1)))
                .andExpect(jsonPath("$.totalPages", is(1)))
                .andExpect(jsonPath("$.page", is(0)))
                .andExpect(jsonPath("$.size", is(25)))
                .andExpect(jsonPath("$.items", not(empty())))
                .andDo(documentation);
    }

    @Test
    public void testGetRecipesWithHighlights() throws Exception {
        RecipeDocument recipeDocument = RecipeFixtures.newStandardRecipeDocument();
        recipeDocument.setCreated(Instant.now());
        recipeDocument.setModified(Instant.now());

        @SuppressWarnings("unchecked")
        HighlightPage<RecipeDocument> page = mock(HighlightPage.class);

        HighlightEntry<RecipeDocument> highlight
                = new HighlightEntry<>(recipeDocument);
        highlight.addSnipplets("test", ImmutableList.of("Hello"));

        when(page.getHighlighted())
                .thenReturn(ImmutableList.of(highlight));
        when(page.getSize()).thenReturn(25);
        when(page.getNumberOfElements()).thenReturn(1);
        when(page.getTotalPages()).thenReturn(1);
        when(page.getTotalElements()).thenReturn(1L);

        when(recipeIndexService.search(
                anyString(), any(Pageable.class), any(Sort.class)))
                .thenReturn(page);
        mockMvc.perform(get("/recipes"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(CONTENT_TYPE_HAL_JSON))
                .andExpect(jsonPath("$.totalElements", is(1)))
                .andExpect(jsonPath("$.totalPages", is(1)))
                .andExpect(jsonPath("$.page", is(0)))
                .andExpect(jsonPath("$.size", is(25)))
                .andExpect(jsonPath("$.items", not(empty())))
                .andExpect(jsonPath("$.items[0].highlights[0]", is("Hello")))
                .andExpect(jsonPath("$.items[0].item.name",
                        is(recipeDocument.getName())));
    }

    @Test
    public void testGetRecipesIllegalPageNumber() throws Exception {
        mockMvc.perform(get("/recipes")
                    .param(RecipeController.PARAM_PAGE, "-1"))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(CONTENT_TYPE_JSON))
                .andExpect(jsonPath("$.status", is(400)))
                .andExpect(jsonPath("$.message",
                        is("Request could not be completed")))
                .andDo(modifyResponseTo(prettyPrintContent())
                        .andDocument("recipes-get-illegal-page"));
    }

    @Test
    public void testGetRecipesIllegalPageSize() throws Exception {
        mockMvc.perform(get("/recipes")
                    .param(RecipeController.PARAM_SIZE, "0"))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(CONTENT_TYPE_JSON))
                .andExpect(jsonPath("$.status", is(400)))
                .andExpect(jsonPath("$.message",
                        is("Request could not be completed")))
                .andDo(modifyResponseTo(prettyPrintContent())
                        .andDocument("recipes-get-illegal-size"));
    }

    @Test
    public void testCreateRecipe() throws Exception {
        Recipe expected = new Recipe(UUID.randomUUID(), "No-Bake Cake");
        expected.setSummary("This delicious dessert is super easy to make");
        expected.setYield("Serves 4 people");

        Set<String> labels = ImmutableSet.of("new", "yummy");

        expected.addLabels(labels.stream()
                .map(Label::valueOf)
                .collect(Collectors.toSet()));
        expected.addAuthor("Ilonka Basurto");
        expected.setInstructions("Mix and let chill.");
        expected.setCreator(UserFixtures.mickeyWalmsley());

        Map<String, Object> data = new LinkedHashMap<>();
        data.put("name", expected.getName());
        data.put("summary", expected.getSummary());
        data.put("yield", expected.getYield());
        data.put("labels", labels);
        data.put("authors", expected.getAuthors());
        data.put("instructions", expected.getInstructions());

        when(recipeService.add(any(RecipeDTO.class)))
                .thenReturn(expected);

        mockMvc.perform(post("/recipes")
                .content(objectMapper.writeValueAsString(data))
                .contentType(CONTENT_TYPE_JSON))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(content().contentType(CONTENT_TYPE_HAL_JSON))
                .andDo(modifyResponseTo(prettyPrintContent())
                    .andDocument("recipes-post"));

        ArgumentCaptor<RecipeDTO> captor = ArgumentCaptor.forClass(RecipeDTO.class);

        verify(recipeService).add(captor.capture());

        RecipeDTO actual = captor.getValue();

        assertEquals(actual.getName(), expected.getName());
        assertEquals(actual.getSummary(), expected.getSummary());
        assertEquals(actual.getYield(), expected.getYield());
        assertEquals(actual.getLabelSet(), labels);
        assertEquals(actual.getAuthorsAsList(), expected.getAuthors());
        assertEquals(actual.getInstructions(), expected.getInstructions());
    }

    @Test
    public void testGetRecipe() throws Exception {
        ResultHandler documentation = modifyResponseTo(prettyPrintContent())
                .andDocument("recipe-get")
                .withLinks(halLinks(),
                        linkWithRel("self")
                                .description("Use this URL to reference this "
                                        + "recipe"),
                        linkWithRel("creator")
                                .description("Person who created the recipe")
                                .optional(),
                        linkWithRel("modifier")
                                .description("Person who last modified the recipe")
                                .optional());

        mockMvc.perform(get("/recipes/" + recipe.getId()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(CONTENT_TYPE_HAL_JSON))
                .andExpect(jsonPath("$.name", is(recipe.getName())))
                .andExpect(jsonPath("$.summary", is(recipe.getSummary())))
                .andExpect(jsonPath("$.yield", is(recipe.getYield())))
                .andExpect(jsonPath("$.labels", hasSize(3)))
                .andExpect(jsonPath("$.labels[0]", is("yummy")))
                .andExpect(jsonPath("$.labels[1]", is("fresh")))
                .andExpect(jsonPath("$.labels[2]", is("low-fat")))
                .andExpect(jsonPath("$.authors", hasSize(2)))
                .andExpect(jsonPath("$.authors[0]", is("Standard Author")))
                .andExpect(jsonPath("$.authors[1]", is("Second Author")))
                .andExpect(jsonPath("$.instructions", is(recipe.getInstructions())))
                .andExpect(jsonPath("$.created", is(recipe.getCreated().toString())))
                .andDo(documentation);
    }

    @Test
    public void testGetRecipeNotFound() throws Exception {
        String developerMessage = String.format(
                "Recipe [%s] was not found", recipe.getId());

        when(recipeService.findById(recipe.getId()))
                .thenThrow(new NotFoundException(developerMessage));

        mockMvc.perform(get("/recipes/" + recipe.getId()))
                .andDo(modifyResponseTo(prettyPrintContent())
                        .andDocument("recipe-get-not-found"))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(CONTENT_TYPE_JSON))
                .andExpect(jsonPath("$.status", is(404)))
                .andExpect(jsonPath("$.message",
                        is("The requested resource was not found")))
                .andExpect(jsonPath("$.developerMessage", is(developerMessage)));
    }

    @Test
    public void testUpdateRecipeName() throws Exception {
        String expected = "No-Bake Cake Mk. 2";
        String patch = new PatchBuilder().replace("/name", expected).build();

        when(recipeService.update(any(RecipeDTO.class)))
                .thenAnswer(i -> Recipe.fromDTO((RecipeDTO) i.getArguments()[0]));

        mockMvc.perform(post("/recipes/{id}", recipe.getId())
                .content(patch)
                .contentType(CONTENT_TYPE_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andDo(modifyResponseTo(prettyPrintContent())
                    .andDocument("recipe-post-name"));

        ArgumentCaptor<RecipeDTO> captor = ArgumentCaptor.forClass(RecipeDTO.class);

        verify(recipeService).update(captor.capture());

        RecipeDTO actual = captor.getValue();

        assertEquals(actual.getName(), expected);
    }

    @Test
    public void testUpdateRecipeNameIsEmpty() throws Exception {
        String expected = "";
        String patch = new PatchBuilder().replace("/name", expected).build();

        when(recipeService.update(any(RecipeDTO.class)))
                .thenAnswer(i -> Recipe.fromDTO((RecipeDTO) i.getArguments()[0]));

        mockMvc.perform(post("/recipes/{id}", recipe.getId())
                .content(patch)
                .contentType(CONTENT_TYPE_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andDo(modifyResponseTo(prettyPrintContent())
                    .andDocument("recipe-post-name-empty"));

        ArgumentCaptor<RecipeDTO> captor = ArgumentCaptor.forClass(RecipeDTO.class);

        verify(recipeService).update(captor.capture());

        RecipeDTO actual = captor.getValue();

        assertEquals(actual.getName(), expected);
    }

    @Test
    public void testUpdateRecipeMultipleFields() throws Exception {
        String expectedName = "No-Bake Cake Mk. 2";
        String expectedSummary = "An improved recipe";
        Set<String> expectedLabels = ImmutableSet.of("yummy", "improved");

        String patch = new PatchBuilder()
                .replace("/name", expectedName)
                .replace("/summary", expectedSummary)
                .replace("/labels", expectedLabels)
                .build();

        when(recipeService.update(any(RecipeDTO.class)))
                .thenAnswer(i -> Recipe.fromDTO((RecipeDTO) i.getArguments()[0]));

        mockMvc.perform(post("/recipes/{id}", recipe.getId())
                .content(patch)
                .contentType(CONTENT_TYPE_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andDo(modifyResponseTo(prettyPrintContent())
                    .andDocument("recipe-post-multiple"));

        ArgumentCaptor<RecipeDTO> captor = ArgumentCaptor.forClass(RecipeDTO.class);

        verify(recipeService).update(captor.capture());

        RecipeDTO actual = captor.getValue();

        assertThat(actual, Matchers.allOf(
                RecipeMatchers.nameEq(expectedName),
                RecipeMatchers.summaryEq(expectedSummary),
                RecipeMatchers.labelsEq(expectedLabels)));
    }

    @Test
    public void testUpdateRecipeUnknownOperation() throws Exception {
        String operation = "justdoit";
        String patch = new PatchBuilder()
                .addOperation(operation, "/name", "invalid")
                .build();

        mockMvc.perform(post("/recipes/{id}", recipe.getId())
                .content(patch)
                .contentType(CONTENT_TYPE_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(CONTENT_TYPE_JSON))
                .andExpect(jsonPath("$.status", is(400)))
                .andExpect(jsonPath("$.message",
                        is("Request could not be completed")))
                .andDo(modifyResponseTo(prettyPrintContent())
                    .andDocument("recipe-post-unknown-operation"));
    }

    @Test
    public void testUpdateRecipeInvalidPath() throws Exception {
        String path = "/shouldnotexist";
        String patch = new PatchBuilder().replace(path, true).build();

        String developerMessage = String.format(
                "Invalid path [%s] for replace operation", path);

        mockMvc.perform(post("/recipes/{id}", recipe.getId())
                .content(patch)
                .contentType(CONTENT_TYPE_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(CONTENT_TYPE_JSON))
                .andExpect(jsonPath("$.status", is(400)))
                .andExpect(jsonPath("$.message",
                        is("Request could not be completed")))
                .andExpect(jsonPath("$.developerMessage", is(developerMessage)))
                .andDo(modifyResponseTo(prettyPrintContent())
                    .andDocument("recipe-post-invalid-path"));
    }

    @Test
    public void testUpdateRecipeAllFields() throws Exception {
        RecipeDTO expected = new RecipeDTO();
        expected.setId(recipe.getId());
        expected.setName("No-Bake Cake Mk. 2");
        expected.setSummary("An improved recipe");
        expected.setYield("4 servings");
        expected.setLabels(ImmutableSet.of("yummy", "improved"));
        expected.setAuthors(ImmutableList.of("Rearden Borg"));
        expected.setInstructions("Instructions for the improved recipe.");

        String patch = new PatchBuilder()
                .replace("/name", expected.getName())
                .replace("/summary", expected.getSummary())
                .replace("/yield", expected.getYield())
                .replace("/labels", expected.getLabelSet())
                .replace("/authors", expected.getAuthorsAsList())
                .replace("/instructions", expected.getInstructions())
                .build();

        when(recipeService.update(any(RecipeDTO.class)))
                .thenAnswer(i -> Recipe.fromDTO((RecipeDTO) i.getArguments()[0]));

        mockMvc.perform(post("/recipes/{id}", recipe.getId())
                .content(patch)
                .contentType(CONTENT_TYPE_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andDo(modifyResponseTo(prettyPrintContent())
                    .andDocument("recipe-post-all"));

        ArgumentCaptor<RecipeDTO> captor = ArgumentCaptor.forClass(RecipeDTO.class);

        verify(recipeService).update(captor.capture());

        RecipeDTO actual = captor.getValue();

        assertThat(actual, RecipeMatchers.eq(expected));
    }

    @Test
    public void testUpdateRecipeNotFound() throws Exception {
        String expected = "No-Bake Cake Mk. 2";
        String patch = new PatchBuilder().replace("/name", expected).build();

        String developerMessage = String.format(
                "Recipe [%s] was not found", recipe.getId());

        when(recipeService.findById(recipe.getId()))
                .thenThrow(new NotFoundException(developerMessage));

        mockMvc.perform(post("/recipes/{id}", recipe.getId())
                .content(patch)
                .contentType(CONTENT_TYPE_JSON))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(CONTENT_TYPE_JSON))
                .andExpect(jsonPath("$.status", is(404)))
                .andExpect(jsonPath("$.message",
                        is("The requested resource was not found")))
                .andExpect(jsonPath("$.developerMessage", is(developerMessage)))
                .andDo(modifyResponseTo(prettyPrintContent())
                    .andDocument("recipe-post-not-found"));
    }

    @Test
    public void testReplaceRecipe() throws Exception {
        RecipeDTO expected = new RecipeDTO();
        expected.setId(recipe.getId());
        expected.setName("Replaced recipe");
        expected.setSummary("A completely renewed recipe");
        expected.setYield("More plentiful than before");
        expected.setLabels(ImmutableSet.of("new", "neuf", "niuwe"));
        expected.setAuthors(ImmutableList.of("Fedlimid Norris"));
        expected.setInstructions(
                "Hiss at vacuum cleaner hide at bottom of staircase to trip human.");

        Map<String, Object> data = new LinkedHashMap<>();
        data.put("name", expected.getName());
        data.put("summary", expected.getSummary());
        data.put("yield", expected.getYield());
        data.put("labels", expected.getLabelSet());
        data.put("authors", expected.getAuthorsAsList());
        data.put("instructions", expected.getInstructions());

        Recipe updated = new Recipe(recipe.getId(), expected.getName());
        updated.setSummary(expected.getSummary());
        updated.setYield(expected.getYield());
        expected.getLabelSet()
                .stream()
                .map(Label::valueOf)
                .forEach(updated::addLabel);
        updated.addAuthors(expected.getAuthorsAsList());
        updated.setInstructions(expected.getInstructions());

        when(recipeService.update(expected))
                .thenReturn(updated);

        mockMvc.perform(put("/recipes/{id}", recipe.getId())
                .content(objectMapper.writeValueAsString(data))
                .contentType(CONTENT_TYPE_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(CONTENT_TYPE_HAL_JSON))
                .andDo(modifyResponseTo(prettyPrintContent())
                    .andDocument("recipe-put"));

        ArgumentCaptor<RecipeDTO> argumentCaptor
                = ArgumentCaptor.forClass(RecipeDTO.class);

        verify(recipeService).update(argumentCaptor.capture());

        RecipeDTO actual = argumentCaptor.getValue();

        assertThat(actual, RecipeMatchers.eq(expected));
    }

    @Test
    public void testReplaceRecipeNotFound() throws Exception {
        Map<String, Object> data = new LinkedHashMap<>();
        data.put("name", "Replaced recipe");
        data.put("summary", "A completely renewed recipe");
        data.put("yield", "More plentiful than before");
        data.put("labels", ImmutableSet.of("new", "neuf", "niuwe"));
        data.put("authors", ImmutableList.of("Fedlimid Norris"));
        data.put("instructions",
                "Hiss at vacuum cleaner hide at bottom of staircase to trip human.");

        String developerMessage = "Recipe was not found";
        when(recipeService.update(any(RecipeDTO.class)))
                .thenThrow(new NotFoundException(developerMessage));

        mockMvc.perform(put("/recipes/{id}", recipe.getId())
                .content(objectMapper.writeValueAsString(data))
                .contentType(CONTENT_TYPE_JSON))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(CONTENT_TYPE_JSON))
                .andExpect(jsonPath("$.status", is(404)))
                .andExpect(jsonPath("$.message",
                        is("The requested resource was not found")))
                .andExpect(jsonPath("$.developerMessage",
                        is(developerMessage)))
                .andDo(modifyResponseTo(prettyPrintContent())
                    .andDocument("recipe-put-not-found"));
    }

    @Test
    public void testDeleteRecipe() throws Exception {
        mockMvc.perform(delete("/recipes/{id}", recipe.getId()))
                .andDo(print())
                .andExpect(status().isNoContent())
                .andDo(modifyResponseTo(prettyPrintContent())
                        .andDocument("recipe-delete"));

        verify(recipeService).deleteById(recipe.getId());
    }

    @Test
    public void testDeleteRecipeNotFound() throws Exception {
        String developerMessage = String.format(
                "Recipe [%s] was not found", recipe.getId());

        when(recipeService.deleteById(any(UUID.class)))
                .thenThrow(new NotFoundException(developerMessage));

        mockMvc.perform(delete("/recipes/{id}", recipe.getId()))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(CONTENT_TYPE_JSON))
                .andExpect(jsonPath("$.status", is(404)))
                .andExpect(jsonPath("$.message",
                        is("The requested resource was not found")))
                .andExpect(jsonPath("$.developerMessage", is(developerMessage)))
                .andDo(modifyResponseTo(prettyPrintContent())
                        .andDocument("recipe-delete-not-found"));
    }

    private class PatchBuilder {

        private final List<Map<String, Object>> operations = new ArrayList<>();

        PatchBuilder addOperation(String operation, String path, Object value) {
            operations.add(ImmutableMap.of(
                "op", operation,
                "path", path,
                "value", value));
            return this;
        }

        PatchBuilder replace(String path, Object value) {
            return addOperation("replace", path, value);
        }

        String build() throws JsonProcessingException {
            return objectMapper.writeValueAsString(operations);
        }

    }

}
