package com.versatackle.cookbook.rest.api10.person;

import com.google.common.collect.ImmutableList;
import com.versatackle.cookbook.core.NotFoundException;
import com.versatackle.cookbook.rest.api10.config.RestApi10Context;
import com.versatackle.cookbook.rest.api10.config.TestContext;
import com.versatackle.cookbook.user.User;
import com.versatackle.cookbook.user.UserService;
import com.versatackle.cookbook.user.fixtures.UserFixtures;
import java.util.UUID;
import javax.inject.Inject;
import static org.hamcrest.Matchers.*;
import static org.mockito.Matchers.any;
import org.mockito.Mockito;
import static org.mockito.Mockito.when;
import static org.springframework.restdocs.RestDocumentation.*;
import static org.springframework.restdocs.hypermedia.HypermediaDocumentation.linkWithRel;
import static org.springframework.restdocs.hypermedia.LinkExtractors.halLinks;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.response.ResponsePostProcessors.prettyPrintContent;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultHandler;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Unit test for {@link PersonController}.
 *
 * @author Ville Vornanen
 */
@ContextConfiguration(classes = {
    RestApi10Context.class, TestContext.class
})
@WebAppConfiguration
public class PersonControllerTest extends AbstractTestNGSpringContextTests {

    private static final String CONTENT_TYPE_HAL_JSON = "application/hal+json";
    private static final String CONTENT_TYPE_JSON = "application/json;charset=UTF-8";

    private MockMvc mockMvc;

    @Inject
    private UserService userService;

    @Inject
    private WebApplicationContext webApplicationContext;

    private final User user = UserFixtures.mickeyWalmsley();
    private final ImmutableList<User> users = ImmutableList.of(
            UserFixtures.mickeyWalmsley(),
            UserFixtures.joslynHightower());


    @BeforeMethod
    public void setUp() throws Exception {
        Mockito.reset(userService);

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                .apply(documentationConfiguration()
                    .uris().withContextPath("/cookbook/api/1.0"))
                .build();

        when(userService.findById(user.getId())).thenReturn(user);
        when(userService.findAll()).thenReturn(users);
    }

    @Test
    public void testGetPerson() throws Exception {
        mockMvc.perform(get(String.format("/people/%s", user.getId())))
                .andDo(modifyResponseTo(prettyPrintContent())
                        .andDocument("person-get"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(CONTENT_TYPE_HAL_JSON))
                .andExpect(jsonPath("$.name", is(user.getName())))
                .andExpect(jsonPath("$.username", is(user.getUsername())))
                .andExpect(jsonPath("$.created", is(user.getCreated().toString())));
    }

    @Test
    public void testGetPersonNotFound() throws Exception {
        when(userService.findById(any(UUID.class)))
                .thenThrow(new NotFoundException(String.format(
                        "User [%s] was not found", user.getId())));

        mockMvc.perform(get(String.format("/people/%s", user.getId())))
                .andDo(modifyResponseTo(prettyPrintContent())
                        .andDocument("person-get-not-found"))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(CONTENT_TYPE_JSON));
    }

    @Test
    public void testGetPeople() throws Exception {
        ResultHandler documentation = modifyResponseTo(prettyPrintContent())
                .andDocument("people-get")
                .withLinks(halLinks(),
                        linkWithRel("self")
                                .description("Use this URL to reference this "
                                        + "collection"),
                        linkWithRel(PersonController.REL_FIRST)
                                .description("First page of this collection"),
                        linkWithRel(PersonController.REL_PREVIOUS)
                                .description("Previous page of this collection")
                                .optional(),
                        linkWithRel(PersonController.REL_NEXT)
                                .description("Next page of this collection")
                                .optional(),
                        linkWithRel(PersonController.REL_LAST)
                                .description("Last page of this collection"))
                .withQueryParameters(
                        parameterWithName(PersonController.PARAM_Q)
                                .description("Search query to filter results"),
                        parameterWithName(PersonController.PARAM_PAGE)
                                .description("Zero-based page number"),
                        parameterWithName(PersonController.PARAM_SIZE)
                                .description("Maximum number of items per page"));

        mockMvc.perform(get("/people")
                        .param(PersonController.PARAM_Q, "")
                        .param(PersonController.PARAM_PAGE, "0")
                        .param(PersonController.PARAM_SIZE, "25"))
                .andDo(documentation)
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(CONTENT_TYPE_HAL_JSON))
                .andExpect(jsonPath("$.totalElements", is(2)))
                .andExpect(jsonPath("$.totalPages", is(1)))
                .andExpect(jsonPath("$.page", is(0)))
                .andExpect(jsonPath("$.size", is(25)))
                .andExpect(jsonPath("$.items", not(empty())));
    }

    @Test
    public void testGetPeopleIllegalPageNumber() throws Exception {
        mockMvc.perform(get("/people")
                    .param(PersonController.PARAM_PAGE, "NaN"))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(CONTENT_TYPE_JSON))
                .andExpect(jsonPath("$.status", is(400)))
                .andExpect(jsonPath("$.message",
                        is("Request could not be completed")))
                .andDo(modifyResponseTo(prettyPrintContent())
                        .andDocument("people-get-illegal-page"));
    }

}
