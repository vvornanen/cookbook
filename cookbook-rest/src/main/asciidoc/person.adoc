== Person Instance

=== Get Person

==== Example Request

include::{snippets}/person-get/http-request.adoc[]

==== Example Responses

include::{snippets}/person-get/http-response.adoc[]
include::{snippets}/person-get-not-found/http-response.adoc[]

== People Collection

=== Get People

==== Example Request

include::{snippets}/people-get/http-request.adoc[]

==== Query Parameters

include::{snippets}/people-get/query-parameters.adoc[]

==== Example Responses

include::{snippets}/people-get/http-response.adoc[]
include::{snippets}/people-get-illegal-page/http-response.adoc[]

==== Links

include::{snippets}/people-get/links.adoc[]
