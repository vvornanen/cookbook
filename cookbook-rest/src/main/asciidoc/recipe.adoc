== Recipe Instance

=== Get Recipe

==== Example Request

include::{snippets}/recipe-get/http-request.adoc[]

==== Example Responses

include::{snippets}/recipe-get/http-response.adoc[]
include::{snippets}/recipe-get-not-found/http-response.adoc[]

==== Links

include::{snippets}/recipe-get/links.adoc[]

=== Update Recipe

A recipe may be completely replaced by performing a `PUT` request on
the desired resource.

==== Example Request

include::{snippets}/recipe-put/http-request.adoc[]

==== Example Responses

include::{snippets}/recipe-put/http-response.adoc[]
include::{snippets}/recipe-put-not-found/http-response.adoc[]

=== Update Recipe Partially

Update a single or multiple recipe fields by performing a `POST` request on
the desired resource.

==== Example Requests

include::{snippets}/recipe-post-name/http-request.adoc[]
include::{snippets}/recipe-post-multiple/http-request.adoc[]
include::{snippets}/recipe-post-all/http-request.adoc[]

==== Example Responses

include::{snippets}/recipe-post-name/http-response.adoc[]
include::{snippets}/recipe-post-not-found/http-response.adoc[]
include::{snippets}/recipe-post-unknown-operation/http-response.adoc[]
include::{snippets}/recipe-post-invalid-path/http-response.adoc[]

=== Delete Recipe

Recipes are deleted by performing a `DELETE` request on the desired resource.

==== Example Request

include::{snippets}/recipe-delete/http-request.adoc[]

==== Example Responses

If the operation was successful, a success status with no content is returned.

include::{snippets}/recipe-delete/http-response.adoc[]

The server returns an error message if the recipe was not found.

include::{snippets}/recipe-delete-not-found/http-response.adoc[]

== Recipe Collection

=== Get Recipes

==== Example Request

include::{snippets}/recipes-get/http-request.adoc[]

==== Query Parameters

include::{snippets}/recipes-get/query-parameters.adoc[]

==== Example Responses

include::{snippets}/recipes-get/http-response.adoc[]
include::{snippets}/recipes-get-illegal-page/http-response.adoc[]

==== Links

include::{snippets}/recipes-get/links.adoc[]

=== Create a New Recipe

==== Example Request

include::{snippets}/recipes-post/http-request.adoc[]

==== Example Response

include::{snippets}/recipes-post/http-response.adoc[]
