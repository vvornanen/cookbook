package com.versatackle.cookbook.rest.api10.config;

import javax.servlet.Filter;
import org.springframework.core.annotation.Order;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.filter.DelegatingFilterProxy;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

/**
 *
 * @author Ville Vornanen
 */
@Order(10)
public class RestApi10Initializer
        extends AbstractAnnotationConfigDispatcherServletInitializer {

    @Override
    protected String getServletName() {
        return "rest-api10-dispatcher";
    }

    @Override
    protected Class<?>[] getRootConfigClasses() {
        return null;
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class<?>[] {RestApi10Context.class};
    }

    @Override
    protected String[] getServletMappings() {
        return new String[] {"/api/1.0/*"};
    }

    @Override
    protected Filter[] getServletFilters() {
        CharacterEncodingFilter characterEncodingFilter
                = new CharacterEncodingFilter();
        characterEncodingFilter.setEncoding("UTF-8");
        characterEncodingFilter.setForceEncoding(true);

        return new Filter[] {
            characterEncodingFilter,
            new DelegatingFilterProxy("springSecurityFilterChain"),
            new DelegatingFilterProxy("corsFilter")
        };
    }

}
