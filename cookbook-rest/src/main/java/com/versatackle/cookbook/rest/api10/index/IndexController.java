package com.versatackle.cookbook.rest.api10.index;

import com.versatackle.cookbook.rest.api10.label.LabelResource;
import com.versatackle.cookbook.rest.api10.person.PersonResource;
import com.versatackle.cookbook.rest.api10.recipe.RecipeResource;
import javax.inject.Inject;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Index provides links to all available REST API resources.
 *
 * @author Ville Vornanen
 */
@RestController
@ExposesResourceFor(IndexResource.class)
@RequestMapping("")
public class IndexController {

    protected static final String REL_PEOPLE = "people";
    protected static final String REL_RECIPES = "recipes";
    protected static final String REL_LABELS = "labels";

    @Inject
    private EntityLinks entityLinks;

    @RequestMapping(method = RequestMethod.GET)
    public IndexResource getIndex() {
        IndexResource index = new IndexResource();

        index.add(entityLinks.linkToCollectionResource(PersonResource.class)
                .withRel(REL_PEOPLE));
        index.add(entityLinks.linkToCollectionResource(RecipeResource.class)
                .withRel(REL_RECIPES));
        index.add(entityLinks.linkToCollectionResource(LabelResource.class)
                .withRel(REL_LABELS));

        return index;
    }

}
