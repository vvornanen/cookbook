package com.versatackle.cookbook.rest.api10.recipe;

import com.versatackle.cookbook.core.NotFoundException;
import com.versatackle.cookbook.recipe.Recipe;
import com.versatackle.cookbook.recipe.RecipeDTO;
import com.versatackle.cookbook.recipe.RecipeService;
import com.versatackle.cookbook.recipe.index.RecipeDocument;
import com.versatackle.cookbook.recipe.index.RecipeIndexService;
import com.versatackle.cookbook.rest.api10.CollectionResource;
import com.versatackle.cookbook.rest.api10.IllegalClientRequestException;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.solr.core.query.result.HighlightEntry.Highlight;
import org.springframework.data.solr.core.query.result.HighlightPage;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.ExposesResourceFor;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * Exposes REST resources recipe instance and recipes collection.
 *
 * @author Ville Vornanen
 */
@RestController
@ExposesResourceFor(RecipeResource.class)
@RequestMapping("/recipes")
public class RecipeController {

    protected static final String PARAM_Q = "q";
    protected static final String PARAM_PAGE = "page";
    protected static final String PARAM_SIZE = "size";
    protected static final String REL_FIRST = "first";
    protected static final String REL_PREVIOUS = "previous";
    protected static final String REL_NEXT = "next";
    protected static final String REL_LAST = "last";

    private static final Logger logger
            = LoggerFactory.getLogger(RecipeController.class);

    @Inject
    private RecipeService recipeService;

    @Inject
    private RecipeIndexService recipeIndexService;

    @Inject
    private RecipeResourceAssembler resourceAssembler;

    @Inject
    private RecipeDocumentResourceAssembler recipeDocumentResourceAssembler;

    @Inject
    private EntityLinks links;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public CollectionResource<RecipeResource> getRecipes(
            @RequestParam(value = PARAM_Q, required = false, defaultValue = "")
                    String query,
            @RequestParam(value = PARAM_PAGE, required = false, defaultValue = "0")
                    int page,
            @RequestParam(value = PARAM_SIZE, required = false, defaultValue = "25")
                    int size) throws IllegalClientRequestException {
        logger.debug("Get recipes");

        Pageable pageable = newPageRequest(page, size);

        HighlightPage<RecipeDocument> recipes
                = recipeIndexService.search(query, pageable, null);

        CollectionResource<RecipeResource> collection = new CollectionResource<>();
        collection.setTotalElements(recipes.getTotalElements());
        collection.setTotalPages(recipes.getTotalPages());
        collection.setSize(recipes.getSize());
        collection.setPage(recipes.getNumber());

        recipes.getHighlighted().stream().forEach((entry) -> {
            RecipeResource resource = recipeDocumentResourceAssembler.toResource(
                    entry.getEntity());
            List<String> highlights = entry.getHighlights().stream()
                    .map(Highlight::getSnipplets)
                    .flatMap(List::stream)
                    .collect(Collectors.toList());

            collection.addItem(resource, highlights);
        });

        collection.add(linkTo(methodOn(RecipeController.class)
                .getRecipes(query, recipes.getNumber(), recipes.getSize()))
                .withSelfRel());

        collection.add(linkTo(methodOn(RecipeController.class)
                .getRecipes(query, 0, recipes.getSize()))
                .withRel(REL_FIRST));

        if (recipes.hasPrevious()) {
            collection.add(linkTo(methodOn(RecipeController.class)
                    .getRecipes(
                            query,
                            recipes.previousPageable().getPageNumber(),
                            recipes.getSize()))
                    .withRel(REL_PREVIOUS));
        }

        if (recipes.hasNext()) {
            collection.add(linkTo(methodOn(RecipeController.class)
                    .getRecipes(
                            query,
                            recipes.nextPageable().getPageNumber(),
                            recipes.getSize()))
                    .withRel(REL_NEXT));
        }

        collection.add(linkTo(methodOn(RecipeController.class)
                .getRecipes(
                        query,
                        recipes.getTotalPages() - 1,
                        recipes.getSize()))
                .withRel(REL_LAST));

        return collection;
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public RecipeResource createRecipe(@RequestBody RecipeResource resource) {
        logger.debug("Create recipe [{}]", resource);

        Recipe recipe = recipeService.add(toDTO(resource));

        return resourceAssembler.toResource(recipe);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public RecipeResource getRecipe(@PathVariable("id") UUID id)
            throws NotFoundException {
        logger.debug("Get recipe [{}]", id);

        Recipe recipe = recipeService.findById(id);

        return resourceAssembler.toResource(recipe);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.POST)
    public RecipeResource partialUpdateRecipe(
            @PathVariable("id") UUID id,
            @RequestBody RecipePatch patch)
            throws Exception {
        logger.info("Update recipe [{}]", id);

        RecipeDTO dto = RecipeDTO.fromRecipe(recipeService.findById(id));

        patch.apply(dto);

        Recipe updated = recipeService.update(dto);

        return resourceAssembler.toResource(updated);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public RecipeResource updateRecipe(
            @PathVariable("id") UUID id,
            @RequestBody RecipeResource resource)
            throws NotFoundException {
        logger.info("Update recipe [{}]", id);

        RecipeDTO dto = new RecipeDTO();
        dto.setId(id);
        dto.setName(resource.getName());
        dto.setSummary(resource.getSummary());
        dto.setYield(resource.getYield());
        dto.setLabels(resource.getLabels());
        dto.setAuthors(resource.getAuthors());
        dto.setInstructions(resource.getInstructions());

        Recipe updated = recipeService.update(dto);

        return resourceAssembler.toResource(updated);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteRecipe(@PathVariable("id") UUID id)
            throws NotFoundException {
        logger.info("Delete recipe [{}]", id);
        recipeService.deleteById(id);
    }

    private static RecipeDTO toDTO(RecipeResource resource) {
        RecipeDTO dto = new RecipeDTO();
        dto.setName(resource.getName());
        dto.setSummary(resource.getSummary());
        dto.setYield(resource.getYield());
        dto.setLabels(resource.getLabels());
        dto.setAuthors(resource.getAuthors());
        dto.setInstructions(resource.getInstructions());

        return dto;
    }

    private Pageable newPageRequest(int page, int size)
            throws IllegalClientRequestException {
        if (page < 0) {
            throw new IllegalClientRequestException(String.format(
                    "Expected page >= 0, but was [%s]", page));
        }

        if (size < 1) {
            throw new IllegalClientRequestException(String.format(
                    "Expected size > 0, but was [%s]", size));
        }

        return new PageRequest(page, size);
    }

}
