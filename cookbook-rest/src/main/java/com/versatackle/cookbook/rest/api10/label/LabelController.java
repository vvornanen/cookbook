package com.versatackle.cookbook.rest.api10.label;

import com.versatackle.cookbook.core.NotFoundException;
import com.versatackle.cookbook.recipe.index.RecipeIndexService;
import com.versatackle.cookbook.rest.api10.CollectionResource;
import com.versatackle.cookbook.rest.api10.IllegalClientRequestException;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.solr.core.query.result.FacetFieldEntry;
import org.springframework.hateoas.ExposesResourceFor;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Ville Vornanen
 */
@RestController
@ExposesResourceFor(LabelResource.class)
@RequestMapping("/labels")
class LabelController {

    protected static final String PARAM_Q = "q";
    protected static final String PARAM_PAGE = "page";
    protected static final String PARAM_SIZE = "size";
    protected static final String REL_FIRST = "first";
    protected static final String REL_PREVIOUS = "previous";
    protected static final String REL_NEXT = "next";
    protected static final String REL_LAST = "last";

    private static final Logger logger
            = LoggerFactory.getLogger(LabelController.class);

    @Inject
    private RecipeIndexService recipeIndexService;

    @Inject
    private LabelResourceAssembler labelResourceAssembler;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public CollectionResource<LabelResource> getLabels(
            @RequestParam(value = PARAM_Q, required = false, defaultValue = "")
                    String query,
            @RequestParam(value = PARAM_PAGE, required = false, defaultValue = "0")
                    int page,
            @RequestParam(value = PARAM_SIZE, required = false, defaultValue = "25")
                    int size)
            throws IllegalClientRequestException {
        logger.debug("Get labels");

        Pageable pageable = newPageRequest(page, size);

        Page<FacetFieldEntry> labels = recipeIndexService.findLabels(
                query, pageable);

        CollectionResource<LabelResource> collection = new CollectionResource<>();
        collection.setTotalElements(labels.getTotalElements());
        collection.setTotalPages(labels.getTotalPages());
        collection.setSize(labels.getSize());
        collection.setPage(labels.getNumber());

        labels.getContent().stream()
                .map(labelResourceAssembler::toResource)
                .forEach(collection::addItem);

        collection.add(linkTo(methodOn(LabelController.class)
                .getLabels(query, labels.getNumber(), labels.getSize()))
                .withSelfRel());

        collection.add(linkTo(methodOn(LabelController.class)
                .getLabels(query, 0, labels.getSize()))
                .withRel(REL_FIRST));

        if (labels.hasPrevious()) {
            collection.add(linkTo(methodOn(LabelController.class)
                    .getLabels(
                            query,
                            labels.previousPageable().getPageNumber(),
                            labels.getSize()))
                    .withRel(REL_PREVIOUS));
        }

        if (labels.hasNext()) {
            collection.add(linkTo(methodOn(LabelController.class)
                    .getLabels(
                            query,
                            labels.nextPageable().getPageNumber(),
                            labels.getSize()))
                    .withRel(REL_NEXT));
        }

        collection.add(linkTo(methodOn(LabelController.class)
                .getLabels(
                        query,
                        labels.getTotalPages() - 1,
                        labels.getSize()))
                .withRel(REL_LAST));

        return collection;
    }

    @RequestMapping(value = "/{label}", method = RequestMethod.GET)
    public LabelResource getLabel(@PathVariable("label") String label)
            throws NotFoundException {
        FacetFieldEntry result = recipeIndexService.findLabel(label);

        return labelResourceAssembler.toResource(result);
    }

    private Pageable newPageRequest(int page, int size)
            throws IllegalClientRequestException {
        if (page < 0) {
            throw new IllegalClientRequestException(String.format(
                    "Expected page >= 0, but was [%s]", page));
        }

        if (size < 1) {
            throw new IllegalClientRequestException(String.format(
                    "Expected size > 0, but was [%s]", size));
        }

        return new PageRequest(page, size);
    }

}
