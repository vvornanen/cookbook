package com.versatackle.cookbook.rest.api10.recipe;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.JsonNode;
import static com.google.common.base.Preconditions.checkNotNull;
import com.versatackle.cookbook.recipe.RecipeDTO;
import com.versatackle.cookbook.rest.api10.IllegalClientRequestException;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import java.util.stream.StreamSupport;

/**
 *
 * @author Ville Vornanen
 */
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "op"
)
@JsonSubTypes({
    @Type(name = "replace", value = RecipePatchOperation.Replace.class)
})
public abstract class RecipePatchOperation {

    private final String path;
    private final JsonNode value;

    public static class Replace extends RecipePatchOperation {

        @JsonCreator
        public Replace(
                @JsonProperty("path") String path,
                @JsonProperty("value") JsonNode value) {
            super(path, value);
        }

        @Override
        public void apply(RecipeDTO recipe) throws IllegalClientRequestException {
            switch (getPath()) {
                case "/name":
                    recipe.setName(getValue().asText());
                    return;
                case "/summary":
                    recipe.setSummary(getValue().asText());
                    return;
                case "/yield":
                    recipe.setYield(getValue().asText());
                    return;
                case "/labels":
                    if (!getValue().isArray()) {
                        throw new IllegalClientRequestException(
                                "Expected labels to be an array, but was [%s]",
                                getValue().getNodeType());
                    }
                    recipe.setLabels(StreamSupport
                            .stream(getValue().spliterator(), false)
                            .map(JsonNode::asText)
                            .collect(toSet()));
                    return;
                case "/authors":
                    if (!getValue().isArray()) {
                        throw new IllegalClientRequestException(
                                "Expected authors to be an array, but was [%s]",
                                getValue().getNodeType());
                    }
                    recipe.setAuthors(StreamSupport
                            .stream(getValue().spliterator(), false)
                            .map(JsonNode::asText)
                            .collect(toList()));
                    return;
                case "/instructions":
                    recipe.setInstructions(getValue().asText());
                    return;
                default:
                    throw new IllegalClientRequestException(
                            "Invalid path [%s] for replace operation", getPath());
            }
        }

    }

    public abstract void apply(RecipeDTO recipe)
            throws IllegalClientRequestException;

    public String getPath() {
        return path;
    }

    protected RecipePatchOperation(String path, JsonNode value) {
        this.path = checkNotNull(path, "Path must not be null");
        this.value = checkNotNull(value, "JsonNode must not be null");
    }

    protected JsonNode getValue() {
        return value;
    }

}
