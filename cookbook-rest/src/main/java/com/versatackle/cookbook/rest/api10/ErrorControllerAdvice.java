package com.versatackle.cookbook.rest.api10;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.versatackle.cookbook.core.NotFoundException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * Provides a common exception handler for all REST resources.
 *
 * @author Ville Vornanen
 */
@ControllerAdvice
public class ErrorControllerAdvice extends ResponseEntityExceptionHandler {

    @ExceptionHandler(NotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public ResourceError notFound(NotFoundException ex) {
        logger.info("Resource was not found", ex);

        ResourceError error = new ResourceError(
                HttpStatus.NOT_FOUND, "The requested resource was not found");
        error.setCode(ResourceErrors.NOT_FOUND.getCode());
        error.setDeveloperMessage(ex.getMessage());

        return error;
    }

    @ExceptionHandler(IllegalClientRequestException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ResourceError badRequest(IllegalClientRequestException ex) {
        logger.info("Bad request", ex);

        ResourceError error = new ResourceError(
                HttpStatus.BAD_REQUEST, "Request could not be completed");
        error.setCode(ResourceErrors.BAD_REQUEST.getCode());
        error.setDeveloperMessage(ex.getMessage());

        return error;
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(
            HttpMessageNotReadableException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {
        logger.info("Bad request", ex);

        ResourceError error = new ResourceError(
                HttpStatus.BAD_REQUEST, "Request could not be completed");
        error.setCode(ResourceErrors.BAD_REQUEST.getCode());
        error.setDeveloperMessage(ex.getMessage());

        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleExceptionInternal(
            Exception ex,
            Object body,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {
        if (status == HttpStatus.BAD_REQUEST) {
            logger.info("Bad request", ex);

            ResourceError error = new ResourceError(
                    HttpStatus.BAD_REQUEST, "Request could not be completed");
            error.setCode(ResourceErrors.BAD_REQUEST.getCode());
            error.setDeveloperMessage(ex.getMessage());

            return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
        }

        if (status != HttpStatus.INTERNAL_SERVER_ERROR) {
            return super.handleExceptionInternal(ex, body, headers, status, request);
        }

        logger.error("Internal server error", ex);

        ResourceError error = new ResourceError(
                HttpStatus.INTERNAL_SERVER_ERROR,
                "An error occurred while processing request");
        error.setCode(ResourceErrors.INTERNAL_SERVER_ERROR.getCode());
        error.setDeveloperMessage(ex.getMessage());

        return new ResponseEntity<>(error, status);
    }

}
