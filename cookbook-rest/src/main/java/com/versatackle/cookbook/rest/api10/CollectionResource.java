package com.versatackle.cookbook.rest.api10;

import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.ImmutableList;
import java.util.ArrayList;
import java.util.List;
import static java.util.Objects.requireNonNull;
import javax.annotation.concurrent.Immutable;
import org.springframework.hateoas.ResourceSupport;

/**
 * A generic resource for all types of collections with pagination support.
 *
 * @author Ville Vornanen
 * @param <T> resource type of the contained items
 */
public class CollectionResource<T extends AbstractResource> extends ResourceSupport {

    private long totalElements;
    private int totalPages;
    private int size;
    private int page;
    private final List<HighlightEntry<T>> items = new ArrayList<>();

    @Immutable
    public static final class HighlightEntry<T extends AbstractResource> {

        private final T item;
        private final ImmutableList<String> highlights;

        private HighlightEntry(T item) {
            this(item, ImmutableList.of());
        }

        private HighlightEntry(T item, Iterable<String> highlights) {
            this.item = checkNotNull(item);
            this.highlights = ImmutableList.copyOf(highlights);
        }

        public T getItem() {
            return item;
        }

        public ImmutableList<String> getHighlights() {
            return highlights;
        }

    }

    public long getTotalElements() {
        return totalElements;
    }

    public void setTotalElements(long totalElements) {
        this.totalElements = totalElements;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public void addItem(T item) {
        requireNonNull(item, "Item must not be null");
        items.add(new HighlightEntry<>(item));
    }

    public void addItem(T item, Iterable<String> highlights) {
        requireNonNull(item, "Item must not be null");
        requireNonNull(highlights, "Highlights must not be null");

        items.add(new HighlightEntry<>(item, highlights));
    }

    public void addItems(Iterable<T> items) {
        for (T item: items) {
            addItem(item);
        }
    }

    public ImmutableList<HighlightEntry<T>> getItems() {
        return ImmutableList.copyOf(items);
    }

}
