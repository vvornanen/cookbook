package com.versatackle.cookbook.rest.api10.index;

import org.springframework.hateoas.ResourceSupport;

/**
 * Represents a dictionary of links to other resources.
 *
 * @author Ville Vornanen
 */
public class IndexResource extends ResourceSupport {}
