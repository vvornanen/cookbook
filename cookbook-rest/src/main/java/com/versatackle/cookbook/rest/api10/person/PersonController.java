package com.versatackle.cookbook.rest.api10.person;

import com.versatackle.cookbook.core.NotFoundException;
import com.versatackle.cookbook.rest.api10.CollectionResource;
import com.versatackle.cookbook.user.User;
import com.versatackle.cookbook.user.UserService;
import java.util.List;
import java.util.UUID;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.hateoas.ExposesResourceFor;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Exposes REST resources for person instance and people collection.
 *
 * @author Ville Vornanen
 */
@RestController
@ExposesResourceFor(PersonResource.class)
@RequestMapping("/people")
public class PersonController {

    protected static final String PARAM_Q = "q";
    protected static final String PARAM_PAGE = "page";
    protected static final String PARAM_SIZE = "size";
    protected static final String REL_FIRST = "first";
    protected static final String REL_PREVIOUS = "previous";
    protected static final String REL_NEXT = "next";
    protected static final String REL_LAST = "last";

    private static final Logger logger
            = LoggerFactory.getLogger(PersonController.class);

    @Inject
    private UserService userService;

    @Inject
    private PersonResourceAssembler resourceAssembler;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public CollectionResource<PersonResource> getPeople(
            @RequestParam(value = PARAM_Q, required = false, defaultValue = "")
                    String query,
            @RequestParam(value = PARAM_PAGE, required = false, defaultValue = "0")
                    int page,
            @RequestParam(value = PARAM_SIZE, required = false, defaultValue = "25")
                    int size) {
        logger.debug("Get people");

        List<User> users = userService.findAll();

        CollectionResource<PersonResource> collection = new CollectionResource<>();
        collection.setTotalElements(users.size());
        collection.setTotalPages(1);
        collection.setSize(Math.max(size, users.size()));
        collection.setPage(0);

        users.stream()
                .map(resourceAssembler::toResource)
                .forEach(collection::addItem);

        collection.add(linkTo(methodOn(PersonController.class)
                .getPeople(query, page, size))
                .withSelfRel());

        collection.add(linkTo(methodOn(PersonController.class)
                .getPeople(query, 0, size))
                .withRel(REL_FIRST));

        // TODO: Link to previous page

        // TODO: Link to next page

        collection.add(linkTo(methodOn(PersonController.class)
                .getPeople(query, page, size))
                .withRel(REL_LAST));

        return collection;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public PersonResource getPerson(@PathVariable("id") UUID id)
            throws NotFoundException {
        logger.debug("Get person [{}]", id);

        User user = userService.findById(id);

        return resourceAssembler.toResource(user);
    }

}
