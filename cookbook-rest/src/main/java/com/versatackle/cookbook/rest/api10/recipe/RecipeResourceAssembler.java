package com.versatackle.cookbook.rest.api10.recipe;

import com.versatackle.cookbook.recipe.Label;
import com.versatackle.cookbook.recipe.Recipe;
import com.versatackle.cookbook.user.User;
import com.versatackle.cookbook.rest.api10.person.PersonResource;
import java.util.stream.Collectors;
import javax.inject.Inject;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

/**
 * Assembles REST resources from {@link Recipe} entities.
 *
 * @author Ville Vornanen
 */
@Component
public class RecipeResourceAssembler
        extends ResourceAssemblerSupport<Recipe, RecipeResource> {

    @Inject
    private EntityLinks links;

    public RecipeResourceAssembler() {
        super(RecipeController.class, RecipeResource.class);
    }

    @Override
    public RecipeResource toResource(Recipe recipe) {
        RecipeResource resource = createResourceWithId(recipe.getId(), recipe);

        User creator = recipe.getCreator();
        if (creator != null) {
            resource.add(links
                    .linkToSingleResource(PersonResource.class, creator.getId())
                    .withRel("creator"));
        }

        User modifier = recipe.getModifier();
        if (modifier != null) {
            resource.add(links
                    .linkToSingleResource(PersonResource.class, modifier.getId())
                    .withRel("modifier"));
        }

        resource.setCreated(recipe.getCreated());
        resource.setModified(recipe.getModified());

        resource.setName(recipe.getName());
        resource.setSummary(recipe.getSummary());
        resource.setYield(recipe.getYield());
        resource.setLabels(recipe.getLabels().stream()
                .map(Label::toString)
                .collect(Collectors.toSet()));
        resource.setAuthors(recipe.getAuthors());
        resource.setInstructions(recipe.getInstructions());

        return resource;
    }

}
