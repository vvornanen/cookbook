package com.versatackle.cookbook.rest.api10;

/**
 * Used similarly as {@link IllegalArgumentException}, but indicates invalid
 * parameters passed by a client, not an internal programming error.
 *
 * @author Ville Vornanen
 */
public class IllegalClientRequestException extends Exception {

    private static final long serialVersionUID = 876434348012L;

    public IllegalClientRequestException() {
    }

    public IllegalClientRequestException(String message) {
        super(message);
    }

    public IllegalClientRequestException(String message, Object... args) {
        super(String.format(message, args));
    }

    public IllegalClientRequestException(String message, Throwable cause) {
        super(message, cause);
    }

    public IllegalClientRequestException(Throwable cause) {
        super(cause);
    }

}
