package com.versatackle.cookbook.rest.api10.label;

import com.versatackle.cookbook.rest.api10.IllegalClientRequestException;
import com.versatackle.cookbook.rest.api10.recipe.RecipeController;
import com.versatackle.cookbook.rest.api10.recipe.RecipeResource;
import javax.inject.Inject;
import org.springframework.data.solr.core.query.result.FacetFieldEntry;
import org.springframework.hateoas.EntityLinks;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

/**
 *
 * @author Ville Vornanen
 */
@Component
public class LabelResourceAssembler
        extends ResourceAssemblerSupport<FacetFieldEntry, LabelResource> {

    public static final String REL_RECIPES = "recipes";

    @Inject
    private EntityLinks entityLinks;

    public LabelResourceAssembler() {
        super(LabelController.class, LabelResource.class);
    }

    @Override
    public LabelResource toResource(FacetFieldEntry entry) {
        LabelResource resource = createResourceWithId(entry.getValue(), entry);

        resource.setLabel(entry.getValue());
        resource.setNumberOfRecipes(entry.getValueCount());

        String query = String.format(
                "%s:%s", entry.getField().getName(), entry.getValue());

        try {
            resource.add(linkTo(methodOn(RecipeController.class)
                    .getRecipes(query, 0, 25))
                    .withRel(REL_RECIPES));
        } catch (IllegalClientRequestException ignored) {
            // never thrown
        }

        return resource;
    }

}
