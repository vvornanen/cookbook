package com.versatackle.cookbook.rest.api10.person;

import com.versatackle.cookbook.user.User;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

/**
 * Assembles person resources from {@link User} entities.
 *
 * @author Ville Vornanen
 */
@Component
public class PersonResourceAssembler
        extends ResourceAssemblerSupport<User, PersonResource> {

    public PersonResourceAssembler() {
        super(PersonController.class, PersonResource.class);
    }

    @Override
    public PersonResource toResource(User user) {
        PersonResource resource = createResourceWithId(user.getId(), user);

        resource.setCreated(user.getCreated());
        resource.setModified(user.getModified());

        resource.setUsername(user.getUsername());
        resource.setName(user.getName());

        return resource;
    }

}
