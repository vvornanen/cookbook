package com.versatackle.cookbook.rest.api10;

import static com.google.common.base.Preconditions.checkNotNull;
import javax.annotation.CheckForNull;
import javax.annotation.Nullable;
import org.springframework.http.HttpStatus;

/**
 * Describes an error which occurred while requesting a REST resource.
 *
 * @author Ville Vornanen
 */
public class ResourceError {

    private int status;
    private String code;
    private String property;
    private String message;
    private String developerMessage;

    public ResourceError(HttpStatus status, String message) {
        checkNotNull(status, "Status must not be null");
        this.status = status.value();
        this.message = checkNotNull(message, "Message must not be null");
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @CheckForNull
    public String getCode() {
        return code;
    }

    public void setCode(@Nullable String code) {
        this.code = code;
    }

    @CheckForNull
    public String getProperty() {
        return property;
    }

    public void setProperty(@Nullable String property) {
        this.property = property;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = checkNotNull(message, "Message must not be null");
    }

    @CheckForNull
    public String getDeveloperMessage() {
        return developerMessage;
    }

    public void setDeveloperMessage(@Nullable String developerMessage) {
        this.developerMessage = developerMessage;
    }

}
