package com.versatackle.cookbook.rest.api10;

/**
 * Provides all possible resource error codes.
 *
 * <p>These can be used to localize error messages client side.</p>
 *
 * @author Ville Vornanen
 */
public enum ResourceErrors {

    NOT_FOUND("resource.notFound"),
    BAD_REQUEST("request.notCompleted"),
    INTERNAL_SERVER_ERROR("request.internalServerError");

    private final String code;

    private ResourceErrors(String code) {
        this.code = code;

        assert this.code != null;
    }

    public String getCode() {
        return code;
    }

}
