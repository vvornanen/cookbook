package com.versatackle.cookbook.rest.api10.recipe;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.versatackle.cookbook.rest.api10.AbstractResource;
import java.util.List;
import static java.util.Objects.requireNonNull;
import java.util.Optional;
import static java.util.Optional.ofNullable;
import java.util.Set;
import javax.annotation.CheckForNull;
import javax.annotation.Nullable;

/**
 * Represents a REST resource for a recipe instance.
 *
 * @author Ville Vornanen
 */
public class RecipeResource extends AbstractResource {

    private String name;
    private String summary;
    private String yield;
    private Set<String> labels = ImmutableSet.of();
    private List<String> authors = ImmutableList.of();
    private String instructions;

    public RecipeResource() {}

    @JsonCreator
    public RecipeResource(
            @JsonProperty("name") String name,
            @JsonProperty("summary") @Nullable String summary,
            @JsonProperty("yield") @Nullable String yield,
            @JsonProperty("labels") @Nullable Set<String> labels,
            @JsonProperty("authors") @Nullable List<String> authors,
            @JsonProperty("instructions") @Nullable String instructions) {
        this.name = requireNonNull(name, "Name must not be null");
        this.summary = summary;
        this.yield = yield;
        this.labels = ImmutableSet.copyOf(
                ofNullable(labels).orElse(ImmutableSet.of()));
        this.authors = ImmutableList.copyOf(
                ofNullable(authors).orElse(ImmutableList.of()));
        this.instructions = instructions;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = requireNonNull(name, "Name must not be null");
    }

    @CheckForNull
    public String getSummary() {
        return summary;
    }

    public void setSummary(@Nullable String summary) {
        this.summary = summary;
    }

    @CheckForNull
    public String getYield() {
        return yield;
    }

    public void setYield(String yield) {
        this.yield = yield;
    }

    public Set<String> getLabels() {
        return labels;
    }

    public void setLabels(@Nullable Set<String> labels) {
        this.labels = ImmutableSet.copyOf(
                ofNullable(labels).orElse(ImmutableSet.of()));
    }

    public List<String> getAuthors() {
        return authors;
    }

    public void setAuthors(@Nullable List<String> authors) {
        this.authors = ImmutableList.copyOf(
                ofNullable(authors).orElse(ImmutableList.of()));
    }

    @CheckForNull
    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(@Nullable String instructions) {
        this.instructions = instructions;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("name", name)
                .toString();
    }

}
