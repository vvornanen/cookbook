package com.versatackle.cookbook.rest.api10.person;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.versatackle.cookbook.rest.api10.AbstractResource;

/**
 * Represents a REST resource for a person.
 *
 * @author Ville Vornanen
 */
public class PersonResource extends AbstractResource {

    private String username;
    private String name;

    public PersonResource() {}

    @JsonCreator
    public PersonResource(
            @JsonProperty("username") String username,
            @JsonProperty("name") String name) {
        this.username = username;
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
