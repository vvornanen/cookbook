package com.versatackle.cookbook.rest.api10.recipe;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.google.common.collect.ImmutableList;
import com.versatackle.cookbook.recipe.RecipeDTO;
import com.versatackle.cookbook.rest.api10.IllegalClientRequestException;
import java.util.List;

/**
 * Represents a RFC 6902 JSON Patch object.
 *
 * @author Ville Vornanen
 */
public class RecipePatch {

    private final ImmutableList<RecipePatchOperation> operations;

    @JsonCreator
    public RecipePatch(List<RecipePatchOperation> operations) {
        this.operations = ImmutableList.copyOf(operations);
    }

    public void apply(RecipeDTO recipe) throws IllegalClientRequestException {
        for (RecipePatchOperation operation : operations) {
            operation.apply(recipe);
        }
    }

}
