package com.versatackle.cookbook.rest.api10.recipe;

import com.versatackle.cookbook.recipe.index.RecipeDocument;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

/**
 * Assembles recipes into resources.
 *
 * <p>TODO: Should use {@link com.versatackle.cookbook.recipe.RecipeDTO} instead of
 * {@link RecipeDocument}, so that this assembler and
 * {@link RecipeResourceAssembler} could be merged into a single assembler
 * which could be used in all cases not depending on which service the data
 * originates from.</p>
 *
 * @author Ville Vornanen
 */
@Component
public class RecipeDocumentResourceAssembler
        extends ResourceAssemblerSupport<RecipeDocument, RecipeResource> {

    public RecipeDocumentResourceAssembler() {
        super(RecipeController.class, RecipeResource.class);
    }

    @Override
    public RecipeResource toResource(RecipeDocument recipe) {
        RecipeResource resource = createResourceWithId(recipe.getId(), recipe);

        resource.setCreated(recipe.getCreated());
        resource.setModified(recipe.getModified());
        resource.setName(recipe.getName());
        resource.setSummary(recipe.getSummary());
        resource.setYield(recipe.getYield());
        resource.setLabels(recipe.getLabels());
        resource.setAuthors(recipe.getAuthors());
        resource.setInstructions(recipe.getInstructions());

        return resource;
    }

}
