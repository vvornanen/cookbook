package com.versatackle.cookbook.rest.api10.label;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;
import com.versatackle.cookbook.rest.api10.AbstractResource;
import static java.util.Objects.requireNonNull;
import javax.annotation.CheckForNull;
import javax.annotation.Nullable;

/**
 * Represents a REST resource for a label.
 *
 * @author Ville Vornanen
 */
public class LabelResource extends AbstractResource {

    private String label;
    private Long numberOfRecipes;

    public LabelResource() {}

    @JsonCreator
    public LabelResource(
            @JsonProperty("label") String label,
            @JsonProperty("numberOfRecipes") @Nullable Long numberOfRecipes) {
        this.label = requireNonNull(label, "Label must not be null");
        this.numberOfRecipes = numberOfRecipes;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = requireNonNull(label, "Label must not be null");
    }

    @CheckForNull
    public Long getNumberOfRecipes() {
        return numberOfRecipes;
    }

    public void setNumberOfRecipes(@Nullable Long numberOfRecipes) {
        this.numberOfRecipes = numberOfRecipes;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .addValue(label)
                .toString();
    }

}
