package com.versatackle.cookbook.rest.api10;

import static com.google.common.base.Preconditions.checkNotNull;
import java.time.Instant;
import javax.annotation.CheckForNull;
import javax.annotation.Nullable;
import org.springframework.hateoas.ResourceSupport;

/**
 * A common superclass for all REST resources.
 *
 * <p>Provides common meta data properties, similarly to
 * {@link com.versatackle.cookbook.core.AbstractEntity}.</p>
 *
 * @author Ville Vornanen
 */
abstract public class AbstractResource extends ResourceSupport {

    protected Instant created;
    protected Instant modified;

    @CheckForNull
    public final Instant getCreated() {
        return created;
    }

    @CheckForNull
    public final Instant getModified() {
        return modified;
    }

    public final void setCreated(@Nullable Instant created) {
        this.created = created;
    }

    public final void setModified(@Nullable Instant modified) {
        this.modified = modified;
    }

}
