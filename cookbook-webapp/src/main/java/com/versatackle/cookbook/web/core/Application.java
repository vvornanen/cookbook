package com.versatackle.cookbook.web.core;

/**
 * A singleton class providing application metadata.
 *
 * @author Ville Vornanen
 */
public class Application {
    private static final Application INSTANCE = new Application();

    private Application() {
    }

    /**
     * Returns the application singleton.
     *
     * @return the application instance
     */
    public static Application getInstance() {
        return INSTANCE;
    }

    /**
     * Returns the application specification title.
     *
     * @return project name
     */
    public String getName() {
        return getClass().getPackage().getSpecificationTitle();
    }

    /**
     * Returns the application specification version.
     *
     * @return project version
     */
    public String getVersion() {
        return getClass().getPackage().getSpecificationVersion();
    }
}
