package com.versatackle.cookbook.web.config;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import javax.annotation.PreDestroy;
import javax.inject.Inject;
import javax.sql.DataSource;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.core.env.Environment;
import org.springframework.dao.CleanupFailureDataAccessException;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.solr.core.SolrTemplate;
import org.springframework.data.solr.core.convert.SolrJConverter;
import org.springframework.data.solr.repository.config.EnableSolrRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.util.StringUtils;

/**
 *
 * @author Ville Vornanen
 */
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        basePackages = "com.versatackle.cookbook",
        // Ignore Solr repositories
        excludeFilters = @ComponentScan.Filter(
                type = FilterType.REGEX,
                pattern = ".*IndexRepository$"
        )
)
@EnableSolrRepositories(
        basePackages = "com.versatackle.cookbook.recipe.index",
        multicoreSupport = true
)
public class PersistenceContext {

    private static final Logger logger
            = LoggerFactory.getLogger(PersistenceContext.class);

    @Inject
    private Environment environment;

    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(
                environment.getProperty("dataSource.driverClassName"));
        dataSource.setUrl(environment.getProperty("dataSource.url"));
        dataSource.setUsername(environment.getProperty("dataSource.username"));
        dataSource.setPassword(environment.getProperty("dataSource.password"));

        return dataSource;
    }

    @PreDestroy
    public void shutdownDataSource() {
        String shutdownUrl = environment.getProperty("dataSource.shutdownUrl");
        String shutdownCode = environment.getProperty("dataSource.shutdownCode");

        if (!StringUtils.hasText(shutdownUrl)) {
            logger.debug("Data source shutdown URL has not been defined, "
                    + "skipping shutdown");
            return;
        }

        logger.info("Shutting down the data source");

        try {
            DriverManager.getConnection(shutdownUrl);
        } catch (SQLException ex) {
            if (!ex.getSQLState().equals(shutdownCode)) {
                throw new CleanupFailureDataAccessException(
                        "An error occurred while shutting down the data source", ex);
            }
        }
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean entityManagerFactory
                = new LocalContainerEntityManagerFactoryBean();
        entityManagerFactory.setDataSource(dataSource());
        entityManagerFactory.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        entityManagerFactory.setPersistenceUnitName("cookbook-core");

        Properties jpaProperties = new Properties();
        jpaProperties.setProperty("hibernate.dialect",
                environment.getProperty("hibernate.dialect"));
        jpaProperties.setProperty("hibernate.format_sql",
                environment.getProperty("hibernate.format_sql"));
        jpaProperties.setProperty("hibernate.hbm2ddl.auto",
                environment.getProperty("hibernate.hbm2ddl.auto"));
        jpaProperties.setProperty("hibernate.ejb.naming_strategy",
                environment.getProperty("hibernate.ejb.naming_strategy"));
        jpaProperties.setProperty("hibernate.show_sql",
                environment.getProperty("hibernate.show_sql"));
        jpaProperties.setProperty("jadira.usertype.autoRegisterUserTypes", "true");
        entityManagerFactory.setJpaProperties(jpaProperties);

        return entityManagerFactory;
    }

    @Bean
    public JpaTransactionManager transactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(
                entityManagerFactory().getObject());

        return transactionManager;
    }

    @Bean
    public SolrServer solrServer() {
        return new HttpSolrServer(environment.getProperty("solr.server.url"));
    }

    @Bean
    public SolrTemplate solrTemplate() {
        SolrTemplate solrTemplate = new SolrTemplate(solrServer());
        solrTemplate.setSolrConverter(new SolrJConverter());
        solrTemplate.afterPropertiesSet();

        return solrTemplate;
    }

}
