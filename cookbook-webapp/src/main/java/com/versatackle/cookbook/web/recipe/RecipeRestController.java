package com.versatackle.cookbook.web.recipe;

import com.versatackle.cookbook.recipe.index.RecipeDocument;
import com.versatackle.cookbook.recipe.index.RecipeIndexService;
import javax.annotation.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Provides resources for the recipes REST API.
 *
 * @author Ville Vornanen
 */
@RestController
@RequestMapping("/rest/api/1.0/recipes")
public class RecipeRestController {

    private static final Logger LOGGER
            = LoggerFactory.getLogger(RecipeRestController.class);
    private static final String PARAM_QUERY = "q";

    @Resource
    private RecipeIndexService indexService;

    @RequestMapping(method = RequestMethod.GET)
    public Page<RecipeDocument> search(
            @RequestParam(value = PARAM_QUERY, required = false, defaultValue = "")
                    String queryString,
            Pageable pageable, Sort sort) {
        LOGGER.debug("Searching recipes for query [{}]", queryString);
        return indexService.search(queryString, pageable, sort);
    }
}
