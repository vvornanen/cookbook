package com.versatackle.cookbook.web.recipe;

import com.versatackle.cookbook.core.NotFoundException;
import com.versatackle.cookbook.recipe.Recipe;
import com.versatackle.cookbook.recipe.RecipeDTO;
import com.versatackle.cookbook.recipe.RecipeService;
import com.versatackle.cookbook.web.core.Application;
import java.time.ZoneId;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import javax.annotation.Resource;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * Handles CRUD operations for a recipe service.
 *
 * @author Ville Vornanen
 */
@Controller("recipeController2")
public class RecipeController {

    public static final String ATTRIBUTE_RECIPE = "recipe";
    public static final String ATTRIBUTE_RECIPES = "recipes";

    private static final Logger LOGGER
            = LoggerFactory.getLogger(RecipeController.class);

    protected static final String MESSAGE_RECIPE_ADDED =
            "RecipeController.recipeAdded";
    protected static final String MESSAGE_ADD_FAILED =
            "RecipeController.addRecipeFailed";
    protected static final String MESSAGE_RECIPE_UPDATED =
            "RecipeController.recipeUpdated";
    protected static final String MESSAGE_RECIPE_DELETED =
            "RecipeController.recipeDeleted";

    protected static final String FLASH_KEY_FEEDBACK = "feedbackMessage";

    protected static final String CREATE_RECIPE_VIEW = "recipe/create";
    protected static final String RECIPE_VIEW = "recipe/view";
    protected static final String HOME_VIEW = "recipe/list";
    protected static final String EDIT_RECIPE_VIEW = "recipe/edit";

    protected static final String PARAMETER_RECIPE_ID = "id";
    protected static final String REQUEST_MAPPING_VIEW_RECIPE = "/recipe/{id}";

    @Resource
    private RecipeService service;

    @Resource
    private MessageSource messageSource;

    @RequestMapping(value = "/recipe/create", method = RequestMethod.POST)
    public String addRecipe(@Valid @ModelAttribute(ATTRIBUTE_RECIPE) RecipeDTO dto,
            BindingResult result, RedirectAttributes attributes) {
        LOGGER.debug("Adding new recipe with information: {}", dto);

        if (result.hasErrors()) {
            LOGGER.debug("Form was submitted with validation errors. "
                    + "Rendering form view.");
            return CREATE_RECIPE_VIEW;
        }

        Recipe recipe = service.add(dto);
        LOGGER.debug("Added recipe with information: {}", recipe);

        attributes.addAttribute(PARAMETER_RECIPE_ID, recipe.getId());
        addFeedbackMessage(attributes,
                MESSAGE_RECIPE_ADDED, recipe.getName());

        return createRedirectViewPath(REQUEST_MAPPING_VIEW_RECIPE);
    }

    @RequestMapping(value = "/recipe/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public String deleteRecipe(@PathVariable("id") UUID id)
            throws NotFoundException {
        LOGGER.debug("Deleting recipe with id: {}", id);

        Recipe recipe = service.deleteById(id);
        LOGGER.debug("Deleted recipe: {}", recipe);

        return getMessage(MESSAGE_RECIPE_DELETED, recipe.getName());
    }

    @RequestMapping(value = "/recipe/create", method = RequestMethod.GET)
    public String showCreateRecipePage(Model model) {
        LOGGER.debug("Showing create recipe page");

        RecipeDTO dto = new RecipeDTO();
        model.addAttribute(ATTRIBUTE_RECIPE, dto);

        return CREATE_RECIPE_VIEW;
    }

    @RequestMapping(value = "/recipe/{id}", method = RequestMethod.GET)
    public String showRecipePage(@PathVariable("id") UUID id, Model model)
            throws NotFoundException {
        LOGGER.debug("Showing recipe page for recipe with id: {}", id);

        Recipe recipe = service.findById(id);
        LOGGER.debug("Found recipe: {}", recipe);

        model.addAttribute(ATTRIBUTE_RECIPE, recipe);

        return RECIPE_VIEW;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String showHomePage(Model model) {
        LOGGER.debug("Rendering home page");

        List<Recipe> recipes = service.findAll();
        model.addAttribute(ATTRIBUTE_RECIPES, recipes);

        return HOME_VIEW;
    }

    @RequestMapping(value = "/recipe/edit/{id}", method = RequestMethod.GET)
    public String showEditRecipePage(@PathVariable("id") UUID id, Model model)
            throws NotFoundException {
        LOGGER.debug("Showing edit recipe for a recipe with id: {}", id);

        Recipe recipe = service.findById(id);
        LOGGER.debug("Found recipe: {}", recipe);

        model.addAttribute(ATTRIBUTE_RECIPE, RecipeDTO.fromRecipe(recipe));

        return EDIT_RECIPE_VIEW;
    }

    @RequestMapping(value = "/recipe/edit", method = RequestMethod.POST)
    public String updateRecipe(
            @Valid @ModelAttribute(ATTRIBUTE_RECIPE) RecipeDTO dto,
            BindingResult result, RedirectAttributes attributes)
            throws NotFoundException {
        LOGGER.debug("Updating recipe with information: {}", dto);

        if (result.hasErrors()) {
            LOGGER.debug("Form was submitted with validation errors. "
                    + "Rendering form view");
            return EDIT_RECIPE_VIEW;
        }

        Recipe recipe = service.update(dto);
        LOGGER.debug("Updated recipe with information: {}", recipe);

        attributes.addAttribute(PARAMETER_RECIPE_ID, recipe.getId());
        addFeedbackMessage(attributes, MESSAGE_RECIPE_UPDATED, recipe.getName());

        return createRedirectViewPath(REQUEST_MAPPING_VIEW_RECIPE);
    }

    @ModelAttribute("cookbook")
    public Application getApplication() {
        return Application.getInstance();
    }

    @ModelAttribute
    public ZoneId getUserTimeZone() {
        return ZoneId.systemDefault();
    }

    private void addFeedbackMessage(RedirectAttributes model, String key,
            Object... parameters) {
        LOGGER.debug("Adding feedback message with key: {} and parameters: {}",
                key, parameters);
        String localizedMessage = getMessage(key, parameters);
        LOGGER.debug("Localized message is: {}", localizedMessage);
        model.addFlashAttribute(FLASH_KEY_FEEDBACK, localizedMessage);
    }

    private String getMessage(String key, Object... parameters) {
        Locale locale = LocaleContextHolder.getLocale();
        LOGGER.debug("Current locale is {}", locale);

        return messageSource.getMessage(key, parameters, locale);
    }

    private String createRedirectViewPath(String requestMapping) {
        return new StringBuilder()
                .append("redirect:")
                .append(requestMapping)
                .toString();
    }
}
