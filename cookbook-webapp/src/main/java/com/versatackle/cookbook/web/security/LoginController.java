package com.versatackle.cookbook.web.security;

import com.versatackle.cookbook.web.core.Application;
import com.versatackle.cookbook.spi.login.WelcomeMessage;
import com.versatackle.cookbook.spi.login.WelcomeMessageService;
import javax.annotation.CheckForNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Handles displaying the login page.
 *
 * @author Ville Vornanen
 */
@Controller
public class LoginController {

    protected static final String LOGIN_VIEW = "login/login";
    protected static final String ATTRIBUTE_ERROR = "error";
    protected static final String ATTRIBUTE_LOGOUT = "logout";
    protected static final String ATTRIBUTE_APPLICATION = "cookbook";
    protected static final String ATTRIBUTE_WELCOME_MESSAGE = "welcomeMessage";

    @Autowired(required = false)
    @CheckForNull
    private WelcomeMessageService welcomeMessageService;

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String showLoginPage(
            @RequestParam(required = false, defaultValue = "false") boolean error,
            @RequestParam(required = false, defaultValue = "false") boolean logout,
            ModelMap model) {
        model.addAttribute(ATTRIBUTE_ERROR, error);
        model.addAttribute(ATTRIBUTE_LOGOUT, logout);
        return LOGIN_VIEW;
    }

    @ModelAttribute(ATTRIBUTE_APPLICATION)
    public Application getApplication() {
        return Application.getInstance();
    }

    @ModelAttribute(ATTRIBUTE_WELCOME_MESSAGE)
    public WelcomeMessage getWelcomeMessage() {
        if (welcomeMessageService != null) {
            return welcomeMessageService.getMessage(LocaleContextHolder.getLocale());
        } else {
            return null;
        }
    }
}
