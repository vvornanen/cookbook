package com.versatackle.cookbook.web.config;

import com.google.common.eventbus.EventBus;
import com.versatackle.cookbook.events.EventBusBeanPostProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Contains configuration for the application event bus.
 *
 * @author Ville Vornanen
 */
@Configuration
public class EventBusContext {

    @Bean
    public EventBus eventBus() {
        return new EventBus();
    }

    @Bean
    public EventBusBeanPostProcessor eventBusBeanPostProcessor() {
        return new EventBusBeanPostProcessor();
    }

}
