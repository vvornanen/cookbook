package com.versatackle.cookbook.web.config;

import com.versatackle.cookbook.core.HomeDirectoryResolver;
import com.versatackle.cookbook.identifier.IdentifierGenerator;
import com.versatackle.cookbook.identifier.IdentifierGeneratorRegistry;
import com.versatackle.cookbook.identifier.RandomUUIDGenerator;
import com.versatackle.cookbook.recipe.Recipe;
import com.versatackle.cookbook.recipe.RecipeDTO;
import java.io.File;
import java.util.UUID;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.aop.interceptor.SimpleAsyncUncaughtExceptionHandler;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.core.task.TaskExecutor;
import org.springframework.core.task.support.TaskExecutorAdapter;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.stereotype.Controller;

/**
 *
 * @author Ville Vornanen
 */
@Configuration
@Import({
    EventBusContext.class,
    PersistenceContext.class,
    SecurityContext.class
})
@PropertySource("classpath:cookbook-default.properties")
@PropertySource(
        value = "file:${cookbook.home}/cookbook-config.properties",
        ignoreResourceNotFound = true
)
@ComponentScan(
        basePackages = "com.versatackle.cookbook",
        // Don't scan MVC controllers since they're included in WebAppContext
        excludeFilters = {
            @ComponentScan.Filter(
                type = FilterType.ANNOTATION,
                value = Controller.class
            ),
            @ComponentScan.Filter(
                type = FilterType.REGEX,
                pattern = "^com\\.versatackle\\.cookbook\\.rest\\..*"
            )
        }
)
@EnableAsync
@EnableScheduling
public class CookbookContext implements AsyncConfigurer, SchedulingConfigurer {

    @Bean
    public HomeDirectoryResolver homeDirectoryResolver() {
        return new HomeDirectoryResolver();
    }

    @Bean
    public File homeDirectory() {
        return homeDirectoryResolver().resolve();
    }

    @Bean
    @DependsOn("homeDirectory")
    public PropertyPlaceholderConfigurer propertyPlaceholderConfigurer() {
        return new PropertyPlaceholderConfigurer();
    }

    @Bean
    public MessageSource messageSource() {
        ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
        messageSource.setBasename("i18n/messages");
        messageSource.setUseCodeAsDefaultMessage(true);

        return messageSource;
    }

    @Bean
    public IdentifierGenerator<UUID> randomUUIDGenerator() {
        return new RandomUUIDGenerator();
    }

    @Bean
    public IdentifierGeneratorRegistry idGeneratorRegistry() {
        IdentifierGeneratorRegistry registry
                = IdentifierGeneratorRegistry.getInstance();
        registry.registerGenerator(UUID.class, randomUUIDGenerator());

        return registry;
    }

    @Bean
    public Jaxb2Marshaller jaxb2Marshaller() {
        Jaxb2Marshaller jaxb2Marshaller = new Jaxb2Marshaller();
        jaxb2Marshaller.setClassesToBeBound(
                Recipe.class, RecipeDTO.RecipeList.class);

        return jaxb2Marshaller;
    }

    @Bean(destroyMethod = "shutdown")
    public ScheduledExecutorService scheduledExecutor() {
        return Executors.newScheduledThreadPool(5);
    }

    @Bean
    public TaskExecutor taskExecutor() {
        return new TaskExecutorAdapter(scheduledExecutor());
    }

    @Override
    public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
        taskRegistrar.setScheduler(scheduledExecutor());
    }

    @Override
    public Executor getAsyncExecutor() {
        return scheduledExecutor();
    }

    @Override
    public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
        return new SimpleAsyncUncaughtExceptionHandler();
    }

}
