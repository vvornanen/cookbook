package com.versatackle.cookbook.web.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * This controller is used to forward to error views when an error occurs.
 *
 * @author Ville Vornanen
 */
@Controller
public class ErrorController {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(ErrorController.class);

    protected static final String NOT_FOUND_VIEW = "error/404";
    protected static final String INTERNAL_SERVER_ERROR_VIEW = "error/500";

    @RequestMapping("/error/404")
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String showNotFoundPage() {
        log(HttpStatus.NOT_FOUND);
        return NOT_FOUND_VIEW;
    }

    @RequestMapping("/error/500")
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public String showInternalServerErrorPage() {
        log(HttpStatus.INTERNAL_SERVER_ERROR);
        return INTERNAL_SERVER_ERROR_VIEW;
    }

    private void log(HttpStatus status) {
        LOGGER.debug("Rendering error page: {} {}",
                status.toString(), status.getReasonPhrase());
    }
}
