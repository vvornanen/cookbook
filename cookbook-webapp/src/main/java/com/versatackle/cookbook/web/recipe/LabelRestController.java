package com.versatackle.cookbook.web.recipe;

import com.versatackle.cookbook.recipe.index.RecipeIndexService;
import javax.annotation.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.solr.core.query.result.FacetFieldEntry;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Provides resources for the labels REST API.
 *
 * @author Ville Vornanen
 */
@RestController
@RequestMapping("/rest/api/1.0/labels")
public class LabelRestController {

    private static final Logger LOGGER
            = LoggerFactory.getLogger(LabelRestController.class);
    private static final String PARAM_QUERY = "q";

    @Resource
    private RecipeIndexService indexService;

    @RequestMapping(method = RequestMethod.GET)
    public Page<FacetFieldEntry> searchLabels(
            @RequestParam(value = PARAM_QUERY, required = false, defaultValue = "")
                    String queryString,
            Pageable pageable) {
        LOGGER.debug("Searching labels for query [{}]", queryString);
        return indexService.findLabels(queryString, pageable);
    }

}
