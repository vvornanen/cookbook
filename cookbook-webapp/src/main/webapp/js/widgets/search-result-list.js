(function($) {
    var DisplayingResults = function(widget) {
        this.widget = widget;

        this.refresh = function() {
            var widget = this.widget;
            widget.element.empty();
            $.each(widget._documents, function(index, document) {
                widget.element.append(widget._documentTemplate(document));
            });
        };
    };

    var Busy = function(widget) {
        this.widget = widget;

        this.refresh = function() {
            this.widget.element.empty();
            this.widget.element.append(this.widget._busyTemplate());
        };
    };

    var DisplayingNoResults = function(widget) {
        this.widget = widget;

        this.refresh = function() {
            this.widget.element.empty();
            this.widget.element.append(this.widget._noResultsTemplate());
        };
    };

    $.widget("cook.searchResultList", {
        options: {
            containerClass: "recipe-list-group",
            busyTemplate: "<div class\"busy\">Busy</div>",
            documentTemplate: "<div class=\"document\">"
                    + "<a href=\"recipe/{{entity.id}}\" class=\"recipe-list-item list-group-item\">"
                    + "<h4 class=\"list-group-item-heading\">{{entity.name}}</h4>"
                    + "{{#if entity.summary}}<p>{{entity.summary}}</p>{{/if}}"
                    + "{{#if highlights.[0].snipplets.[0]}}<p><em>&hellip;{{{highlights.[0].snipplets.[0]}}}&hellip;</em></p>{{/if}}"
                    + "<div class=\"recipe-labels\">{{#each entity.labels}}"
                    + "<span class=\"label label-default\">{{this}}</span> "
                    + "{{/each}}</div>"
                    + "</a></div>",
            noResultsTemplate: "<div class=\"no-results\">No results</div>",
        },
        _create: function() {
            this._busyTemplate
                    = Handlebars.compile($(this.options.busyTemplate).html());
            this._documentTemplate
                    = Handlebars.compile($(this.options.documentTemplate).html());
            this._noResultsTemplate
                    = Handlebars.compile($(this.options.noResultsTemplate).html());

            this.element.addClass(this.options.containerClass);

            this.clear();
        },
        add: function(document) {
            this._documents.push(document);
            this._trigger("documentAdded", null, document);
        },
        addAll: function(documents) {
            var that = this;

            $.each(documents, function(index, document) {
                that.add(document);
            });
        },
        setResults: function(result) {
            this.clear();

            if (result.highlighted) {
                this.addAll(result.highlighted);
            } else {
                var highlighted = $.map(result.content, function(entity) {
                    return {
                        entity: entity
                    };
                });
                this.addAll(highlighted);
            }

            if (result.totalElements === 0) {
                this.setNoResults();
            } else {
                this.refresh();
            }
        },
        refresh: function() {
            this._state.refresh();
        },
        clear: function() {
            this._documents = [];
            this._setState(new DisplayingResults(this));
            this._trigger("cleared");
        },
        setBusy: function() {
            this._setState(new Busy(this));
            this._trigger("busy");
        },
        setNoResults: function() {
            this._setState(new DisplayingNoResults(this));
            this._trigger("noResults");
        },
        _setState: function(state) {
            this._state = state;
            this.refresh();
        }
    });
}(jQuery));