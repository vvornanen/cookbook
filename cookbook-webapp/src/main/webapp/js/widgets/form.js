(function($) {
    $.widget("cookbook.form", {
        options: {
            formGroup: ".form-group",
            errorMessage: ".error-message",
            hasError: "has-error has-feedback",
            errorFeedback: "<span class=\"glyphicon glyphicon-remove form-control-feedback\"></span>"
        },
        _create: function() {
            // Add classes and feedback elements for invalid form groups
            $(this.element).find(this.options.formGroup)
                    .has(this.options.errorMessage)
                    .addClass(this.options.hasError)
                    .find("input, textarea, select")
                    .after(this.options.errorFeedback);
        }
    });
}(jQuery));
