(function($) {
    $.widget("cookbook.search", {
        options: {
            searchInput: ".search-input",
            searchButton: ".search-button",
            resultList: ".search-result-list",
            url: null
        },
        _create: function() {
            var that = this;

            this.input = $(this.element).find(this.options.searchInput);
            this.button = $(this.element).find(this.options.searchButton);
            this.resultList = $(this.options.resultList);

            this.button.click(function(e) {
                that.search(e);
            });

            this.input.change(function(e) {
                that._trigger("queryChanged", e);
            });
            this.input.keypress(function(e) {
                if (e.keyCode === 13) {
                    $(that.search(e));
                }
            });

            this.resultList.searchResultList();
            this.search();
        },
        getQuery: function() {
            return this.input.val();
        },
        setQuery: function(q) {
            this.input.val(q);
        },
        search: function(e) {
            var that = this;

            this.resultList.searchResultList("setBusy");

            var data = {
                q: this.getQuery()
            };

            this._trigger("search", e, data);

            $.getJSON(
                this.options.url, data,
                function(data, textStatus, jqXHR) {
                    that.resultList.searchResultList("setResults", data);
                }
            );
        }
    });
}(jQuery));