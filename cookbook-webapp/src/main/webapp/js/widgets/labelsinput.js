(function($) {
    $.widget("cookbook.labelsinput", {
        options: {
            confirmKeys: [10, 13, 32],
            tagClass: "label label-info",
            containerClass: null,
            templates: {
                newLabelSuggestion: function(context) {
                    return $("<span></span>").text(context.item.label).html();
                }
            }
        },
        _create: function() {
            var that = this;
            this.labelsSource = this._newLabelsSource();
            this.labelsSource.initialize();

            this.element.tagsinput({
                confirmKeys: this.options.confirmKeys,
                tagClass: function() {
                    return that.options.tagClass;
                }
            });

            this.container = this.element.next(".bootstrap-tagsinput");

            if (this.options.containerClass !== null) {
                this.container.addClass(this.options.containerClass);
            }

            this.element.tagsinput("input").typeahead({
                        highlight: true
                    },
                    {
                        displayKey: "label",
                        source: this.labelsSource.ttAdapter()
                    },
                    {
                        displayKey: "label",
                        source: function(query, cb) {
                            that._suggestNewLabel(query, cb);
                        },
                        templates: {
                            suggestion: function(item) {
                                var context = {
                                    item: item
                                };

                                return that.options.templates.newLabelSuggestion(
                                        context);
                            }
                        }
                    })
                    .on("typeahead:selected", function (event, suggestion) {
                        that.element.tagsinput("add", suggestion.label);
                        that.element.tagsinput("input").typeahead("val", "");
                    })
                    .focusout(function() {
                        $(this).typeahead("val", "");
                    });

            // Override input's default vertial align
            this.element.tagsinput("input").css("vertical-align", "middle");

            this.element.on("itemAdded", function() {
                $(this).tagsinput("input").typeahead("close");
            });
        },
        _newLabelsSource: function() {
            return new Bloodhound({
                datumTokenizer: function(d) {
                    return Bloodhound.tokenizers.whitespace(d.label);
                },
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                // Some predefined labels until the index api has been implemented.
                prefetch: {
                    url: $("meta[name='predefined-labels-url']").attr("content")
                }
            });
        },
        _suggestNewLabel: function(query, cb) {
            // Returns a new label suggestion if no exact existing match was found.
            this.labelsSource.get(query, function(suggestions) {
                // Find existing labels matching exactly the query
                var matches = $.grep(suggestions, function(item) {
                    return item.label === query;
                });

                // Return a suggestion for a new label if no match was found
                if (matches.length === 0) {
                    cb([{ label: query}]);
                }
            });
        }
    });
}(jQuery));
