(function($) {
    $.widget("cookbook.deleterecipedialog", {
        options: {
            autoOpen: true,
            template: "#template-delete-recipe-dialog",
            cancelButton: "#delete-recipe-dialog-cancel-button",
            deleteButton: "#delete-recipe-dialog-delete-button",
            recipe: {
                id: null
            }
        },
        _create: function() {
            var that = this;
            this._template = Handlebars.compile($(this.options.template).html());

            $(this.element).on("click", this.options.deleteButton, function(e) {
                e.preventDefault();
                that._doDelete(e);
            });

            $(this.element).on("click", this.options.cancelButton, function(e) {
                e.preventDefault();
                that._doCancel(e);
            });
        },
        _init: function() {
            if (this.options.autoOpen) {
                this.open();
            }
        },
        open: function() {
            this._dialog = $(this._template());
            this.element.append(this._dialog);
            $(this._dialog).modal();
        },
        close: function() {
            this._dialog.modal("hide").remove();
        },
        _doCancel: function(event) {
            $(this._dialog).modal("hide").remove();
            this._trigger("cancel", event);
        },
        _doDelete: function(event) {
            var that = this;
            var url = $("meta[name='base-url']").attr("content")
                    + "/recipe/" + encodeURIComponent(this.options.recipe.id);

            var headers = {};
            headers[$("meta[name='csrf-header-name']").attr("content")]
                    = $("meta[name='csrf-token']").attr("content");

            $.ajax({
                type: "DELETE",
                url: url,
                headers: headers,
                success: function(result) {
                    that.close();
                    that._trigger("deleted", event, result);
                }
            });
        }
    });
}(jQuery));
