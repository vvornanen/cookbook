(function($) {
    $(function() {
        $("form").form();
        $("#recipe-labels").labelsinput({
            containerClass: "form-control",
            tagClass: "label label-default",
            templates: {
                newLabelSuggestion: Handlebars.compile(
                        $("#template-new-label-suggestion").html())
            }
        });
        $("#recipe-instructions").summernote();
    });
})(jQuery);
