(function($) {
    $(function() {
        $("#recipes-search-field").search({
            url: $("meta[name='base-url']").attr("content") + "/rest/api/1.0/recipes",
            resultList: "#recipe-list"
        });
        $("button[data-toggle='tooltip']").tooltip();
    });
})(jQuery);
