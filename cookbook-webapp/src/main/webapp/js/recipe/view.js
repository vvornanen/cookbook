(function($) {
    $(function() {
        $("#delete-recipe-button").on("click", function(e) {
            $("body").deleterecipedialog({
                recipe: {
                    id: $("meta[name='recipe-id']").attr("content")
                },
                deleted: function(e, result) {
                    window.location.href = $("meta[name='base-url']")
                            .attr("content");
                }
            });
        });
    });
})(jQuery);
