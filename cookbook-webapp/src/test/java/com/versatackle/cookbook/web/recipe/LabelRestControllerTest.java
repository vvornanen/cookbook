package com.versatackle.cookbook.web.recipe;

import com.versatackle.cookbook.recipe.index.RecipeIndexService;
import com.versatackle.cookbook.test.util.PageStub;
import com.versatackle.cookbook.web.config.TestContext;
import com.versatackle.cookbook.web.config.WebAppContext;
import javax.annotation.Resource;
import org.mockito.Mockito;
import static org.mockito.Mockito.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.solr.core.query.result.FacetFieldEntry;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Integration test for {@link LabelRestController}.
 *
 * @author Ville Vornanen
 */
@ContextConfiguration(classes = {WebAppContext.class, TestContext.class})
@WebAppConfiguration
public class LabelRestControllerTest extends AbstractTestNGSpringContextTests {

    private MockMvc mockMvc;

    @Resource
    private RecipeIndexService serviceMock;

    @Resource
    private WebApplicationContext webApplicationContext;

    @BeforeMethod(alwaysRun = true)
    public void setUp() {
        Mockito.reset(serviceMock);

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                .build();
    }

    @Test
    public void testSearchLabels() throws Exception {
        Page<FacetFieldEntry> expected = new PageStub<>();

        when(serviceMock.findLabels(any(String.class), any(Pageable.class)))
                .thenReturn(expected);

        mockMvc.perform(get("/rest/api/1.0/labels"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(header().string("Content-Type",
                        "application/json;charset=UTF-8"));
        // Response format is not yet specified. Currently, it uses Page as the JSON
        // response object.
    }

}
