package com.versatackle.cookbook.web.core;

import static org.testng.Assert.*;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Unit test for {@link ErrorController}.
 *
 * @author Ville Vornanen
 */
public class ErrorControllerTest {

    private ErrorController controller;

    @BeforeMethod
    public void setUp() {
        controller = new ErrorController();
    }

    @Test
    public void testNotFound() {
        assertEquals(controller.showNotFoundPage(),
                ErrorController.NOT_FOUND_VIEW);
    }

    @Test
    public void testInternalServerError() {
        assertEquals(controller.showInternalServerErrorPage(),
                ErrorController.INTERNAL_SERVER_ERROR_VIEW);
    }
}
