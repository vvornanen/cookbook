package com.versatackle.cookbook.web.security;

import com.versatackle.cookbook.spi.login.WelcomeMessage;
import com.versatackle.cookbook.web.config.TestContext;
import com.versatackle.cookbook.web.config.WebAppContext;
import com.versatackle.cookbook.web.config.WebAppTestContext;
import javax.annotation.Resource;
import static org.hamcrest.Matchers.*;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration test for {@link LoginController}.
 *
 * Runs the controller inside an application context.
 *
 * @author Ville Vornanen
 */
@ContextConfiguration(classes = {
    WebAppContext.class,
    WebAppTestContext.class,
    TestContext.class
})
@WebAppConfiguration
public class LoginControllerTest extends AbstractTestNGSpringContextTests {
    private MockMvc mockMvc;

    @Resource
    private WebApplicationContext webApplicationContext;

    @BeforeMethod(alwaysRun = true)
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                .build();
    }

    @Test
    public void testShowLoginPage() throws Exception {
        mockMvc.perform(get("/login"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name(LoginController.LOGIN_VIEW))
                .andExpect(forwardedUrl("/WEB-INF/templates/login/login.html"))
                .andExpect(model().attribute(LoginController.ATTRIBUTE_ERROR, false))
                .andExpect(model().attribute(
                        LoginController.ATTRIBUTE_LOGOUT, false))
                .andExpect(model().attribute(
                        LoginController.ATTRIBUTE_WELCOME_MESSAGE,
                        any(WelcomeMessage.class)));
    }

    @Test
    public void testShowLoginPageHasError() throws Exception {
        mockMvc.perform(get("/login?error=true"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name(LoginController.LOGIN_VIEW))
                .andExpect(forwardedUrl("/WEB-INF/templates/login/login.html"))
                .andExpect(model().attribute(LoginController.ATTRIBUTE_ERROR, true))
                .andExpect(model().attribute(
                        LoginController.ATTRIBUTE_LOGOUT, false));
    }

    @Test
    public void testShowLoginPageHasLogout() throws Exception {
        mockMvc.perform(get("/login?logout=true"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name(LoginController.LOGIN_VIEW))
                .andExpect(forwardedUrl("/WEB-INF/templates/login/login.html"))
                .andExpect(model().attribute(LoginController.ATTRIBUTE_ERROR, false))
                .andExpect(model().attribute(
                        LoginController.ATTRIBUTE_LOGOUT, true));
    }
}
