package com.versatackle.cookbook.web.recipe;

import com.versatackle.cookbook.core.NotFoundException;
import com.versatackle.cookbook.core.fixtures.CommonFixtures;
import com.versatackle.cookbook.recipe.Recipe;
import com.versatackle.cookbook.recipe.RecipeDTO;
import com.versatackle.cookbook.recipe.RecipeService;
import com.versatackle.cookbook.recipe.fixtures.RecipeFixtures;
import com.versatackle.cookbook.web.config.TestContext;
import com.versatackle.cookbook.web.config.WebAppContext;
import com.versatackle.cookbook.web.config.WebAppTestContext;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isEmptyString;
import static org.hamcrest.Matchers.nullValue;
import org.mockito.Mockito;
import static org.mockito.Mockito.*;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Integration test for {@link RecipeController}.
 *
 * Runs the controller inside an application context with a service mock.
 *
 * @author Ville Vornanen
 */
@ContextConfiguration(classes = {
    WebAppContext.class,
    WebAppTestContext.class,
    TestContext.class
})
@WebAppConfiguration
public class RecipeControllerTest extends AbstractTestNGSpringContextTests {
    private MockMvc mockMvc;

    @Resource
    private RecipeService service;

    @Resource
    private WebApplicationContext webApplicationContext;

    @BeforeMethod(alwaysRun = true)
    public void setUp() {
        Mockito.reset(service);

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                .build();
    }

    @Test
    public void testAddRecipe() throws Exception {
        RecipeDTO dto = RecipeFixtures.newStandardRecipeDTONoId();
        Recipe expected = RecipeFixtures.newStandardRecipe();
        when(service.add(dto)).thenReturn(expected);

        String expectedView = createExpectedRedirectViewPath(
                RecipeController.REQUEST_MAPPING_VIEW_RECIPE);

        mockMvc.perform(post("/recipe/create")
                    .param("name", dto.getName())
                    .param("summary", dto.getSummary())
                    .param("yield", dto.getYield())
                    .param("labels", dto.getLabels())
                    .param("authors", dto.getAuthors())
                    .param("instructions", dto.getInstructions()))
                .andDo(print())
                .andExpect(status().isMovedTemporarily())
                .andExpect(view().name(expectedView))
                .andExpect(redirectedUrl("/recipe/" + expected.getId()))
                .andExpect(flash().attributeExists(
                        RecipeController.FLASH_KEY_FEEDBACK));

        verify(service).add(dto);
        verifyNoMoreInteractions(service);
    }

    @Test
    public void testAddEmptyRecipe() throws Exception {
        mockMvc.perform(post("/recipe/create").param("name", ""))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name(RecipeController.CREATE_RECIPE_VIEW))
                .andExpect(forwardedUrl("/WEB-INF/templates/recipe/create.html"))
                .andExpect(model().attributeHasFieldErrors(
                        RecipeController.ATTRIBUTE_RECIPE, "name"))
                .andExpect(model().attribute(
                        RecipeController.ATTRIBUTE_RECIPE,
                        hasProperty("id", nullValue())))
                .andExpect(model().attribute(
                        RecipeController.ATTRIBUTE_RECIPE,
                        hasProperty("name", is(""))));

        verifyZeroInteractions(service);
    }

    @Test
    public void testDeleteRecipe() throws Exception {
        Recipe expected = RecipeFixtures.newStandardRecipe();
        when(service.deleteById(expected.getId())).thenReturn(expected);

        mockMvc.perform(delete("/recipe/" + expected.getId()))
                .andDo(print())
                .andExpect(status().isOk());

        verify(service).deleteById(expected.getId());
        verifyNoMoreInteractions(service);
    }

    @Test
    public void testDeleteRecipeNotFound() throws Exception {
        when(service.deleteById(CommonFixtures.getStandardId()))
                .thenThrow(new NotFoundException());

        mockMvc.perform(delete("/recipe/" + CommonFixtures.getStandardId()))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andExpect(forwardedUrl("/WEB-INF/templates/error/404.html"));

        verify(service).deleteById(CommonFixtures.getStandardId());
        verifyNoMoreInteractions(service);
    }

    @Test
    public void testShowCreateRecipePage() throws Exception {
        mockMvc.perform(get("/recipe/create"))
                .andDo(print())
                .andExpect(view().name(RecipeController.CREATE_RECIPE_VIEW))
                .andExpect(model().attribute(
                        RecipeController.ATTRIBUTE_RECIPE,
                        hasProperty("id", nullValue())))
                .andExpect(model().attribute(
                        RecipeController.ATTRIBUTE_RECIPE,
                        hasProperty("name", isEmptyString())));
    }

    @Test
    public void testShowRecipePage() throws Exception {
        Recipe expected = RecipeFixtures.newStandardRecipe();
        when(service.findById(expected.getId())).thenReturn(expected);

        mockMvc.perform(get("/recipe/" + expected.getId()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name(RecipeController.RECIPE_VIEW))
                .andExpect(forwardedUrl("/WEB-INF/templates/recipe/view.html"))
                .andExpect(model().attribute(
                        RecipeController.ATTRIBUTE_RECIPE, expected));

        verify(service).findById(expected.getId());
        verifyNoMoreInteractions(service);
    }

    @Test
    public void testShowRecipePageNotFound() throws Exception {
        when(service.findById(CommonFixtures.getStandardId()))
                .thenThrow(new NotFoundException());

        mockMvc.perform(get("/recipe/" + CommonFixtures.getStandardId()))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andExpect(forwardedUrl("/WEB-INF/templates/error/404.html"));

        verify(service).findById(CommonFixtures.getStandardId());
        verifyNoMoreInteractions(service);
    }

    @Test
    public void testShowHomePage() throws Exception {
        List<Recipe> expected = new ArrayList<>();
        when(service.findAll()).thenReturn(expected);

        mockMvc.perform(get("/"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name(RecipeController.HOME_VIEW))
                .andExpect(forwardedUrl("/WEB-INF/templates/recipe/list.html"))
                .andExpect(model().attribute(
                        RecipeController.ATTRIBUTE_RECIPES, expected));

        verify(service).findAll();
        verifyNoMoreInteractions(service);
    }

    @Test
    public void testShowEditRecipePage() throws Exception {
        Recipe expected = RecipeFixtures.newStandardRecipe();
        when(service.findById(expected.getId())).thenReturn(expected);

        mockMvc.perform(get("/recipe/edit/" + expected.getId()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name(RecipeController.EDIT_RECIPE_VIEW))
                .andExpect(forwardedUrl("/WEB-INF/templates/recipe/edit.html"))
                .andExpect(model().attribute(
                        RecipeController.ATTRIBUTE_RECIPE,
                        RecipeDTO.fromRecipe(expected)));

        verify(service).findById(expected.getId());
        verifyNoMoreInteractions(service);
    }

    @Test
    public void testShowEditRecipePageNotFound() throws Exception {
        when(service.findById(CommonFixtures.getStandardId()))
                .thenThrow(new NotFoundException());

        mockMvc.perform(get("/recipe/edit/" + CommonFixtures.getStandardId()))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andExpect(forwardedUrl("/WEB-INF/templates/error/404.html"));

        verify(service).findById(CommonFixtures.getStandardId());
        verifyNoMoreInteractions(service);
    }

    @Test
    public void testUpdateRecipe() throws Exception {
        Recipe expected = RecipeFixtures.newStandardRecipe();
        RecipeDTO dto = RecipeFixtures.newStandardRecipeDTO();
        when(service.update(dto)).thenReturn(expected);

        String expectedView = createExpectedRedirectViewPath(
                RecipeController.REQUEST_MAPPING_VIEW_RECIPE);

        mockMvc.perform(post("/recipe/edit")
                        .param("id", dto.getId().toString())
                        .param("name", dto.getName())
                        .param("summary", dto.getSummary())
                        .param("yield", dto.getYield())
                        .param("labels", dto.getLabels())
                        .param("authors", dto.getAuthors())
                        .param("instructions", dto.getInstructions()))
                .andDo(print())
                .andExpect(status().isMovedTemporarily())
                .andExpect(view().name(expectedView))
                .andExpect(redirectedUrl("/recipe/" + expected.getId()))
                .andExpect(flash().attributeExists(
                        RecipeController.FLASH_KEY_FEEDBACK));

        verify(service).update(dto);
        verifyNoMoreInteractions(service);
    }

    @Test
    public void testUpdateRecipeNotFound() throws Exception {
        RecipeDTO dto = RecipeFixtures.newStandardRecipeDTO();
        when(service.update(dto)).thenThrow(new NotFoundException());

        mockMvc.perform(post("/recipe/edit")
                        .param("id", dto.getId().toString())
                        .param("name", dto.getName())
                        .param("summary", dto.getSummary())
                        .param("yield", dto.getYield())
                        .param("labels", dto.getLabels())
                        .param("authors", dto.getAuthors())
                        .param("instructions", dto.getInstructions()))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andExpect(forwardedUrl("/WEB-INF/templates/error/404.html"));

        verify(service).update(dto);
        verifyNoMoreInteractions(service);
    }

    @Test
    public void testUpdateRecipeEmptyName() throws Exception {
        RecipeDTO dto = RecipeFixtures.newStandardRecipeDTO();
        dto.setName("");

        mockMvc.perform(post("/recipe/edit")
                        .param("id", dto.getId().toString())
                        .param("name", dto.getName())
                        .param("summary", dto.getSummary())
                        .param("yield", dto.getYield())
                        .param("labels", dto.getLabels())
                        .param("authors", dto.getAuthors())
                        .param("instructions", dto.getInstructions()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name(RecipeController.EDIT_RECIPE_VIEW))
                .andExpect(forwardedUrl("/WEB-INF/templates/recipe/edit.html"))
                .andExpect(model().attributeHasFieldErrors(
                        RecipeController.ATTRIBUTE_RECIPE, "name"))
                .andExpect(model().attribute(
                        RecipeController.ATTRIBUTE_RECIPE, dto));

        verifyZeroInteractions(service);
    }

    private String createExpectedRedirectViewPath(String path) {
        return new StringBuilder()
                .append("redirect:")
                .append(path)
                .toString();
    }
}
