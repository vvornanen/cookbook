package com.versatackle.cookbook.web.events;

import com.google.common.eventbus.EventBus;
import com.versatackle.cookbook.web.config.EventBusContext;
import com.versatackle.cookbook.web.config.TestContext;
import javax.annotation.Resource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import static org.testng.Assert.*;
import org.testng.annotations.Test;

/**
 * Tests that the event bus bean and bean post processor are configured properly.
 *
 * @author Ville Vornanen
 */
@ContextConfiguration(
        classes = {EventBusContext.class, TestContext.class}
)
public class EventBusIntegrationTest extends AbstractTestNGSpringContextTests {

    @Resource
    EventBus eventBus;

    @Resource
    TestListener listener;

    @Test
    public void testEventIsPropagated() {
        TestListener.TestEvent e = new TestListener.TestEvent();
        eventBus.post(e);

        assertSame(listener.getLastEvent(), e);
    }
}
