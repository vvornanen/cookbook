package com.versatackle.cookbook.web.events;

import com.google.common.eventbus.Subscribe;

/**
 * A helper class to assert that a posted event was handled by the registered
 * listener.
 *
 * @author Ville Vornanen
 */
public class TestListener {

    private TestEvent lastEvent;

    public static final class TestEvent {
    }

    @Subscribe
    public void onEvent(TestEvent e) {
        lastEvent = e;
    }

    public TestEvent getLastEvent() {
        return lastEvent;
    }
}
