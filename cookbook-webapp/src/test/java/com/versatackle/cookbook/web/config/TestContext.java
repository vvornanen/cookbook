package com.versatackle.cookbook.web.config;

import com.versatackle.cookbook.recipe.RecipeService;
import com.versatackle.cookbook.recipe.index.RecipeIndexService;
import com.versatackle.cookbook.spi.login.WelcomeMessage;
import com.versatackle.cookbook.spi.login.WelcomeMessageService;
import com.versatackle.cookbook.user.UserService;
import com.versatackle.cookbook.web.events.TestListener;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import static org.mockito.Mockito.*;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

/**
 *
 * @author Ville Vornanen
 */
@Configuration
public class TestContext {

    @Bean
    public MessageSource messageSource() {
        ResourceBundleMessageSource messageSource
                = new ResourceBundleMessageSource();
        messageSource.setBasename("i18n/messages");
        messageSource.setUseCodeAsDefaultMessage(true);

        return messageSource;
    }

    @Bean
    public EntityManagerFactory entityManagerFactory() {
        // TODO: Remove after no longer needed by OpenEntityManagerInViewInterceptor
        // defined in WebAppContext
        EntityManagerFactory emf = mock(EntityManagerFactory.class);
        when(emf.createEntityManager()).thenReturn(mock(EntityManager.class));

        return emf;
    }

    @Bean
    public RecipeService recipeService() {
        return mock(RecipeService.class);
    }

    @Bean
    public RecipeIndexService recipeIndexService() {
        return mock(RecipeIndexService.class);
    }

    @Bean
    public UserService userService() {
        return mock(UserService.class);
    }

    @Bean
    public Validator validator() {
        return new LocalValidatorFactoryBean();
    }

    @Bean
    public TestListener testListener() {
        return new TestListener();
    }

    @Bean
    public WelcomeMessage welcomeMessage() {
        return new WelcomeMessage() {

            @Override
            public String getTitle() {
                return "Welcome";
            }

            @Override
            public String getContent() {
                return "Welcome message";
            }
        };
    }

    @Bean
    public WelcomeMessageService welcomeMessageService() {
        WelcomeMessageService welcomeMessageService
                = mock(WelcomeMessageService.class);
        when(welcomeMessageService.getMessage(any(Locale.class)))
                .thenReturn(welcomeMessage());

        return welcomeMessageService;
    }

}
